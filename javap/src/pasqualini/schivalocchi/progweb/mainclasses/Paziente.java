package pasqualini.schivalocchi.progweb.mainclasses;
/*
* TODO:
*  Aggiungere la foto profilo e l'update
*  Aggiungere notifiche*
* */

import java.sql.Date;
import java.util.Objects;

public class Paziente extends Utente {
    private String nome;
    private String cognome;
    private String sesso;
    private String cf;
    private String luogoNascita;
    private Date dataNascita;
    private String provincaResidenza;

    public Paziente(String id, String fotoPath, String email, String nome, String cognome, String sesso, String cf, String luogoNascita, Date dataNascita, String provincaResidenza) {
        super(id, fotoPath, email);
        this.nome = nome;
        this.cognome = cognome;
        this.sesso = sesso;
        this.cf = cf;
        this.luogoNascita = luogoNascita;
        this.dataNascita = dataNascita;
        this.provincaResidenza = provincaResidenza;
    }

    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public String getSesso() {
        return sesso;
    }
    public String getCf() {
        return cf;
    }
    public String getLuogoNascita() {
        return luogoNascita;
    }
    public String getProvincaResidenza() {
        return provincaResidenza;
    }

    public void setProvincaResidenza(String provincaResidenza) {
        this.provincaResidenza = provincaResidenza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paziente paziente = (Paziente) o;
        return id.equals(paziente.id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Paziente{" +
                "id='" + id + '\'' +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", sesso='" + sesso + '\'' +
                ", cf='" + cf + '\'' +
                ", luogoNascita='" + luogoNascita + '\'' +
                ", dataNascita=" + dataNascita +
                ", email='" + email + '\'' +
                ", provincaResidenza='" + provincaResidenza + '\'' +
                '}';
    }
}
