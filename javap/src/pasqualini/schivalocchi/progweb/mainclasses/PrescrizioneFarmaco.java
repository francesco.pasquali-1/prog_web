package pasqualini.schivalocchi.progweb.mainclasses;

public class PrescrizioneFarmaco {
    private VisitaBase visita;
    private Farmaco farmaco;
    private int quantity;

    public PrescrizioneFarmaco(VisitaBase visita, Farmaco farmaco, int quantity) {
        this.visita = visita;
        this.farmaco = farmaco;
        this.quantity = quantity;
    }

    public VisitaBase getVisita() {
        return visita;
    }
    public Farmaco getFarmaco() {
        return farmaco;
    }
    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "PrescrizioneFarmaco{" +
                "farmaco=" + farmaco +
                ", quantity=" + quantity +
                '}';
    }
}
