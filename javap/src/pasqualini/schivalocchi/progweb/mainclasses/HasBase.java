package pasqualini.schivalocchi.progweb.mainclasses;

public class HasBase {
    private String pid;
    private String mid;

    public HasBase(String pid, String mid) {
        this.pid = pid;
        this.mid = mid;
    }

    public String getPid() {
        return pid;
    }
    public String getMid() {
        return mid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
    public void setMid(String mid) {
        this.mid = mid;
    }
}
