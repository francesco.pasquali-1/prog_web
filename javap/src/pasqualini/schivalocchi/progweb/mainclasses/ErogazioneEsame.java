package pasqualini.schivalocchi.progweb.mainclasses;

public class ErogazioneEsame {
    private PrescrizioneEsame esame;
    private Ticket ticket;
    private Ssn ssn;

    public ErogazioneEsame(PrescrizioneEsame esame, Ticket ticket, Ssn ssn) {
        this.esame = esame;
        this.ticket = ticket;
        this.ssn = ssn;
    }

    public PrescrizioneEsame getEsame() {
        return esame;
    }
    public Ticket getTicket() {
        return ticket;
    }
    public Ssn getSsn() {
        return ssn;
    }
}
