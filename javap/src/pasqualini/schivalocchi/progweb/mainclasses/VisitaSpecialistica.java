package pasqualini.schivalocchi.progweb.mainclasses;

import java.util.Objects;

public class VisitaSpecialistica {
    Paziente paziente;
    Medico medico;
    Ticket ticket;
    String description;

    public VisitaSpecialistica(Paziente p, Medico m, Ticket t, String d){
        this.paziente = p;
        this.medico = m;
        this.ticket = t;
        this.description = d;
    }

    public Paziente getPaziente() {
        return paziente;
    }
    public Medico getMedico() {
        return medico;
    }
    public Ticket getTicket() {
        return ticket;
    }
    public String getDescription() {
        return description;
    }

    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }
    public void setMedico(Medico medico) {
        this.medico = medico;
    }
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitaSpecialistica that = (VisitaSpecialistica) o;
        return paziente.equals(that.paziente) &&
                medico.equals(that.medico) &&
                ticket.equals(that.ticket);
    }
    @Override
    public int hashCode() {
        return Objects.hash(paziente, medico, ticket);
    }

    @Override
    public String toString() {
        return "VisitaSpecialistica{" +
                "paziente=" + paziente +
                ", medico=" + medico +
                ", ticket=" + ticket +
                ", description='" + description + '\'' +
                '}';
    }
}
