package pasqualini.schivalocchi.progweb.mainclasses;

import java.util.Objects;

public class Utente {
    String id;
    String fotoPath;
    String email;

    public Utente(String id, String fotoPath, String email){
        this.id = id;
        this.fotoPath = fotoPath;
        this.email = email;
    }

    public String getId() {
        return id;
    }
    public String getFotoPath() {
        return fotoPath;
    }
    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setFotoPath(String fotoPath) {
        this.fotoPath = fotoPath;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utente utente = (Utente) o;
        return id.equals(utente.id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Utente{" +
                "id='" + id + '\'' +
                ", fotoPath='" + fotoPath + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
