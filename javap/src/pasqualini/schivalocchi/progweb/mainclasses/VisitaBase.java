package pasqualini.schivalocchi.progweb.mainclasses;

import java.util.Date;

public class VisitaBase {
    private String id;
    private Date date;
    private String description;
    private Paziente paziente;
    private Medico medico;

    public VisitaBase(String id, Date date, String description, Paziente paziente, Medico medico) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.paziente = paziente;
        this.medico = medico;
    }

    public String getId() {
        return id;
    }
    public Date getDate() {
        return date;
    }
    public String getDescription() {
        return description;
    }
    public Paziente getpaziente() {
        return paziente;
    }
    public Medico getmedico() {
        return medico;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setpaziente(Paziente paziente) {
        this.paziente = paziente;
    }
    public void setmedico(Medico medico) {
        this.medico = medico;
    }
}
