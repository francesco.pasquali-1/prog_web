package pasqualini.schivalocchi.progweb.mainclasses;

public class Ssn extends Utente {
    private String nome;

    public Ssn(String id, String fotoPath, String email, String nome) {
        super(id, fotoPath, email);
        this.id = id;
        this.nome = nome;
    }

    public String getId() {
        return id;
    }
    public String getNome() {
        return nome;
    }
}
