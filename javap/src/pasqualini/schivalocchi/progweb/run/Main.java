package pasqualini.schivalocchi.progweb.run;

import pasqualini.schivalocchi.progweb.DAO.PasswordDAO;
import pasqualini.schivalocchi.progweb.DAO.PazienteDAO;
import pasqualini.schivalocchi.progweb.mainclasses.*;

public class Main {
    public static void main (String args[]){
        PasswordDAO p = new PasswordDAO();
        boolean pa = p.getAccess("sigismondo.mezzera538788@hospital.com", "123456789", 0);
        PasswordDAO m = new PasswordDAO();
        boolean ma = m.getAccess("libero.mirra297034@hospital.coma", "123456789", 1);

        PazienteDAO pf = new PazienteDAO();
        for (PrescrizioneFarmaco prescrizioneFarmaco : pf.getMyPrescrizioniFarmaci("paz_4")){
            System.out.println(prescrizioneFarmaco);
        }

        for (ErogazioneEsame prescrizioneEsame : pf.getMyErogazioni("paz_81")){
            System.out.println(prescrizioneEsame);
        }

//        ListPazientiDAO ld = new ListPazientiDAO();
//        for(Paziente paz : ld.getMyPazienti("med_2")){
//            System.out.println(paz);
//        }
//
//        ListFarmaciDAO lf = new ListFarmaciDAO();
//        for(Farmaco farmaco : lf.getListFarmaci("Zy")){
//            System.out.println(farmaco);
//        }
//
//        ListEsamiDAO le = new ListEsamiDAO();
//        for(Esame esame : le.getListEsami("")){
//            System.out.println(esame);
//        }

    }
}
