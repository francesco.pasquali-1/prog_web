package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.Pazienti;
import pasqualini.schivalocchi.progweb.mainclasses.Medico;
import pasqualini.schivalocchi.progweb.mainclasses.Paziente;
import pasqualini.schivalocchi.progweb.mainclasses.VisitaBase;

import java.sql.*;
import java.util.Date;

public class VisitaBaseDAO {
    public VisitaBase getVisita (String id){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_base WHERE vid = '" + id + "';";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            Date date = rs.getDate("date");
            String description = rs.getString("description");

            String pid = rs.getString("pid");
            String mid = rs.getString("mid");
            Medico m = new MedicoDAO().getMedico(mid);

            return new VisitaBase(id, date, description, Pazienti.getPaziente(pid), m);
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return null;
    }

    // ADD
    public boolean addVisitaBase(VisitaBase v){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "INSERT INTO visita_base (vid, pid, mid, date, description) VALUES(" + v.getId() + "," +
                    v.getpaziente().getId() + "," + v.getmedico().getId() + "," + v.getDate() + "," +
                    v.getDescription() + ")";
            ResultSet rs = stmt.executeQuery(query);


        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }
}
