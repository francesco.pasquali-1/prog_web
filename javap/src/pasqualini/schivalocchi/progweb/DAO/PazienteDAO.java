package pasqualini.schivalocchi.progweb.DAO;
import java.sql.*;
import java.util.ArrayList;

import pasqualini.schivalocchi.progweb.generalclass.Pazienti;
import pasqualini.schivalocchi.progweb.generalclass.Tickets;
import pasqualini.schivalocchi.progweb.mainclasses.*;

/*
* TODO:
*   Close all connections
* */

public class PazienteDAO {
    public Paziente getPaziente(String id){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query = "SELECT * FROM utenti as u INNER JOIN pazienti as p ON u.uid = p.pid WHERE u.uid='" + id + "';";

            ResultSet rs_id = stmt.executeQuery( query );
            rs_id.next();

            // return le informazioni del paziente se la password e' corretta (null altrimenti)
            String fotoPaht = rs_id.getString("ufoto");
            String email = rs_id.getString("uemail");
            String nome = rs_id.getString("pnome");
            String conome = rs_id.getString("pcognome");
            String sesso = rs_id.getString("psesso");
            String cf = rs_id.getString("pcodice_fiscale");
            String luogoNascita = rs_id.getString("pluogo_nascita");
            Date dataNascita = rs_id.getDate("pdata_nascita");
            String provincaResidenza = rs_id.getString("pprovincia_residenza");
            return new Paziente(id, fotoPaht, email, nome, conome, sesso, cf, luogoNascita, dataNascita, provincaResidenza);
        } catch (Exception e){
            System.err.println("PazienteDAO getPaziente Error: " + e);
        }
        return null;
    }
    public ArrayList<PrescrizioneEsame> getMyPrescrizioniEsami(String id){
        ArrayList<PrescrizioneEsame> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT prescid, v.vid, v.eid " +
                    "FROM (prescrizione_esami as p INNER JOIN visita_base as v ON p.vid = v.vid) " +
                    "WHERE v.pid='" + id + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String prescid = rs.getString("prescid");
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                prescrizioni.add(new PrescrizioneEsame(prescid, v, e));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyEsamiDaFareDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ArrayList<ErogazioneEsame> getMyErogazioni(String id){
        ArrayList<ErogazioneEsame> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM erogazione_esami as e INNER JOIN " +
                    "(SELECT * FROM prescrizione_esami as p INNER JOIN visita_base as v ON p.vid=v.vid) as p " +
                    "ON e.prescid=p.prescid WHERE p.pid='" + id + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String prescid = rs.getString("prescid");
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                PrescrizioneEsame p = new PrescrizioneEsame(prescid, v, e);
                prescrizioni.add(new ErogazioneEsame(p, Tickets.getTicket(rs.getString("tid")), new SsnDAO().getSSN(rs.getString("sid"))));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyEsamiFattiDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ArrayList<PrescrizioneFarmaco> getMyPrescrizioniFarmaci(String id){
        ArrayList<PrescrizioneFarmaco> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT v.vid, fid, quantity " +
                    "FROM (prescrizione_farmaci as p INNER JOIN visita_base as v ON p.vid = v.vid) " +
                    "WHERE v.pid='" + id + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
                prescrizioni.add(new PrescrizioneFarmaco(v, f, rs.getInt("quantity")));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyPrescirioniFarmaciDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ArrayList<VisitaBase> getVisiteBase(String id){
        ArrayList<VisitaBase> visiteBase = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = null;
            query = "SELECT * FROM visite_base WHERE pid='" + id + "';";


            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(rs.getString("mid"));
                Paziente p = Pazienti.getPaziente(id);
                Date data = rs.getDate("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(id, paz) Error: " + e);
        }
        return visiteBase;
    }
    public ArrayList<VisitaSpecialistica> getVisitaSpecialistica(String id){
        ArrayList<VisitaSpecialistica> visiteSpecialistiche = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = null;
            query = "SELECT * FROM visite_specialistiche WHERE pid='" + id + "';";

            visiteSpecialistiche = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                Paziente p = this.getPaziente(rs.getString("pid"));
                Medico m = new MedicoDAO().getMedico(rs.getString("mid"));
                Ticket t = new TicketDAO().getTicket(rs.getString("tid"));
                visiteSpecialistiche.add(new VisitaSpecialistica(p,m,t, rs.getString("description")));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return visiteSpecialistiche;
    }

    // UPDATES
    public Paziente updateEmail(String pid, String email){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE pazienti SET pemail='" + email + "' WHERE pid='" + pid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Paziente p = Pazienti.getPaziente(pid);
            p.setEmail(email);
            return Pazienti.updatePaziente(p);
        } catch (Exception e){
            System.err.println("PazienteDAO updateEmail Error: " + e);
        }
        return null;
    }
    public Paziente updateProvinciaResidenza(String pid, String provinciaResidenza) {
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE pazienti SET pprovincia_residenza='" + provinciaResidenza + "' WHERE pid='" + pid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Paziente p = Pazienti.getPaziente(pid);
            p.setProvincaResidenza(provinciaResidenza);
            return Pazienti.updatePaziente(p);
        } catch (Exception e){
            System.err.println("PazienteDAO updateEmail Error: " + e);
        }
        return null;
    }
}
