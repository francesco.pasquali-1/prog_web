package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.GeneralMethods;

import java.security.NoSuchAlgorithmException;
import java.sql.*;

public class PasswordDAO {
    /*
    * paziente == 0: paziente
    *             1: medico
    *             2: ssn
    * */
    public static boolean getAccess(String email, String password, int paziente){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();
            String query_password = "SELECT uid, upassword FROM utenti WHERE uemail='" + email + "';";
            ResultSet rd_pssw = stmt.executeQuery(query_password);
            String passwd = null, passed_hash = null;
            while (rd_pssw.next()){
                if (paziente == 0 && rd_pssw.getString("uid").startsWith("paz_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);
                } else if (paziente == 1 && rd_pssw.getString("uid").startsWith("med_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);

                } else if (paziente == 2 && rd_pssw.getString("uid").startsWith("ssn_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);
                }
            }
            if (passwd != null && passwd.equals(passed_hash)) {
                System.out.println("Password Accepted");
                return true;
            } else {
                System.err.println("Password Rejected");
                return false;
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("PasswordDAO Error: " + e);
            return false;
        }
    }

    // UPDATE
    public boolean updatePassword(String id, String password){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String hash = GeneralMethods.getSHA512(password);
            String query_id = "UPDATE utenti SET upassword='" + hash + "' WHERE uid='" + id + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            String check_query = "SELECT upassword FROM utenti WHERE uid='" + id + "';";
            ResultSet rs_check = stmt.executeQuery( check_query);
            if (rs_check.next()) {
                if (rs_check.getString("upassword").equals(GeneralMethods.getSHA512(password))){
                    return false;
                }
            }
        } catch (Exception e){
            System.err.println("UpdatePasswordDAO updatePassword Error: " + e);
        }
        return false;
    }
}
