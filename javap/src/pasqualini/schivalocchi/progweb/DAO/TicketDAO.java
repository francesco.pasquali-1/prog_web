package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.Tickets;
import pasqualini.schivalocchi.progweb.mainclasses.Ticket;

import java.sql.*;

public class TicketDAO {
    public Ticket getTicket(String id) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = null;
            query = "SELECT * FROM ticket WHERE tid='" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return new Ticket(rs.getString("tid"), rs.getFloat("tprice"), rs.getBoolean("tpaid"));
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}