package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.Pazienti;
import pasqualini.schivalocchi.progweb.mainclasses.Medico;
import pasqualini.schivalocchi.progweb.mainclasses.Paziente;
import pasqualini.schivalocchi.progweb.mainclasses.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

/*
* TODO:
*
* */


public class ListVisiteBaseDAO {
    ArrayList<VisitaBase> visiteBase = null;

    public ArrayList<VisitaBase> getVisiteBase(String pid, String mid){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visite_base WHERE pid='" + pid + "' and mid='" + mid + "';";

            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(mid);
                Paziente p = Pazienti.getPaziente(pid);
                Date data = rs.getDate("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(pid,mid) Error: " + e);
        }
        return visiteBase;
    }
}
