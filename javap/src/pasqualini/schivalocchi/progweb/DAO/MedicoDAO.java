package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.Pazienti;
import pasqualini.schivalocchi.progweb.mainclasses.Medico;
import pasqualini.schivalocchi.progweb.mainclasses.Paziente;
import pasqualini.schivalocchi.progweb.mainclasses.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

public class MedicoDAO {
    public Medico getMedico(String id){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM utenti as u INNER JOIN medici as m ON u.uid = m.mid WHERE mid='" + id + "';";

            ResultSet rs_id = stmt.executeQuery( query );
            rs_id.next();

            String fotoPaht = rs_id.getString("ufoto");
            String email = rs_id.getString("uemail");
            String nome = rs_id.getString("mnome");
            String cognome = rs_id.getString("mcognome");
            String sesso = rs_id.getString("msesso");
            String cf = rs_id.getString("mcodice_fiscale");
            String provincia_operato = rs_id.getString("mprovincia_operato");
            return new Medico(id, fotoPaht, email, nome, cognome, sesso, cf, provincia_operato);
        } catch (Exception e){
            System.err.println("MedicoDAO getMedico Error: " + e);
        }
        return null;
    }
    public ArrayList<VisitaBase> getVisiteBase(String id){
        ArrayList<VisitaBase> visiteBase = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = null;
            query = "SELECT * FROM visite_base WHERE mid='" + id + "';";

            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(id);
                Paziente p = Pazienti.getPaziente(rs.getString("pid"));
                Date data = rs.getDate("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(id, paz) Error: " + e);
        }
        return visiteBase;
    }

    // UPDATE
    public Medico updateEmail(String mid, String email){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE medici SET memail='" + email + "' WHERE mid='" + mid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Medico m = new MedicoDAO().getMedico(mid);
            m.setEmail(email);
            return m;
        } catch (Exception e){
            System.err.println("UpdateMedicoDAO updateEmail Error: " + e);
        }
        return null;
    }
    public Medico updateProvinciaOperato(String mid, String provinciaOperato){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE medici SET mprovincia_operato='" + provinciaOperato + "' WHERE mid='" + mid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Medico m = new MedicoDAO().getMedico(mid);
            m.setProvinciaOperato(provinciaOperato);
            return m;
        } catch (Exception e){
            System.err.println("UpdateMedicoDAO updateProvinciaOperato Error: " + e);
        }
        return null;
    }
}
