package pasqualini.schivalocchi.progweb.DAO;

import pasqualini.schivalocchi.progweb.generalclass.Tickets;
import pasqualini.schivalocchi.progweb.mainclasses.Paziente;
import pasqualini.schivalocchi.progweb.mainclasses.PrescrizioneEsame;
import pasqualini.schivalocchi.progweb.mainclasses.Ssn;
import pasqualini.schivalocchi.progweb.mainclasses.Ticket;

import java.sql.*;
import java.util.ArrayList;

public class SsnDAO {
    public Ssn getSSN(String id){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM ssn as s INNER JOIN utenti as u ON s.sid=u.uid WHERE sid='" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return new Ssn(id, rs.getString("ufoto"), rs.getString("uemail"), rs.getString("snome"));
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<PrescrizioneEsame> getEsamiDaFare(String pid){
        ArrayList<PrescrizioneEsame> prescrizioni = null;
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();
            String query = "select prescid, p.vid, eid from prescrizione_esami as p inner join " +
                    "(select * from visita_base where pid = \'" + pid + "\') as v on p.vid=v.vid;";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                prescrizioni.add(new PrescrizioneEsame(rs.getString("pid"),
                                new VisitaBaseDAO().getVisita(rs.getString("vid")),
                                new EsameDAO().getEsame(rs.getString("eid"))));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean paidTicket(String tid){
        Ticket t = Tickets.getTicket(tid);
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "UPDATE ticket SET tpaid = true WHERE tid = '" + t.getId() + "';";
            t.setPaid(true);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // ADD
    public boolean addErogazioneEsame(){
        return false;
    }
}
