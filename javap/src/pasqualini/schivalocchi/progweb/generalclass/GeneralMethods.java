package pasqualini.schivalocchi.progweb.generalclass;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GeneralMethods {
    public static String getSHA512(String password){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());

            StringBuffer sb = new StringBuffer();
            for (byte b : md.digest()) {
                sb.append(String.format("%02x", b & 0xff));
            }

            System.out.println(sb);
            return new String(sb);
        } catch (NoSuchAlgorithmException e){
            System.out.println("NoSuchAlgorithmException SHA-512");
        }
        return null;
    }
}
