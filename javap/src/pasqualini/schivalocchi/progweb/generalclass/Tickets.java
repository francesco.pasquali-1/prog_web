package pasqualini.schivalocchi.progweb.generalclass;

import pasqualini.schivalocchi.progweb.DAO.TicketDAO;
import pasqualini.schivalocchi.progweb.mainclasses.Ticket;


import java.util.HashMap;

public class Tickets {
    private static HashMap<String, Ticket> tickets = new HashMap<>();

    public static Ticket getTicket(String tid){
        return tickets.containsKey(tid) ? tickets.get(tid) : new TicketDAO().getTicket(tid);
    }
    public static boolean addTicket(Ticket t){
        return tickets.put(t.getId(), t) != null;
    }
}
