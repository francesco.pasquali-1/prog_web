import requests
from bs4 import BeautifulSoup

alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
            'r','s','t','u','v','w','x','y','z']
farmaci = []

for c in alphabet:
    sent = False
    req = requests.get('https://www.drugs.com/alpha/{}.html'.format(c))

    html = BeautifulSoup(req.content, 'html.parser')
    for name in html.find_all('li'):
        if '<li class=""><span id="letter' in str(name):
            sent = False
        if sent:
            farmaci.append(name.text)
        if 'Professional Monographs' in name.text:
            sent = True

with open('farmaci.csv','w') as f:
    for farmaco in farmaci:
        print(farmaco)
        f.write('%s,\n' % farmaco)