import requests
from bs4 import BeautifulSoup, NavigableString

links = [
    "https://www.nomix.it/nomi-italiani-maschili-e-femminili.php",
    # "https://www.nomix.it/nomi-italiani-lettera-B.php",
    # "https://www.nomix.it/nomi-italiani-lettera-C.php",
    # "https://www.nomix.it/nomi-italiani-lettera-D.php",
    # "https://www.nomix.it/nomi-italiani-lettera-E.php",
    # "https://www.nomix.it/nomi-italiani-lettera-F.php",
    # "https://www.nomix.it/nomi-italiani-lettera-G.php",
    # "https://www.nomix.it/nomi-italiani-lettera-I.php",
    # "https://www.nomix.it/nomi-italiani-lettera-L.php",
    # "https://www.nomix.it/nomi-italiani-lettera-M.php",
    # "https://www.nomix.it/nomi-italiani-lettera-NO.php",
    # "https://www.nomix.it/nomi-italiani-lettera-PQ.php",
    # "https://www.nomix.it/nomi-italiani-lettera-R.php",
    # "https://www.nomix.it/nomi-italiani-lettera-S.php",
    # "https://www.nomix.it/nomi-italiani-lettera-TUV.php",
    # "https://www.nomix.it/nomi-italiani-lettera-WZ.php"
]


maschili = []
femminili = []
for link in links:
    req = requests.get(link)

    html = BeautifulSoup(str(req.content).replace("\\n", " "), 'html.parser')

    count = 0
    for tables in html.find_all('table'):
        count +=1
        for table in tables:
            for name in table.td:
                if type(name) is NavigableString:
                    n = name
                    ## print(name)
                else:
                    n = name.text
                    ## print(name.text)
                if count == 1:
                    maschili.append(n)
                elif count == 2:
                    femminili.append(n)
                else:
                    print("______________WHAT IS THIS?______________")
    
print("maschili")
for name in maschili:
    print(name)
print("\n\nfemminili")
for name in femminili:
    print(name)

with open("maschili.csv","w") as fm:
    for name in maschili:
        fm.write("%s;\n" % name)

with open("femminili.csv","w") as ff:
    for name in femminili:
        fm.write("%s;\n" % name)