import sqlite3

def get_maschili(db, cursor):
    query = "SELECT * FROM surnames;"
    cursor.execute(query)
    mnames = cursor.fetchall()
    return mnames

if __name__=="__main__":
    db = sqlite3.connect('names_surnames.sql3')
    cursor = db.cursor()

    print(get_maschili(db, cursor))

    db.close()