import requests
from bs4 import BeautifulSoup

links = [
    "https://www.ganino.com/cognomi_italiani_a",
    "https://www.ganino.com/cognomi_italiani_b",
    "https://www.ganino.com/cognomi_italiani_c",
    "https://www.ganino.com/cognomi_italiani_d",
    "https://www.ganino.com/cognomi_italiani_e",
    "https://www.ganino.com/cognomi_italiani_f",
    "https://www.ganino.com/cognomi_italiani_g",
    "https://www.ganino.com/cognomi_italiani_h",
    "https://www.ganino.com/cognomi_italiani_i",
    "https://www.ganino.com/cognomi_italiani_j",
    "https://www.ganino.com/cognomi_italiani_k",
    "https://www.ganino.com/cognomi_italiani_l",
    "https://www.ganino.com/cognomi_italiani_m",
    "https://www.ganino.com/cognomi_italiani_n",
    "https://www.ganino.com/cognomi_italiani_o",
    "https://www.ganino.com/cognomi_italiani_p",
    "https://www.ganino.com/cognomi_italiani_q",
    "https://www.ganino.com/cognomi_italiani_r",
    "https://www.ganino.com/cognomi_italiani_s",
    "https://www.ganino.com/cognomi_italiani_t",
    "https://www.ganino.com/cognomi_italiani_u",
    "https://www.ganino.com/cognomi_italiani_v",
    "https://www.ganino.com/cognomi_italiani_w",
    "https://www.ganino.com/cognomi_italiani_x",
    "https://www.ganino.com/cognomi_italiani_z"

]

cognomi = []

for link in links:
    print(link)
    req = requests.get(link, verify=False)

    if req.status_code != 200:
        print("Status code: %s." % req.status_code)
        exit(1)

    html = BeautifulSoup(req.content, "html.parser")

    # for table in html.table:
    table = html.table
    for tr in table.find_all("tr"):
        if tr is not None and tr.td is not None:
            text = tr.td.text
            text = text.split("\n")
            for t in text:
                if t != "" and t != " " and t != "\n":
                    cognomi.append(t)

for cognome in cognomi:
    print(cognome)

with open("cognomi.csv", "w") as fc:
    for cognome in cognomi:
        fc.write("%s;\n" % cognome)