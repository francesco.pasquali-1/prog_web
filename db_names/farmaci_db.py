import sqlite3

_create = True

def create_table(db, cursor):
    query = "CREATE TABLE farmaci (\
        name VARCHAR(40) NOT NULL,\
        code VARCHAR(4) PRIMARY KEY\
        )"

    cursor.execute(query)
    db.commit()

def popolate(db, cursor, farmaci):
    cursor.executemany("INSERT INTO farmaci (name, code)\
        VALUES (?,?)", farmaci)
    db.commit()

if __name__=='__main__':
    db = sqlite3.connect('farmaci.sql3')
    cursor = db.cursor()

    if _create:
        print('Creating table...')
        create_table(db, cursor)

    print('Reading file...')
    farmaci = []
    count = 0
    with open('farmaci.csv', 'r') as f:
        string = f.readline()
        while string != "":
            count += 1
            string = string.replace(";", "").replace("\n", "")
            farmaci.append((string, count))
            string = f.readline()

    print('Popolating...')
    popolate(db, cursor, farmaci)

    print('Finished.')