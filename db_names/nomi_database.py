import sqlite3

_create = True

def create_table(db, cursor):
    query = "CREATE TABLE names (\
        name VARCHAR(50) PRIMARY KEY,\
        gender CHAR(1) NOT NULL,\
        country VARCHAR(50) NOT NULL\
        );"
    cursor.execute(query)
    db.commit()
    query = "CREATE TABLE surnames (\
        surname VARCHAR(50),\
        country VARCHAR(50)\
        );"
    cursor.execute(query)
    db.commit()

def popolate_name(db, cursor, names):
    cursor.executemany("INSERT INTO names (name, gender, country)\
        VALUES (?,?,?)", names)
    db.commit()

def popolate_surname(db, cursor, surnames):
    cursor.executemany("INSERT INTO surnames (surname, country)\
        VALUES (?,?)", surnames)
    db.commit()

if __name__=="__main__":
    db = sqlite3.connect('names_surnames.sql3')
    cursor = db.cursor()

    if _create:
        create_table(db, cursor)

    maschili = []
    with open("maschili.csv", "r") as f:
        string = f.readline()
        while string != "":
            string = string.replace(";", "").replace("\n", "")
            maschili.append((string, 'm', 'Italy'))
            string = f.readline()

    popolate_name(db, cursor, maschili)
    
    femminili = []
    with open("femminili.csv", "r") as f:
        string = f.readline()
        while string != "":
            string = string.replace(";", "").replace("\n", "")
            femminili.append((string, 'f', 'Italy'))
            string = f.readline()

    popolate_name(db, cursor, femminili)

    cognomi = []
    with open("cognomi.csv", "r") as f:
        string = f.readline()
        while string != "":
            string = string.replace(";", "").replace("\n", "")
            cognomi.append((string, 'Italy'))
            string = f.readline()
    
    popolate_surname(db, cursor, cognomi)

    db.close()