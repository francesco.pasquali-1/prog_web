<%--
  Created by IntelliJ IDEA.
  User: pasqui
  Date: 1/18/20
  Time: 3:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<title>my ajax</title>
<form class="form-horizontal" role="form" autocomplete="off" method="post">&nbsp;
    <div class="col-lg-9"><input id="username" class="form-control" required="" type="text" placeholder="Username" /></div>
    <div class="form-group">

        <div class="col-sm-offset-3 col-sm-9" id="ajaxResponse"><button class="btn btn-success btn-sm" type="button" onclick="valUser()">Sign in</button></div>

    </div>
    <script type="text/javascript">
        function valUser() {
            if (null == $('#username').val() || $('#username').val() == '') {
                alert("enter username");
                return false;
            } else {
//alert("ajax");
                var name = $('#username').val();
                var dataString = "username=" + name;
                $.ajax({
                    type : "POST",
                    url : "fittizia",
                    data : dataString,
                    dataType : "text",
                    success : function(data) {
// alert("success data :"+data);
                        var ajaxResponse = $("#ajaxResponse").html(data)
                            .text();
                    },
//If we get any error from the server
                    error : function(jqXHR, textStatus, errorThrown) {
                        console.log("Something really bad happened "
                            + textStatus + ";" + errorThrown);

                    }
                });
            }
        }
    </script>

</form>
</body>
</html>
