<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Resume - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="login">
      <span class="d-block d-lg-none">${medico.getNome()} ${medico.getCognome()}</span>
      <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2"
               src="${medico.getFotoPath()}" alt="${medico.getFotoPath()}">
        </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <h3>${medico.getNome()} ${medico.getCognome()}</h3>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <a href="login">HOME</a>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <h3>SETTINGS</h3>
        <div>
          <p>
            Nome: ${medico.getNome()}<br>
            Cognome: ${medico.getCognome()}<br>
            E-mail: ${medico.getEmail()}<br>
            Residenza: ${medico.getProvinciaOperato()}<br>
          </p>

          <p style="color: red;">
            ${change_password}
          </p>

          <p onclick="change_password()">
            Cambia Password
          </p>

          <div id="password_changed" style="color: red; font-style: italic;">

          </div>
          <div id="change_password_div" style="visibility: hidden;">
            <input id="id_pssw" type="hidden" value="${medico.getId()}" />
            Old password: <input id="old_password" size="35" type="password" name="old_password" value="" /> <br>
            New password: <input id="new_password" size="35" type="password" name="new_password" value="" /> <br>
            Confirm password: <input id="confirm_password" size="35" type="password" name="confirm_password" value="" /> <br>
            <button onclick="changePassword()">Submit</button>
          </div>

          <script>
            function change_password(){
              var password_changed = $("#password_changed").html("")
                      .text();
              document.getElementById("change_password_div").style.visibility = "visible";
              document.getElementById("change_medico_div").style.visibility = "hidden";
            }
            function changePassword() {
              var password_changed = $("#password_changed").html("")
                      .text();
              if (null == $('#old_password').val() || $('#old_password').val() == '') {
                alert("enter username");
                return false;
              } else {
                var id = $('#id_pssw').val();
                var old_password = $('#old_password').val();
                var new_password = $('#new_password').val();
                var confirm_password = $('#confirm_password').val();
                var dataString =  "id=" + id
                                  + "&old_password=" + old_password
                                  + "&new_password=" + new_password
                                  + "&confirm_password=" + confirm_password;
                $.ajax({
                  type : "POST",
                  url : "change_password",
                  data : dataString,
                  dataType : "text",
                  success : function(data) {
                    var password_changed = $("#password_changed").html(data)
                            .text();
                  },
                  error : function(jqXHR, textStatus, errorThrown) {
                    var password_changed = $("#password_changed").html(data)
                            .text();
                  }
                });
              }
            }
          </script>
        </div>
      </div>
    </section>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
