<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Home - Servizio Sanitario Nazionale</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

  <style>
    #login-background {
      background-color: #333;
      position: fixed;
      top: -500px;
      display: block;
      transition: top 0.3s;
    }
    #login {
      background-color: black;
      position: center;
      opacity: 1.0;
    }
  </style>

</head>

<c:choose>
  <c:when test="${error == null}">
    <body id="page-top">
  </c:when>
  <c:when test="${error != null}">
    <body id="page-top" onload="getLoginForm()">
  </c:when>
</c:choose>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <div>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" style="color: black;" onclick="getLoginForm()">Login</a>
        </li>
      </ul>
    </div>
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <span class="d-block d-lg-none">Clarence Taylor</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/unknown.png" alt="">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#news">News</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#servizio">servizio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#developer">Developer</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="news">
      <div class="w-100">
        <h1 class="mb-0">NEWS</h1>
        <p class="lead mb-5">
          <span>
            <a href="https://edition.cnn.com/2019/12/11/health/physical-activity-food-labels-wellness-scli-intl-gbr/index.html">
              <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/health1.jpeg">
            </a>
          </span>
          <span>
            <a href="https://edition.cnn.com/2019/10/18/health/exercise-breakfast-fat-burn-wellness/index.html">
              <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/health2.jpeg">
            </a>
          </span>
          <span>
            <a href="https://edition.cnn.com/2019/08/09/health/plant-fake-meat-burgers-good-for-you-or-not/index.html">
              <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/health3.jpeg">
            </a>
          </span>
        </p>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="servizio">
      <div class="w-100">
        <h1>Il servizio</h1>
        <h3>Per i cittadini:</h3>
        <p class="lead mb-5">
          Questo servizio offre ai cittadini un buon approccio al sistema sanitario comodamente da casa propria o da
          qualsiasi punto ci si trova. <br>
          Senza bisogno di compromessi &egrave; possibile accedere alle ricette ed ai report delle visite con un Click. <br>
          Inoltre &egrave; possibile controllare la quantit&agrave; di farmaci prescritti e quelli da andare a prendere
          e di vedere gli esami prescritti e quelli erogati con eventuali report.
        </p>
        <br><br>
        <h3>Per i medici</h3>
        <p class="lead mb-5">
          Questo servizio offre ai medici un approccio pi&ugrave; tecnologico e rapido al cittadino guardando prontamente
          le visite, gli esami ed i farmaci di un paziente in maniera affidabile.
          Un medico pu&ograve; inoltre erogare i propri servizi senza dover preoccuparsi di avere abbastanza fogli per
          le ricette e senza dover riscrivere pi&ugrave; volte una ricetta per un errore.
        </p>
        <br><br>
        <h3>Per il servizio sanitario provinciale</h3>
        <p class="lead mb-5">
          Questo servizio offre al servizio sanitario provinciale la visione delle prestazioni erogate fin d'ora dai vari
          medici e pu&ograve; erogare esami.
        </p>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="developer">
      <div class="w-100">
        <h1 class="mb-0">Francesco
          <span class="text-primary">Pasqualini</span>
        </h1>
        <div class="subheading mb-5">Universit&agrave; degli Studi di Trento · Informatica
          <a href="mailto:name@email.com">francesco.pasquali-1@studenti.unitn.it</a>
        </div>
        <p class="lead mb-5">
          Lo sviluppatore di questo sito web lavora attualmente nell'unit&agrave; ES (Embedded System) della Fondazione
          Bruno Kessler. <br>
          Inoltre insegna annualmente (dal 2016 ad oggi) all'Istituto Tecnico Industriale "G. Marconi" di Verona tenendo
          un corso VoIP di un sistema di gestione delle chiamate Open Source. <br>
          Francesco ha passione per la sicurezza informatica nonch&egrave; questa passione lo ha guidato ad iscriversi
          all'Universit&agrave; degli Studi di Trento - Informatica con possibile Specializzazione in Cyber Security.
        </p>
        <div class="social-icons">
          <a href="https://www.linkedin.com/in/francesco-pasqualini-994812134/">
            <i class="fab fa-linkedin-in"></i>
          </a>
          <a href="https://gitlab.com/francesco.pasquali-1">
            <i class="fab fa-github"></i>
          </a>
          <a href="https://twitter.com/PasquiJr">
            <i class="fab fa-twitter"></i>
          </a>
        </div>
      </div>
    </section>

  </div>

  <div id="login-background" style="background-color: black; position: center;">
    <div id="login">
      <form name="login" action="login" method="post">
        <c:choose>
          <c:when test="${error == null}">
            E-mail:<br>
            <input id="email" size="35" type="text" name="email" value="">
          </c:when>
          <c:when test="${error != null}">
            <div style="color: red">${error}</div>
            E-mail:<br>
            <input id="email" size="35" type="text" name="email" value="${email}">
          </c:when>
        </c:choose>
          <br>
          Password:<br>
          <input id="password" size="35" type="password" name="password" value="">
        <br><br>
        <select id="agent" name="agent" size="1">
          <option value="0" selected="selected">Paziente</option>
          <option value="1">Medico</option>
          <option value="2">Servizio Sanitario</option>
        </select>
        <input type="checkbox" name="remember_me" value="True"> Ricordami <br>
        <input type="submit" value="Submit">
      </form>
    </div>
  </div>

  <!-- My JavaScript functions -->
  <script src="functions.js"></script>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
