package pasqualini.progweb.model;

public class Esame {
    private String id;
    private String nome;

    public Esame(String id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
