package pasqualini.progweb.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ErogazioneEsame {
    private PrescrizioneEsame esame;
    private Ticket ticket;
    private Ssn ssn;
    private long date;
    private String desription;

    public ErogazioneEsame(PrescrizioneEsame esame, Ticket ticket, Ssn ssn, long date, String description) {
        this.esame = esame;
        this.ticket = ticket;
        this.ssn = ssn;
        this.date = date;
        this.desription = description;
    }

    public PrescrizioneEsame getEsame() {
        return esame;
    }
    public Ticket getTicket() {
        return ticket;
    }
    public Ssn getSsn() {
        return ssn;
    }
    public long getDate() {
        return this.date;
    }
    public String getData() {
        Date date=new Date(this.date);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        return df2.format(date);
    }
    public String getDescription() {
        return this.desription;
    }
}
