package pasqualini.progweb.model;

public class Ssn extends Utente {
    private String nome;
    private String provincia;

    public Ssn(String id, String fotoPath, String email, String nome, String provincia) {
        super(id, fotoPath, email);
        this.id = id;
        this.nome = nome;
        this.provincia = provincia;
    }

    public String getId() {
        return id;
    }
    public String getNome() {
        return nome;
    }
    public String getProvincia() {
        return provincia;
    }
}
