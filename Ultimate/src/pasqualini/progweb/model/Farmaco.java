package pasqualini.progweb.model;


public class Farmaco {
    final private String id;
    final private String nome;

    public Farmaco(String id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public String getId() {
        return id;
    }
    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return nome;
    }
}
