package pasqualini.progweb.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VisitaBase {
    private String id;
    private long date;
    private String description;
    private Paziente paziente;
    private Medico medico;

    public VisitaBase(String id, long date, String description, Paziente paziente, Medico medico) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.paziente = paziente;
        this.medico = medico;
    }

    public String getId() {
        return id;
    }
    public long getDate() {
        return date;
    }
    public String getData() {
        Date date=new Date(this.date);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        return df2.format(date);
    }
    public String getDescription() {
        return description;
    }
    public Paziente getPaziente() {
        return paziente;
    }
    public Medico getMedico() {
        return medico;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setDate(long date) {
        this.date = date;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setpaziente(Paziente paziente) {
        this.paziente = paziente;
    }
    public void setmedico(Medico medico) {
        this.medico = medico;
    }
}
