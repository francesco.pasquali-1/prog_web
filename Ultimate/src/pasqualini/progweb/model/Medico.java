package pasqualini.progweb.model;

public class Medico extends Utente {
    private String id;
    private String nome;
    private String cognome;
    private String sesso;
    private String cf;
    private String provinciaOperato;

    public Medico(String id, String email, String fotoPath, String nome, String cognome, String sesso, String cf, String provinciaOperato) {
        super(id, fotoPath, email);
        this.nome = nome;
        this.cognome = cognome;
        this.sesso = sesso;
        this.cf = cf;
        this.provinciaOperato = provinciaOperato;
    }

    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public String getNomeCognome(){
        return nome + " " + cognome;
    }
    public String getSesso() {
        return sesso;
    }
    public String getCf() {
        return cf;
    }
    public String getProvinciaOperato() {
        return provinciaOperato;
    }

    public void setProvinciaOperato(String provinciaOperato) {
        this.provinciaOperato = provinciaOperato;
    }
}
