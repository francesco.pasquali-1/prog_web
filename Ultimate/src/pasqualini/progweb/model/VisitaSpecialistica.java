package pasqualini.progweb.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class VisitaSpecialistica {
    private Paziente paziente;
    private Medico medico;
    private Ticket ticket;
    private String description;
    private long date;

    public VisitaSpecialistica(Paziente p, Medico m, Ticket t, String d, long date){
        this.paziente = p;
        this.medico = m;
        this.ticket = t;
        this.description = d;
        this.date = date;
    }

    public Paziente getPaziente() {
        return paziente;
    }
    public Medico getMedico() {
        return medico;
    }
    public Ticket getTicket() {
        return ticket;
    }
    public String getDescription() {
        return description;
    }
    public long getDate() {
        return this.date;
    }
    public String getData() {
        Date date=new Date(this.date);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        return df2.format(date);
    }

    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }
    public void setMedico(Medico medico) {
        this.medico = medico;
    }
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setDate(long date){
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitaSpecialistica that = (VisitaSpecialistica) o;
        return paziente.equals(that.paziente) &&
                medico.equals(that.medico) &&
                ticket.equals(that.ticket);
    }
    @Override
    public int hashCode() {
        return Objects.hash(paziente, medico, ticket);
    }

    @Override
    public String toString() {
        return "VisitaSpecialistica{" +
                "paziente=" + paziente +
                ", medico=" + medico +
                ", ticket=" + ticket +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
