package pasqualini.progweb.model;

public class Ticket {
    private String id;
    private float price;
    private boolean paid;

    public Ticket(String id, float price, boolean paid) {
        this.id = id;
        this.price = price;
        this.paid = paid;
    }

    public String getId() {
        return id;
    }
    public float getPrice() {
        return price;
    }
    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean isPaid){
        this.paid = isPaid;
    }
}
