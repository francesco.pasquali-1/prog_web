package pasqualini.progweb.model;

import pasqualini.progweb.DAO.ErogazioneFarmacoDAO;
import pasqualini.progweb.DAO.TicketDAO;
import pasqualini.progweb.DAO.VisitaBaseDAO;

import java.util.ArrayList;

public class Statistiche {
    private Ssn ssn;
    private long startDate;
    private long endDate;
    private ArrayList<VisitaBase> visiteBase;
    private ArrayList<ErogazioneFarmaco> erogazioneFarmaci;
    private ArrayList<Ticket> tickets;

    public Statistiche(Ssn ssn, long startDate, long endDate){
        this.ssn = ssn;
        this.startDate = startDate;
        this.endDate = endDate;

        this.visiteBase = new VisitaBaseDAO().getVisitaBaseTime(startDate, endDate, this.ssn.getProvincia());
        this.erogazioneFarmaci = new ErogazioneFarmacoDAO().getErogazioneFarmaciTime(startDate, endDate, this.ssn.getProvincia());
        this.tickets = new TicketDAO().getTicketDate(startDate, endDate, this.ssn.getProvincia());
    }

    public Ssn getSsn() {
        return ssn;
    }
    public void setSsn(Ssn ssn) {
        this.ssn = ssn;
    }
    public long getStartDate() {
        return startDate;
    }
    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }
    public long getEndDate() {
        return endDate;
    }
    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
    public ArrayList<VisitaBase> getVisiteBase() {
        return visiteBase;
    }
    public void setVisiteBase(ArrayList<VisitaBase> visiteBase) {
        this.visiteBase = visiteBase;
    }
    public ArrayList<ErogazioneFarmaco> getErogazioneFarmaci() {
        return erogazioneFarmaci;
    }
    public void setErogazioneFarmaci(ArrayList<ErogazioneFarmaco> erogazioneFarmaci) {
        this.erogazioneFarmaci = erogazioneFarmaci;
    }
    public ArrayList<Ticket> getTickets() {
        return tickets;
    }
    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }
}
