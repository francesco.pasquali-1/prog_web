package pasqualini.progweb.model;

public class PrescrizioneEsame {
    private String id;
    private VisitaBase visita;
    private Esame esame;

    public PrescrizioneEsame(String id, VisitaBase visita, Esame esame) {
        this.id = id;
        this.visita = visita;
        this.esame = esame;
    }

    public String getId() {
        return id;
    }
    public VisitaBase getVisita() {
        return visita;
    }
    public Esame getEsame() {
        return esame;
    }
}
