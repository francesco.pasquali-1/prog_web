package pasqualini.progweb.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ErogazioneFarmaco {
    private VisitaBase visita;
    private Farmaco farmaco;
    private long date;
    private int quantity;

    public ErogazioneFarmaco(VisitaBase visita, Farmaco farmaco, long date, int quantity) {
        this.visita = visita;
        this.farmaco = farmaco;
        this.date = date;
        this.quantity = quantity;
    }

    public VisitaBase getVisita() {
        return visita;
    }
    public Farmaco getFarmaco() {
        return farmaco;
    }
    public long getDate() {
        return date;
    }
    public String getData() {
        Date date=new Date(this.date);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        return df2.format(date);
    }
    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "PrescrizioneFarmaco{" +
                "farmaco=" + farmaco +
                ", date=" + date +
                '}';
    }
}
