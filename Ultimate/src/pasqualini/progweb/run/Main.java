package pasqualini.progweb.run;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import pasqualini.progweb.model.ErogazioneEsame;
import pasqualini.progweb.DAO.PasswordDAO;
import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.model.PrescrizioneFarmaco;
import pasqualini.progweb.model.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    public static void main (String args[]){
        /*PasswordDAO p = new PasswordDAO();
        boolean pa = p.getAccess("sigismondo.mezzera538788@hospital.com", "123456789", 0);
        PasswordDAO m = new PasswordDAO();
        boolean ma = m.getAccess("libero.mirra297034@hospital.coma", "123456789", 1);

        PazienteDAO pf = new PazienteDAO();
        for (PrescrizioneFarmaco prescrizioneFarmaco : pf.getMyPrescrizioniFarmaci("paz_4")){
            System.out.println(prescrizioneFarmaco);
        }

        for (ErogazioneEsame prescrizioneEsame : pf.getErogazioniEsame("paz_81")){
            System.out.println(prescrizioneEsame);
        }*/

        PazienteDAO pazienteDAO = new PazienteDAO();
        Paziente p = pazienteDAO.getPaziente("guerrino.angelica367473@hospital.com", false);

        try {
            Document document = new Document();

            // intestazione
            String filename = "./prova_pdf.pdf";
            PdfWriter.getInstance(document, new FileOutputStream(filename));
            document.open();
            Paragraph nome = new Paragraph("Paziente: " + p.getNomeCognome());
            Paragraph cf = new Paragraph("Codice Fiscale: " + p.getCf());
            document.add(nome);
            document.add(cf);

            PdfPTable table = new PdfPTable(2);

            // prima riga (Medico)
            table.addCell("Medico Visita:");
            table.addCell(pazienteDAO.getMyMedico(p.getId()).getNomeCognome());

            // seconda riga (Data)
            table.addCell("Data Visita: ");
            table.addCell("08/05/1998");

            // terza riga (Descrizione)
            table.addCell("Descrizione: ");
            table.addCell("QUesta è una visita medica");

            document.add(table);


            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }

//        ListPazientiDAO ld = new ListPazientiDAO();
//        for(Paziente paz : ld.getMyPazienti("med_2")){
//            System.out.println(paz);
//        }
//
//        ListFarmaciDAO lf = new ListFarmaciDAO();
//        for(Farmaco farmaco : lf.getListFarmaci("Zy")){
//            System.out.println(farmaco);
//        }
//
//        ListEsamiDAO le = new ListEsamiDAO();
//        for(Esame esame : le.getListEsami("")){
//            System.out.println(esame);
//        }

    }
}
