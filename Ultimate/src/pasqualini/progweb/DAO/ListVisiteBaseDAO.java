package pasqualini.progweb.DAO;

import pasqualini.progweb.generalclass.Pazienti;
import pasqualini.progweb.model.Medico;
import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.model.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

/*
* TODO:
*
* */


public class ListVisiteBaseDAO {
    ArrayList<VisitaBase> visiteBase = null;

    public ArrayList<VisitaBase> getVisiteBase(String pid, String mid){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_base WHERE pid='" + pid + "' and mid='" + mid + "';";

            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(mid, true);
                Paziente p = Pazienti.getPaziente(pid);
                long data = rs.getLong("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(pid,mid) Error: " + e);
        }
        return visiteBase;
    }
}
