package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Esame;

import java.sql.*;

public class EsameDAO {
    public Esame getEsame(String id) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();


            String query = "SELECT ename FROM esami WHERE eid='" + id + "';";
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                String ename = rs.getString("ename");
                return new Esame(id, ename);
            } else {
                System.out.println("Esame non trovato!");
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("EsameDAO Error: " + e);
        }
        return null;
    }
}