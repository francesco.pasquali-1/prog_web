package pasqualini.progweb.DAO;

import pasqualini.progweb.model.PrescrizioneEsame;
import pasqualini.progweb.model.*;

import java.sql.*;

public class PrescrizioneEsameDAO {
    public Integer getNextPrescrizione(){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT prescid FROM prescrizione_esami;";
            ResultSet rs = stmt.executeQuery(query);
            int last = 0;
            while(rs.next()){
                int curr = rs.getInt("prescid");
                if (last < curr){
                    last = curr;
                }
            }
            return last +1;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return null;
    }

    public boolean addPrescrizioneEsame(PrescrizioneEsame p){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO prescrizione_esami (prescid, vid, eid) VALUES('" + p.getId() + "','" +
                    p.getVisita().getId() + "','" + p.getEsame().getId() + "');";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }
}
