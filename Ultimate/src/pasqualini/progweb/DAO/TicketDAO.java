package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Ticket;

import java.sql.*;
import java.util.ArrayList;

public class TicketDAO {
    public Ticket getTicket(String id) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = null;
            query = "SELECT * FROM ticket WHERE tid='" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            rs.next();

            String tid = rs.getString("tid");
            float tprice = rs.getFloat("tprice");
            boolean tpaid = rs.getBoolean("tpaid");

            stmt.close();
            c.close();

            return new Ticket(tid, tprice, tpaid);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Return a new added ticket (It adds the new ticket to database automatically
     * */
    public Ticket getNewTicket(String tprice, boolean tpaid){
        Ticket t = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT tid FROM ticket;";
            ResultSet rs = stmt.executeQuery(query);
            int tid = 0;
            while(rs.next()){
                int curr = rs.getInt("tid");
                if (tid < curr){
                    tid = curr;
                }
            }

            t = new Ticket(String.valueOf(tid+1),Float.parseFloat(tprice), tpaid);

            this.addTicket(t);

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    public boolean addTicket(Ticket t){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO ticket (tid, tprice, tpaid) VALUES('" + t.getId() + "'," +
                    t.getPrice() + "," + t.isPaid() + ");";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }

    public ArrayList<Ticket> getTicketDate(long startDate, long endDate, String provincia) {
        ArrayList<Ticket> tickets = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String queryEsami = null;
            queryEsami = "SELECT * FROM ticket WHERE tid in (SELECT tid FROM erogazione_esami WHERE sid in (" +
                    "SELECT sid FROM ssn WHERE sprovincia = '" + provincia + "') " +
                    "and date > " + startDate + " and date < " + endDate +");";

            ResultSet rs = stmt.executeQuery(queryEsami);
            tickets = new ArrayList<>();
            while (rs.next()){
                String tid = rs.getString("tid");
                float tprice = rs.getFloat("tprice");
                boolean tpaid = rs.getBoolean("tpaid");
                tickets.add(new Ticket(tid, tprice, tpaid));
            }

            String querySpecialitica =" SELECT * FROM ticket WHERE tid in (SELECT tid FROM visita_specialistica WHERE mid in (" +
                        "SELECT mid FROM medici WHERE mprovincia_operato = '" + provincia + "') " +
                        "and date > " + startDate + " and date < " + endDate +");";

            ResultSet rsSpecialistica = stmt.executeQuery(querySpecialitica);
            while (rsSpecialistica.next()){
                String tid = rsSpecialistica.getString("tid");
                float tprice = rsSpecialistica.getFloat("tprice");
                boolean tpaid = rsSpecialistica.getBoolean("tpaid");
                tickets.add(new Ticket(tid, tprice, tpaid));
            }

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return tickets;
    }
}