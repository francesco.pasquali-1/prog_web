package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Farmaco;

import java.sql.*;

public class FarmacoDAO {
    public Farmaco getFarmaco(String id) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();


            String query = "SELECT fnome FROM farmaci WHERE fid='" + id + "';";
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                String fnome = rs.getString("fnome");
                return new Farmaco(id, fnome);
            } else {
                System.out.println("Farmaco non trovato!");
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("FarmacoDAO Error: " + e);
        }
        return null;
    }
}
