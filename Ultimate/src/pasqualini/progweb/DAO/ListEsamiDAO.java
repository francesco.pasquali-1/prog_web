package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Esame;

import java.sql.*;
import java.util.ArrayList;

public class ListEsamiDAO {
    private ArrayList<Esame> esami;

    public ListEsamiDAO(){
        this.esami = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM esami;";

            esami = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                String id = rs.getString("eid");
                String name = rs.getString("ename");
                esami.add(new Esame(id, name));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListEsamiDAO Constructor Error: " + e);
        }
    }

    public ArrayList<Esame> getListEsami(){
        return esami;
    }
    public ArrayList<Esame> getListEsami(String name){
        ArrayList<Esame> new_list = new ArrayList<>();
        for (Esame e : this.esami){
            if (e.getNome().startsWith(name)){
                new_list.add(e);
            }
        }
        return new_list;
    }
}
