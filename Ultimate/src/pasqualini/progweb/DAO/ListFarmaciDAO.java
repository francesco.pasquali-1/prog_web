package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Farmaco;

import java.sql.*;
import java.util.ArrayList;

public class ListFarmaciDAO {
    private ArrayList<Farmaco> farmaci;

    public ListFarmaciDAO(){
        this.farmaci = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM farmaci;";

            farmaci = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                String id = rs.getString("fid");
                String name = rs.getString("fnome");
                farmaci.add(new Farmaco(id, name));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListFarmaciDAO Constructor Error: " + e);
        }
    }

    public ArrayList<Farmaco> getListFarmaci(){
        return farmaci;
    }
    public ArrayList<Farmaco> getListFarmaci(String name){
        ArrayList<Farmaco> new_list = new ArrayList<>();
        for (Farmaco f : this.farmaci){
            if (f.getNome().startsWith(name)){
                new_list.add(f);
            }
        }
        return new_list;
    }
}
