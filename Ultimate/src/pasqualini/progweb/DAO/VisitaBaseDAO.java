package pasqualini.progweb.DAO;

import pasqualini.progweb.generalclass.Pazienti;
import pasqualini.progweb.model.*;

import java.sql.*;
import java.util.ArrayList;

public class VisitaBaseDAO {
    public Integer getNextVisita(){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT vid FROM visita_base;";
            ResultSet rs = stmt.executeQuery(query);
            int last = 0;
            while(rs.next()){
                int curr = rs.getInt("vid");
                if (last < curr){
                    last = curr;
                }
            }
            return last +1;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return null;
    }

    public VisitaBase getVisita (String id){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_base WHERE vid = '" + id + "';";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            long date = rs.getLong("date");
            String description = rs.getString("description");

            String pid = rs.getString("pid");
            String mid = rs.getString("mid");
            Medico m = new MedicoDAO().getMedico(mid, true);

            stmt.close();
            c.close();

            return new VisitaBase(id, date, description, Pazienti.getPaziente(pid), m);
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return null;
    }

    public ArrayList<VisitaBase> getVisitaBaseTime(long startDate, long endDate, String provincia){
        ArrayList<VisitaBase> visite = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            visite = new ArrayList<>();

            String query = "SELECT * FROM visita_base WHERE mid=(" +
                    "SELECT mid from medici WHERE mprovincia_operato='" + provincia + "') and date>" + startDate +
                    " or date< " + endDate + ";";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                String vid = rs.getString("vid");
                String pid = rs.getString("pid");
                String mid = rs.getString("mid");
                long date = rs.getLong("date");
                String description = rs.getString("description");

                visite.add(new VisitaBase(vid, date, description,
                        new PazienteDAO().getPaziente(pid, true), new MedicoDAO().getMedico(mid, true)));
            }

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return visite;
    }

    public ArrayList<PrescrizioneEsame> getPrescrizioniEsami(String vid){
        ArrayList<PrescrizioneEsame> pr = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            pr = new ArrayList<>();

            String query = "SELECT * FROM prescrizione_esami WHERE vid = '" + vid + "';";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                String prescid = rs.getString("prescid");
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                VisitaBase v = new VisitaBaseDAO().getVisita(vid);
                pr.add(new PrescrizioneEsame(prescid, v, e));
            }

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return pr;
    }
    public ArrayList<PrescrizioneFarmaco> getPrescrizioniFarmaci(String vid){
        ArrayList<PrescrizioneFarmaco> pr = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            pr = new ArrayList<>();
            String query = "SELECT * FROM prescrizione_farmaci WHERE vid = '" + vid + "';";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                VisitaBase v = new VisitaBaseDAO().getVisita(vid);
                Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
                pr.add(new PrescrizioneFarmaco(v, f, rs.getInt("quantity")));
            }

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return pr;
    }

    // ADD
    public boolean addVisitaBase(VisitaBase v){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO visita_base (vid, pid, mid, date, description) VALUES('" + v.getId() + "','" +
                    v.getPaziente().getId() + "','" + v.getMedico().getId() + "'," + v.getDate() + ",'" +
                    v.getDescription() + "');";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }
}
