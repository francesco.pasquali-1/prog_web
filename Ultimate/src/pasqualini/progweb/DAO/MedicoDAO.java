package pasqualini.progweb.DAO;

import pasqualini.progweb.generalclass.Pazienti;
import pasqualini.progweb.model.Medico;
import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.model.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

public class MedicoDAO {
    public Medico getMedico(String search_key, boolean isID){
        Medico medico = null;
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query;
            if (isID){
                query = "SELECT * FROM utenti as u INNER JOIN medici as m ON u.uid = m.mid WHERE mid='" + search_key + "';";
            } else {
                query = "SELECT * FROM utenti as u INNER JOIN medici as m ON u.uid = m.mid WHERE u.uemail='" + search_key + "';";
            }

            ResultSet rs_id = stmt.executeQuery( query );
            rs_id.next();

            String id = rs_id.getString("uid");
            String fotoPaht = rs_id.getString("ufoto");
            String email = rs_id.getString("uemail");
            String nome = rs_id.getString("mnome");
            String cognome = rs_id.getString("mcognome");
            String sesso = rs_id.getString("msesso");
            String cf = rs_id.getString("mcodice_fiscale");
            String provincia_operato = rs_id.getString("mprovincia_operato");

            stmt.close();
            c.close();

            medico = new Medico(id, email, fotoPaht, nome, cognome, sesso, cf, provincia_operato);
        } catch (Exception e){
            System.err.println("MedicoDAO getMedico Error: " + e);
        }
        return medico;
    }
    public ArrayList<VisitaBase> getVisiteBase(String id){
        ArrayList<VisitaBase> visiteBase = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query;
            query = "SELECT * FROM visita_base WHERE mid='" + id + "';";

            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(id, true);
                Paziente p = Pazienti.getPaziente(rs.getString("pid"));
                long data = rs.getLong("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(id, paz) Error: " + e);
        }
        return visiteBase;
    }

    // UPDATE
    public Medico updateEmail(String mid, String email){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE utenti SET uemail='" + email + "' WHERE uid='" + mid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Medico m = new MedicoDAO().getMedico(mid, true);
            m.setEmail(email);

            stmt.close();
            c.close();

            return m;
        } catch (Exception e){
            System.err.println("UpdateMedicoDAO updateEmail Error: " + e);
        }
        return null;
    }
    public Medico updateProvinciaOperato(String mid, String provinciaOperato){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE medici SET mprovincia_operato='" + provinciaOperato + "' WHERE mid='" + mid + "';";

            ResultSet rs_id = stmt.executeQuery( query_id );

            Medico m = new MedicoDAO().getMedico(mid, true);
            m.setProvinciaOperato(provinciaOperato);

            stmt.close();
            c.close();

            return m;
        } catch (Exception e){
            System.err.println("UpdateMedicoDAO updateProvinciaOperato Error: " + e);
        }
        return null;
    }
}
