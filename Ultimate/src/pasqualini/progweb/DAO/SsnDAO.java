package pasqualini.progweb.DAO;

import pasqualini.progweb.generalclass.Tickets;
import pasqualini.progweb.model.PrescrizioneEsame;
import pasqualini.progweb.model.Ssn;
import pasqualini.progweb.model.Ticket;

import java.sql.*;
import java.util.ArrayList;

public class SsnDAO {
    public Ssn getSSN(String key, boolean isId){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query;
            if (isId){
                query = "SELECT * FROM ssn as s INNER JOIN utenti as u ON s.sid=u.uid WHERE s.sid='" + key + "';";
            } else {
                query = "SELECT * FROM ssn as s INNER JOIN utenti as u ON s.sid=u.uid WHERE u.uemail='" + key + "';";
            }

            ResultSet rs = stmt.executeQuery(query);
            rs.next();

            String id = rs.getString("uid");
            String foto = rs.getString("ufoto");
            String email = rs.getString("uemail");
            String nome = rs.getString("snome");
            String provincia = rs.getString("sprovincia");

            stmt.close();
            c.close();

            return new Ssn(id, foto, email, nome, provincia);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<PrescrizioneEsame> getEsamiDaFare(String pid){
        ArrayList<PrescrizioneEsame> prescrizioni = null;
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();
            String query = "select prescid, p.vid, eid from prescrizione_esami as p inner join " +
                    "(select * from visita_base where pid = \'" + pid + "\') as v on p.vid=v.vid;";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                prescrizioni.add(new PrescrizioneEsame(rs.getString("pid"),
                                new VisitaBaseDAO().getVisita(rs.getString("vid")),
                                new EsameDAO().getEsame(rs.getString("eid"))));
            }

            stmt.close();
            c.close();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean paidTicket(String tid){
        Ticket t = Tickets.getTicket(tid);
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "UPDATE ticket SET tpaid = true WHERE tid = '" + t.getId() + "';";
            t.setPaid(true);

            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // ADD
    public boolean addErogazioneEsame(){
        return false;
    }
}
