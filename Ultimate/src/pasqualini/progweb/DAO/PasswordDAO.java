package pasqualini.progweb.DAO;

import pasqualini.progweb.generalclass.GeneralMethods;

import java.sql.*;

public class PasswordDAO {
    /*
    * agent == 0: agent
    *             1: medico
    *             2: ssn
    * */
    public static boolean getAccess(String email, String password, int agent){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();
            String query_password = "SELECT uid, upassword FROM utenti WHERE uemail='" + email + "';";
            ResultSet rd_pssw = stmt.executeQuery(query_password);
            String passwd = null, passed_hash = null;
            while (rd_pssw.next()){
                if (agent == 0 && rd_pssw.getString("uid").startsWith("paz_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);
                } else if (agent == 1 && rd_pssw.getString("uid").startsWith("med_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);
                } else if (agent == 2 && rd_pssw.getString("uid").startsWith("ssn_")){
                    passwd = rd_pssw.getString("upassword");
                    passed_hash = GeneralMethods.getSHA512(password);
                }
            }
            stmt.close();
            c.close();
            if (passwd != null && passwd.equals(passed_hash)) {
                System.out.println("Password Accepted");
                return true;
            } else {
                System.err.println("Password Rejected");
                return false;
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("PasswordDAO Error: " + e);
            return false;
        }
    }

    // UPDATE
    public static boolean updatePassword(String id, String old_password, String password){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // Check old password
            String check_old = "SELECT upassword FROM utenti WHERE uid='" + id + "';";
            ResultSet rs_old = stmt.executeQuery(check_old);
            if (rs_old.next()){
                if (!rs_old.getString("upassword").equals(GeneralMethods.getSHA512(old_password))){
                    System.out.println("HERe");
                    return false;
                }
            }
            System.out.println("HERE");

            // get id
            String hash = GeneralMethods.getSHA512(password);
            String query_id = "UPDATE utenti SET upassword='" + hash + "' WHERE uid='" + id + "';";

            stmt.execute(query_id);

            System.out.println("HEREe");
            String check_query = "SELECT upassword FROM utenti WHERE uid='" + id + "';";
            ResultSet rs_check = stmt.executeQuery( check_query);
            if (rs_check.next()) {
                if (rs_check.getString("upassword").equals(GeneralMethods.getSHA512(password))){
                    stmt.close();
                    c.close();
                    System.out.println("HEREee");
                    return true;
                }
            }
            stmt.close();
            c.close();
        } catch (Exception e){
            System.err.println("UpdatePasswordDAO updatePassword Error: " + e);
        }
        return false;
    }
}
