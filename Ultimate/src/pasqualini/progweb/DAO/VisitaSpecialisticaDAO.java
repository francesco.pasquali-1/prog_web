package pasqualini.progweb.DAO;

import pasqualini.progweb.model.VisitaSpecialistica;

import java.sql.*;

public class VisitaSpecialisticaDAO {
    public boolean addVisitaSpecialistica(VisitaSpecialistica vs){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO visita_specialistica (pid, mid, tid, date, description) VALUES('"
                    + vs.getPaziente().getId() + "','" + vs.getMedico().getId() + "','" + vs.getTicket().getId()
                    + "'," + vs.getDate() + ",'" + vs.getDescription() + "');";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaSpecialisticaDAO Error: " + e);
        }
        return false;
    }
    public VisitaSpecialistica getVisitaSpecialistica(String tid){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_specialistica WHERE tid = '" + tid + "';";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            String pid = rs.getString("pid");
            String mid = rs.getString("mid");
            long date = rs.getLong("date");
            String description = rs.getString("description");



            stmt.close();
            c.close();

            return new VisitaSpecialistica(new PazienteDAO().getPaziente(pid, true),
                    new MedicoDAO().getMedico(mid, true), new TicketDAO().getTicket(tid), description, date);
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaSpecialisticaDAO Error: " + e);
        }
        return null;
    }
}
