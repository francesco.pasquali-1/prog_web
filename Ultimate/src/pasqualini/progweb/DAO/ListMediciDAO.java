package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Medico;

import java.sql.*;
import java.util.ArrayList;

public class ListMediciDAO {
    public ArrayList<Medico> getMediciProvincia(String provincia){
        ArrayList<Medico> medici = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM medici where mprovincia_operato='" + provincia + "';";

            medici = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while(rs.next()){
                String id = rs.getString("mid");
                medici.add(new MedicoDAO().getMedico(id, true));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListPazientiDAO getMyPazienti Error: " + e);
        }
        return medici;
    }
}
