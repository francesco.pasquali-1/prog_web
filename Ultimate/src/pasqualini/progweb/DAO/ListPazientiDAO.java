package pasqualini.progweb.DAO;

/*
* TODO:
*  Not return all patients but just the name (For space)
* */

import pasqualini.progweb.model.Paziente;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class ListPazientiDAO {
    public ArrayList<Paziente> getMyPazienti(String mid){
        ArrayList<Paziente> pazienti = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM " +
                        "(SELECT pazienti.pid, has_base.mid FROM pazienti INNER JOIN has_base ON pazienti.pid = has_base.pid)as j " +
                    "WHERE mid='" + mid + "';";

            pazienti = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while(rs.next()){
                String id = rs.getString("pid");
                pazienti.add(new PazienteDAO().getPaziente(id, true));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListPazientiDAO getMyPazienti Error: " + e);
        }
        return pazienti;
    }

    public HashMap<String, String> getAllPazienti(){
        HashMap<String, String> pazienti = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT pid, pcodice_fiscale FROM pazienti;";

            pazienti = new HashMap<>();
            ResultSet rs = stmt.executeQuery( query );
            while(rs.next()){
                String id = rs.getString("pid");
                String cf = rs.getString("pcodice_fiscale");
                pazienti.put(id, cf);
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListPazientiDAO getAllPatient Error: " + e);
        }
        return pazienti;
    }
//    public ArrayList<String> getAllPazientiList(){
//        ArrayList<String> pazienti = null;
//        try {
//            Class.forName("org.postgresql.Driver");
//            Connection c = DriverManager
//                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
//            Statement stmt = c.createStatement();
//
//            String query = "SELECT pid FROM pazienti;";
//
//            pazienti = new HashMap<>();
//            ResultSet rs = stmt.executeQuery( query );
//            while(rs.next()){
//                String id = rs.getString("pid");
//                String cf = rs.getString("pcodice_fiscale");
//                pazienti.put(id, cf);
//            }
//            stmt.close();
//            c.close();
//        } catch (ClassNotFoundException | SQLException e) {
//            System.err.println("ListPazientiDAO getAllPatient Error: " + e);
//        }
//        return pazienti;
//    }
}
