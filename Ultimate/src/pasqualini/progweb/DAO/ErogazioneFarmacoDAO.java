package pasqualini.progweb.DAO;

import pasqualini.progweb.model.ErogazioneFarmaco;
import pasqualini.progweb.model.Farmaco;
import pasqualini.progweb.model.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

public class ErogazioneFarmacoDAO {
    public ArrayList<ErogazioneFarmaco> getErogazioneFarmaciTime(long startDate, long endDate, String provincia){
        ArrayList<ErogazioneFarmaco> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM erogazione_farmaci WHERE vid in (SELECT vid FROM visita_base WHERE mid in(" +
                    "SELECT mid from medici WHERE mprovincia_operato='" + provincia + "') and date>" + startDate +
                    " or date< " + endDate + ");";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
                prescrizioni.add(new ErogazioneFarmaco(v, f, rs.getLong("date"), rs.getInt("quantity")));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyErogazioniFarmaciDAO Error: " + e);
        }
        return prescrizioni;
    }
}
