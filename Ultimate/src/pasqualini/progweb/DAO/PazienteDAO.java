package pasqualini.progweb.DAO;
import java.sql.*;
import java.util.ArrayList;

import pasqualini.progweb.generalclass.Pazienti;
import pasqualini.progweb.generalclass.Tickets;
import pasqualini.progweb.model.*;

/*
* TODO:
*   Close all connections
* */

public class PazienteDAO {
    public Paziente getPaziente(String search_key, boolean isID){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query;
            if (isID){
                query = "SELECT * FROM utenti as u INNER JOIN pazienti as p ON u.uid = p.pid WHERE u.uid='" + search_key + "';";
            } else {
                query = "SELECT * FROM utenti as u INNER JOIN pazienti as p ON u.uid = p.pid WHERE u.uemail='" + search_key + "';";
            }

            ResultSet rs_id = stmt.executeQuery( query );
            rs_id.next();

            // return le informazioni del paziente se la password e' corretta (null altrimenti)
            String id, email;
            if (isID){
                id = search_key;
                email = rs_id.getString("uemail");
            } else {
                id = rs_id.getString("uid");
                email = search_key;
            }
            String fotoPaht = rs_id.getString("ufoto");
            String nome = rs_id.getString("pnome");
            String conome = rs_id.getString("pcognome");
            String sesso = rs_id.getString("psesso");
            String cf = rs_id.getString("pcodice_fiscale");
            String luogoNascita = rs_id.getString("pluogo_nascita");
            long dataNascita = rs_id.getLong("pdata_nascita");
            String provincaResidenza = rs_id.getString("pprovincia_residenza");

            stmt.close();
            c.close();

            return new Paziente(id, fotoPaht, email, nome, conome, sesso, cf, luogoNascita, dataNascita, provincaResidenza);
        } catch (Exception e){
            System.err.println("PazienteDAO getPaziente Error: " + e);
        }
        return null;
    }
    public Paziente getPazienteByCF(String cf){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query;
            query = "SELECT * FROM utenti as u INNER JOIN pazienti as p ON u.uid = p.pid WHERE p.pcodice_fiscale='" + cf + "';";

            ResultSet rs_id = stmt.executeQuery( query );
            rs_id.next();

            // return le informazioni del paziente se la password e' corretta (null altrimenti)
            String id = rs_id.getString("uid");
            String email = rs_id.getString("uemail");
            String fotoPaht = rs_id.getString("ufoto");
            String nome = rs_id.getString("pnome");
            String conome = rs_id.getString("pcognome");
            String sesso = rs_id.getString("psesso");
            String luogoNascita = rs_id.getString("pluogo_nascita");
            long dataNascita = rs_id.getLong("pdata_nascita");
            String provincaResidenza = rs_id.getString("pprovincia_residenza");

            stmt.close();
            c.close();

            return new Paziente(id, fotoPaht, email, nome, conome, sesso, cf, luogoNascita, dataNascita, provincaResidenza);
        } catch (Exception e){
            System.err.println("PazienteDAO getPaziente Error: " + e);
        }
        return null;
    }
    public Medico getMyMedico(String id){
        Medico medico = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT mid from has_base where pid = '" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()){
                medico = new MedicoDAO().getMedico(rs.getString("mid"), true);
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return medico;
    }
    public ArrayList<PrescrizioneEsame> getMyPrescrizioniEsami(String id){
        ArrayList<PrescrizioneEsame> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT prescid, v.vid, p.eid " +
                    "FROM (prescrizione_esami as p INNER JOIN visita_base as v ON p.vid = v.vid) " +
                    "WHERE v.pid='" + id + "' and prescid not in (SELECT prescid FROM erogazione_esami);";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String prescid = rs.getString("prescid");
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                prescrizioni.add(new PrescrizioneEsame(prescid, v, e));
            }

            stmt.close();
            c.close();

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyEsamiDaFareDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ArrayList<ErogazioneEsame> getErogazioniEsame(String id){
        ArrayList<ErogazioneEsame> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM erogazione_esami as e INNER JOIN " +
                    "(SELECT * FROM prescrizione_esami as p INNER JOIN visita_base as v ON p.vid=v.vid) as p " +
                    "ON e.prescid=p.prescid WHERE p.pid='" + id + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String prescid = rs.getString("prescid");
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                PrescrizioneEsame p = new PrescrizioneEsame(prescid, v, e);
                prescrizioni.add(new ErogazioneEsame(p, Tickets.getTicket(rs.getString("tid")),
                        new SsnDAO().getSSN(rs.getString("sid"), true), rs.getLong("date"), rs.getString("description")));
            }

            stmt.close();
            c.close();

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyEsamiFattiDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ErogazioneEsame getErogazioneEsame(String id){
        ErogazioneEsame erogazioneEsame = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM erogazione_esami WHERE prescid='" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()){
                erogazioneEsame = new ErogazioneEsame(new PazienteDAO().getPrescrizioneEsame(id),
                        new TicketDAO().getTicket(rs.getString("tid")),
                        new SsnDAO().getSSN(rs.getString("sid"), true),
                        rs.getLong("date"),
                        rs.getString("description"));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return erogazioneEsame;
    }
    public ArrayList<PrescrizioneFarmaco> getMyPrescrizioniFarmaci(String id){
        ArrayList<PrescrizioneFarmaco> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT v.vid, fid, quantity " +
                    "FROM (prescrizione_farmaci as p INNER JOIN visita_base as v ON p.vid = v.vid) " +
                    "WHERE v.pid='" + id + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
                prescrizioni.add(new PrescrizioneFarmaco(v, f, rs.getInt("quantity")));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyPrescirioniFarmaciDAO Error: " + e);
        }
        return prescrizioni;
    }
    public ArrayList<VisitaBase> getVisiteBase(String id){
        ArrayList<VisitaBase> visiteBase = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_base WHERE pid='" + id + "';";


            visiteBase = new ArrayList<>();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                MedicoDAO md = new MedicoDAO();
                Medico m = md.getMedico(rs.getString("mid"), true);
                Paziente p = Pazienti.getPaziente(id);
                long data = rs.getLong("date");
                String description = rs.getString("description");
                String vid = rs.getString("vid");
                visiteBase.add(new VisitaBase(vid, data, description, p, m));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(id, paz) Error: " + e);
        }
        return visiteBase;
    }
    public VisitaBase getVisitaBase(String id){
        VisitaBase visitaBase = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_base WHERE vid='" + id + "';";

            ResultSet rs = stmt.executeQuery( query );
            System.out.println(rs);
            rs.next();
            System.out.println(rs);
            visitaBase = new VisitaBase(id, rs.getLong("date"), rs.getString("description"),
                    Pazienti.getPaziente(rs.getString("pid")), new MedicoDAO().getMedico(rs.getString("mid"), true));
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("ListVisiteBase getVisiteBase(id, paz) Error: " + e);
        }
        return visitaBase;
    }
    public ArrayList<VisitaSpecialistica> getVisitaSpecialistiche(String pid){
        ArrayList<VisitaSpecialistica> visiteSpecialistiche = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM visita_specialistica WHERE pid='" + pid + "';";

            visiteSpecialistiche = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                Paziente p = this.getPaziente(rs.getString("pid"), true);
                Medico m = new MedicoDAO().getMedico(rs.getString("mid"), true);
                Ticket t = new TicketDAO().getTicket(rs.getString("tid"));
                visiteSpecialistiche.add(new VisitaSpecialistica(p,m,t,
                        rs.getString("description"),rs.getLong("date")));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return visiteSpecialistiche;
    }
    public PrescrizioneEsame getPrescrizioneEsame(String id){
        PrescrizioneEsame prescrizioneEsame = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM prescrizione_esami WHERE prescid='" + id + "';";

            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()){
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Esame e = new EsameDAO().getEsame(rs.getString("eid"));
                prescrizioneEsame = new PrescrizioneEsame(rs.getString("prescid"), v, e);
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return prescrizioneEsame;
    }

    public ArrayList<Ticket> getTicket(String pid, boolean pagati){
        ArrayList<Ticket> tickets = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM ticket WHERE (tid in(SELECT tid FROM visita_specialistica WHERE pid='" + pid
                                + "') or tid in(SELECT tid FROM erogazione_esami WHERE prescid in "
                                + "(SELECT prescid FROM prescrizione_esami WHERE vid in ("
                                + "SELECT vid FROM visita_base WHERE pid='" + pid + "')))) and tpaid =" + pagati + ";";

            tickets = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                Ticket v = new TicketDAO().getTicket(rs.getString("tid"));
                tickets.add(v);
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return tickets;
    }
    public ArrayList<ErogazioneFarmaco> getErogazioneFarmaci(String pid){
        ArrayList<ErogazioneFarmaco> prescrizioni = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT v.vid, fid, p.date, p.quantity " +
                    "FROM (erogazione_farmaci as p INNER JOIN visita_base as v ON p.vid = v.vid) " +
                    "WHERE v.pid='" + pid + "';";

            prescrizioni = new ArrayList<>();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
                Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
                prescrizioni.add(new ErogazioneFarmaco(v, f, rs.getLong("date"), rs.getInt("quantity")));
            }
            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyErogazioniFarmaciDAO Error: " + e);
        }
        return prescrizioni;
    }

    // UPDATES
    public boolean changeMedico(String id, String idMedico){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            // get id
            String query_id = "UPDATE has_base SET mid='" + idMedico + "' WHERE pid='" + id + "';";

            stmt.execute(query_id);

            String check_query = "SELECT mid FROM has_base WHERE pid='" + id + "';";
            ResultSet rs_check = stmt.executeQuery( check_query);
            if (rs_check.next()) {
                if (rs_check.getString("mid").equals(idMedico)){
                    stmt.close();
                    c.close();
                    return true;
                }
            }
            stmt.close();
            c.close();
        } catch (Exception e){
            System.err.println("UpdatePasswordDAO updatePassword Error: " + e);
        }
        return false;
    }

    // ADDING
    public boolean addErogazioneEsame(ErogazioneEsame erogazioneEsame){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO erogazione_esami (prescid, tid, sid, date, description) VALUES('"
                    + erogazioneEsame.getEsame().getId() + "','" + erogazioneEsame.getTicket().getId() + "','"
                    + erogazioneEsame.getSsn().getId() + "'," + erogazioneEsame.getDate() + ",'"
                    + erogazioneEsame.getDescription() + "');";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }
}
