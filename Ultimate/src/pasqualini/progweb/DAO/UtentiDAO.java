package pasqualini.progweb.DAO;

import pasqualini.progweb.model.VisitaSpecialistica;

import java.sql.*;

public class UtentiDAO {
    public boolean changePhoto(VisitaSpecialistica vs){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO visita_specialistica (pid, mid, tid, date, description) VALUES('"
                    + vs.getPaziente().getId() + "','" + vs.getMedico().getId() + "','" + vs.getTicket().getId()
                    + "'," + vs.getDate() + ",'" + vs.getDescription() + "');";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaSpecialisticaDAO Error: " + e);
        }
        return false;
    }
}
