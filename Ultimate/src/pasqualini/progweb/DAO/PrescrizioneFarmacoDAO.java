package pasqualini.progweb.DAO;

import pasqualini.progweb.model.Farmaco;
import pasqualini.progweb.model.PrescrizioneFarmaco;
import pasqualini.progweb.model.VisitaBase;

import java.sql.*;
import java.util.ArrayList;

public class PrescrizioneFarmacoDAO {
    public boolean addPrescrizioneEsame(PrescrizioneFarmaco p){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();

            String query = "INSERT INTO prescrizione_farmaci (vid, fid, quantity) VALUES('" + p.getVisita().getId() + "','" +
                    p.getFarmaco().getId() + "'," + p.getQuantity() + ");";
            stmt.executeUpdate(query);

            stmt.close();
            c.commit();
            c.close();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("VisitaBasteDAO Error: " + e);
        }
        return false;
    }

    public PrescrizioneFarmaco getPrescrizioneFarmaco(String vid, String fid){
        PrescrizioneFarmaco prescrizioni = null;
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/prog_web", "postgres", "password");
            Statement stmt = c.createStatement();

            String query = "SELECT * FROM prescrizione_farmaci WHERE vid = '" + vid + "' and fid = '" + fid + "';";

            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            VisitaBase v = new VisitaBaseDAO().getVisita(rs.getString("vid"));
            Farmaco f = new FarmacoDAO().getFarmaco(rs.getString("fid"));
            prescrizioni = new PrescrizioneFarmaco(v, f, rs.getInt("quantity"));


            stmt.close();
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("MyPrescirioniFarmaciDAO Error: " + e);
        }
        return prescrizioni;
    }
}
