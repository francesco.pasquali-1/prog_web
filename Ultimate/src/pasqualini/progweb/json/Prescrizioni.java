package pasqualini.progweb.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;

@JsonPropertyOrder({
        "esami",
        "farmaci"
})
public class Prescrizioni {
    private String mid;
    private String pid;
    private String description;
    private ArrayList<String> presrcizioneEsami = new ArrayList<>();
    private HashMap<String, String> prescrizioneFarmaco = new HashMap<>();

    public Prescrizioni(String mid, String pid, String description,
                        ArrayList<String> presrcizioneEsami, HashMap<String, String> prescrizioneFarmaco){
        this.mid = mid;
        this.pid = pid;
        this.description = description;
        this.presrcizioneEsami = presrcizioneEsami;
        this.prescrizioneFarmaco = prescrizioneFarmaco;
    }

    @JsonProperty("mid")
    public void setMid(String mid){
        this.mid = mid;
    }
    @JsonProperty("pid")
    public void setPid(String pid){
        this.pid = pid;
    }
    @JsonProperty("description")
    public void setDescription(String description){
        this.description = description;
    }
    @JsonProperty("esami")
    public void setPresrcizioneEsami(ArrayList<String> presrcizioneEsami) {
        System.out.println("HERE");
        this.presrcizioneEsami = presrcizioneEsami;
    }
    @JsonProperty("farmaci")
    public void setPrescrizioneFarmaco(HashMap<String, String> prescrizioneFarmaco) {
        this.prescrizioneFarmaco = prescrizioneFarmaco;
    }

    public String getMid(){
        return mid;
    }
    public String getPid(){
        return pid;
    }
    public String getDescription(){
        return this.description;
    }
    public ArrayList<String> getPresrcizioneEsami() {
        return presrcizioneEsami;
    }
    public HashMap<String, String> getPrescrizioneFarmaco() {
        return prescrizioneFarmaco;
    }

    @Override
    public String toString() {
        return "Presrcizioni esami: " + this.presrcizioneEsami.size() + " prescrizioni farmaci: " + this.prescrizioneFarmaco.size();
    }


}
