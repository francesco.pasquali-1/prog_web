package pasqualini.progweb.json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class PrescrizioniDeserializer implements JsonDeserializer {
    @Override
    public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        JsonObject jobject = jsonElement.getAsJsonObject();
        String mid = jobject.get("mid").getAsString();
        String pid = jobject.get("pid").getAsString();
        String description = jobject.get("description").getAsString();
        JsonArray ja = jobject.getAsJsonArray("esami");
        JsonArray jf = jobject.getAsJsonArray("farmaci");
        JsonArray jq = jobject.getAsJsonArray("quantity");
        ArrayList<String> aesami = new ArrayList<>();
        HashMap<String, String> afarmaci = new HashMap<>();
        for (JsonElement s:
             ja) {
            aesami.add(s.getAsString());
        }
        for (int i = 0; i < jf.size(); i++){
            afarmaci.put(jf.get(i).getAsString(), jq.get(i).getAsString());
        }
        return new Prescrizioni(mid, pid, description, aesami, afarmaci);
    }
}
