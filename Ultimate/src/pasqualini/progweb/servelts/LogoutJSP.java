package pasqualini.progweb.servelts;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/*
* TODO:
* */

@WebServlet(name = "LogoutServet",urlPatterns = "/logout")
public class LogoutJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.invalidate();

        Cookie cookie = new Cookie("user_id", "");
        cookie.setMaxAge(-1);

        response.addCookie(cookie);
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }
}
