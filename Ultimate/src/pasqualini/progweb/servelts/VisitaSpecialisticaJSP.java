package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.VisitaSpecialisticaDAO;
import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.VisitaSpecialistica;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
* TODO:
*  Redirect to GetPaziente
* */

@WebServlet(name = "VisitaSpecialistica",urlPatterns = "/visita_specialistica")
public class VisitaSpecialisticaJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        String tid = request.getParameter("tid");
        VisitaSpecialistica vs = new VisitaSpecialisticaDAO().getVisitaSpecialistica(tid);
        if (vs != null) {
            HttpSession session = request.getSession();

            session.setAttribute("visita_specialistica", vs);

            RequestDispatcher rd;
            if (uid.startsWith("paz_")){
                rd = request.getRequestDispatcher("visita_specialistica.jsp");
            } else if (uid.startsWith("med_")) {
                session.setAttribute("paziente", vs.getPaziente());
                rd = request.getRequestDispatcher("visita_medico.jsp");
            } else {
                rd = request.getRequestDispatcher("404.html");
            }
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
