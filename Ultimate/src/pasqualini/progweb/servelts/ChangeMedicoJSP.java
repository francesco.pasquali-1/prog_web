package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.PazienteDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
* TODO:
*  Pagina dedicata in caso di password errata o meno (conferma con redirect)
* */

@WebServlet(name = "ChangeMedicoServet",urlPatterns = "/change_medico")
public class ChangeMedicoJSP extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pid = request.getParameter("id");
        String mid = request.getParameter("new_medico");

        String change_medico = null;
        try {
            if (new PazienteDAO().changeMedico(pid, mid)){
                change_medico = "Medico cambiato correttamente";
            } else {
                change_medico = "Qualcosa e' andato storto. Riprovare piu' tardi.";
            }

            response.getWriter().write(change_medico);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
