package pasqualini.progweb.servelts;

import com.google.gson.GsonBuilder;
import pasqualini.progweb.DAO.*;
import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.model.PrescrizioneEsame;
import pasqualini.progweb.model.PrescrizioneFarmaco;
import pasqualini.progweb.model.VisitaBase;
import pasqualini.progweb.json.Prescrizioni;
import pasqualini.progweb.json.PrescrizioniDeserializer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/*
* TODO:
*  Pagina dedicata in caso di password errata o meno (conferma con redirect)
* */

@WebServlet(name = "AddVisitaServet",urlPatterns = "/add_visita")
public class AddVisitaJSP extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String prescrizioni = request.getParameter("data");
        long date = System.currentTimeMillis();

        GsonBuilder gsonBldr = new GsonBuilder();
        gsonBldr.registerTypeAdapter(Prescrizioni.class, new PrescrizioniDeserializer());
        Prescrizioni presc = gsonBldr.create().fromJson(prescrizioni, Prescrizioni.class);

        VisitaBaseDAO vDAO = new VisitaBaseDAO();
        PazienteDAO pDAO = new PazienteDAO();
        MedicoDAO mDAO = new MedicoDAO();

        Paziente paziente = pDAO.getPaziente(presc.getPid(), true);

        VisitaBase visitaBase = new VisitaBase(vDAO.getNextVisita().toString(), date, presc.getDescription(),
                paziente, mDAO.getMedico(presc.getMid(), true));
        PrescrizioneEsameDAO pEsameDAO = new PrescrizioneEsameDAO();
        PrescrizioneFarmacoDAO pFarmacoDAO = new PrescrizioneFarmacoDAO();

        try {
            String add_visita;
            if (vDAO.addVisitaBase(visitaBase)){
                for (String esame:
                        presc.getPresrcizioneEsami()) {
                    PrescrizioneEsame pEsame = new PrescrizioneEsame(pEsameDAO.getNextPrescrizione().toString(),
                            visitaBase, new EsameDAO().getEsame(esame));
                    pEsameDAO.addPrescrizioneEsame(pEsame);
                }
                for (String farmaco:
                        presc.getPrescrizioneFarmaco().keySet()){
                    PrescrizioneFarmaco pFarmaco = new PrescrizioneFarmaco(visitaBase, new FarmacoDAO().getFarmaco(farmaco),
                            Integer.parseInt(presc.getPrescrizioneFarmaco().get(farmaco)));
                    pFarmacoDAO.addPrescrizioneEsame(pFarmaco);
                }
                if (this.sendMail(paziente)){
                    add_visita = "Visita Aggiunta";
                } else {
                    add_visita = "Qualcosa e' andato storto. Riprovare.";
                }
            } else {
                add_visita = "Qualcosa e' andato storto. Riprovare.";
            }

            response.getWriter().write(add_visita);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean sendMail(Paziente p){
        // Recipient's email ID needs to be mentioned.
        // String to = p.getEmail();
        String to = "francesco.pasquali-1@studenti.unitn.it";

        // Sender's email ID needs to be mentioned
        String from = "francypasqualaroccia@alice.it";

        // Assuming you are sending email from localhost
        String host = "out.alice.it";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        String port = "587";
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);

        // Get the default Session object.
        String username = "francypasqualaroccia@alice.it";
        String password = "08112731";
        Session session = Session.getInstance(properties, new javax.mail.Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return  new PasswordAuthentication(username, password);
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Nuova Ricetta!");

            // Now set the actual message
            message.setText("Hai una nuova ricetta da guardare. Accedi subito per controllare!");

            // Send message
            Transport.send(message);
            return true;
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
        return false;
    }
}