package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.*;
import pasqualini.progweb.model.*;
import pasqualini.progweb.generalclass.GeneralMethods;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/*
* TODO:
*  - PDF 1d
*  - Cambiare foto 1h
*  - Developer 0.5h
*  - Date not as timestamp
*  - SSN settings 0.5h
* */

@WebServlet(name = "CiaoServlet",urlPatterns = "/login")
public class LoginJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();
        RequestDispatcher rd;

        String uid = GeneralMethods.getUserIdCookie(request.getCookies());

        if (uid != null && uid.startsWith("paz_")){
            PazienteDAO pDAO= new PazienteDAO();
            Paziente p = pDAO.getPaziente(uid, true);
            String paz_id = uid;

            ArrayList<PrescrizioneEsame> pr = pDAO.getMyPrescrizioniEsami(paz_id);
            ArrayList<ErogazioneEsame> er = pDAO.getErogazioniEsame(paz_id);
            ArrayList<VisitaBase> vi = pDAO.getVisiteBase(paz_id);
            ArrayList<VisitaSpecialistica> vs = pDAO.getVisitaSpecialistiche(paz_id);
            ArrayList<PrescrizioneFarmaco> fp = pDAO.getMyPrescrizioniFarmaci(paz_id);
            ArrayList<ErogazioneFarmaco> fe = pDAO.getErogazioneFarmaci(paz_id);
            ArrayList<Ticket> tp = pDAO.getTicket(paz_id, false);
            ArrayList<Ticket> tpp = pDAO.getTicket(paz_id, true);
            System.out.println(vs);

            session.setAttribute("id", p.getId());
            session.setAttribute("foto", p.getFotoPath());
            session.setAttribute("nome", p.getNome());
            session.setAttribute("cognome", p.getCognome());
            session.setAttribute("prescrizioni_esami", pr);
            session.setAttribute("esami_fatti", er);
            session.setAttribute("visite_base", vi);
            session.setAttribute("visite_speciailstche", vs);
            session.setAttribute("prescrizioni_farmaci", fp);
            session.setAttribute("erogaioni_farmaci", fe);
            session.setAttribute("ticket_pagare", tp);
            session.setAttribute("ticket_pagati", tpp);
            session.setAttribute("error", null);

            rd = request.getRequestDispatcher("paziente.jsp");
        } else if (uid != null && uid.startsWith("med_")){
            MedicoDAO mDAO = new MedicoDAO();
            Medico m = mDAO.getMedico(uid, true);
            String mid = m.getId();
            ArrayList<Esame> lista_esami = new ListEsamiDAO().getListEsami();
            ArrayList<Farmaco> lista_farmaci = new ListFarmaciDAO().getListFarmaci();
            ArrayList<Paziente> lista_miei_pazienti = new ListPazientiDAO().getMyPazienti(m.getId());
             HashMap<String, String> lista_tutti_pazienti = new ListPazientiDAO().getAllPazienti();

            session.setAttribute("id", mid);
            session.setAttribute("medico", m);
            session.setAttribute("lista_miei_pazienti", lista_miei_pazienti);
            session.setAttribute("lista_tutti_pazienti", lista_tutti_pazienti);
            session.setAttribute("lista_esami", lista_esami);
            session.setAttribute("lista_farmaci", lista_farmaci);

            rd = request.getRequestDispatcher("medico.jsp");
        } else if (uid != null && uid.startsWith("ssn_")){
            SsnDAO ssnDAO = new SsnDAO();
            Ssn ssn = ssnDAO.getSSN(uid, true);
            String sid = ssn.getId();
            HashMap<String, String> lista_tutti_pazienti = new ListPazientiDAO().getAllPazienti();

            session.setAttribute("id", sid);
            session.setAttribute("ssn", ssn);
            session.setAttribute("lista_tutti_pazienti", lista_tutti_pazienti);

            rd = request.getRequestDispatcher("ssn.jsp");
        } else {
            rd = request.getRequestDispatcher("index.jsp");
        }
        rd.forward(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        int agent = Integer.parseInt(request.getParameter("agent"));
        String rememberMe_mid = request.getParameter("remember_me");
        boolean rememberMe = false;
        if (rememberMe_mid != null && rememberMe_mid.equals("True")){
            rememberMe = true;
        }
        System.out.println(rememberMe);

        HttpSession session = request.getSession();
        int expireSessionRemember = 60 * 60 * 24 * 15;

        if (agent == 0 && PasswordDAO.getAccess(email, password, agent)) {
            PazienteDAO pDAO= new PazienteDAO();
            Paziente p = pDAO.getPaziente(email, false);
            String paz_id = p.getId();

            ArrayList<PrescrizioneEsame> pr = pDAO.getMyPrescrizioniEsami(paz_id);
            ArrayList<ErogazioneEsame> er = pDAO.getErogazioniEsame(paz_id);
            ArrayList<VisitaBase> vi = pDAO.getVisiteBase(paz_id);
            ArrayList<VisitaSpecialistica> vs = pDAO.getVisitaSpecialistiche(paz_id);
            ArrayList<PrescrizioneFarmaco> fp = pDAO.getMyPrescrizioniFarmaci(paz_id);
            ArrayList<ErogazioneFarmaco> fe = pDAO.getErogazioneFarmaci(paz_id);
            ArrayList<Ticket> tp = pDAO.getTicket(paz_id, false);
            ArrayList<Ticket> tpp = pDAO.getTicket(paz_id, true);

            session.setAttribute("id", p.getId());
            session.setAttribute("foto", p.getFotoPath());
            session.setAttribute("nome", p.getNome());
            session.setAttribute("cognome", p.getCognome());
            session.setAttribute("prescrizioni_esami", pr);
            session.setAttribute("esami_fatti", er);
            session.setAttribute("visite_base", vi);
            session.setAttribute("visite_speciailstche", vs);
            session.setAttribute("prescrizioni_farmaci", fp);
            session.setAttribute("erogazione_farmaci", fe);
            session.setAttribute("ticket_pagare", tp);
            session.setAttribute("ticket_pagati", tpp);
            session.setAttribute("error", null);

            Cookie cookie = new Cookie("user_id", paz_id);
            if (rememberMe){
                cookie.setMaxAge(expireSessionRemember);
            } else {
                cookie.setMaxAge(-1);
            }

            response.addCookie(cookie);
            RequestDispatcher rd = request.getRequestDispatcher("paziente.jsp");
            rd.forward(request, response);
        } else if (agent == 1 && PasswordDAO.getAccess(email, password, agent)){
            MedicoDAO mDAO = new MedicoDAO();
            Medico m = mDAO.getMedico(email, false);
            String mid = m.getId();
            ArrayList<Esame> lista_esami = new ListEsamiDAO().getListEsami();
            ArrayList<Farmaco> lista_farmaci = new ListFarmaciDAO().getListFarmaci();
            ArrayList<Paziente> lista_miei_pazienti = new ListPazientiDAO().getMyPazienti(m.getId());
            HashMap<String, String> lista_tutti_pazienti = new ListPazientiDAO().getAllPazienti();

            session.setAttribute("id", mid);
            session.setAttribute("medico", m);
            session.setAttribute("lista_miei_pazienti", lista_miei_pazienti);
            session.setAttribute("lista_tutti_pazienti", lista_tutti_pazienti);
            session.setAttribute("lista_esami", lista_esami);
            session.setAttribute("lista_farmaci", lista_farmaci);

            Cookie cookie = new Cookie("user_id", mid);
            if (rememberMe){
                cookie.setMaxAge(expireSessionRemember);
            } else {
                cookie.setMaxAge(-1);
            }

            response.addCookie(cookie);
            RequestDispatcher rd = request.getRequestDispatcher("medico.jsp");
            rd.forward(request, response);
        } else if(agent == 2 && PasswordDAO.getAccess(email, password, agent)){
            SsnDAO ssnDAO = new SsnDAO();
            Ssn ssn = ssnDAO.getSSN(email, false);
            String sid = ssn.getId();
            Statistiche statistiche = new Statistiche(ssn, 0, System.currentTimeMillis());
            HashMap<String, String> lista_tutti_pazienti = new ListPazientiDAO().getAllPazienti();

            session.setAttribute("id", sid);
            session.setAttribute("ssn", ssn);
            session.setAttribute("statistiche", statistiche);
            session.setAttribute("lista_tutti_pazienti", lista_tutti_pazienti);

            Cookie cookie = new Cookie("user_id", sid);
            if (rememberMe){
                cookie.setMaxAge(expireSessionRemember);
            } else {
                cookie.setMaxAge(-1);
            }

            response.addCookie(cookie);
            RequestDispatcher rd = request.getRequestDispatcher("ssn.jsp");
            rd.forward(request, response);
        } else {
            String error = "email o password errate";
            session.setAttribute("email", email);
            session.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            rd.forward(request, response);
        }
    }
}
