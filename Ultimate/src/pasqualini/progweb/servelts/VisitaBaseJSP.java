package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.DAO.VisitaBaseDAO;
import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.PrescrizioneEsame;
import pasqualini.progweb.model.PrescrizioneFarmaco;
import pasqualini.progweb.model.VisitaBase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/*
* TODO:
*  Redirect to GetPaziente
* */

@WebServlet(name = "VisitaBase",urlPatterns = "/visita_base")
public class VisitaBaseJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        String vid = request.getParameter("id");
        VisitaBase v = new PazienteDAO().getVisitaBase(vid);
        if (v != null) {
            ArrayList<PrescrizioneEsame> pre = new VisitaBaseDAO().getPrescrizioniEsami(v.getId());
            ArrayList<PrescrizioneFarmaco> prf = new VisitaBaseDAO().getPrescrizioniFarmaci(v.getId());

            HttpSession session = request.getSession();

            session.setAttribute("visita", v);
            session.setAttribute("prescrisioni_esami", pre);
            session.setAttribute("prescrisioni_farmaci", prf);

            RequestDispatcher rd = null;
            if (uid.startsWith("paz_")){
                rd = request.getRequestDispatcher("visita.jsp");
            } else if (uid.startsWith("med_")) {
                session.setAttribute("paziente", v.getPaziente());
                rd = request.getRequestDispatcher("visita_medico.jsp");
            } else {
                rd = request.getRequestDispatcher("404.html");
            }
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
