package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.ListMediciDAO;
import pasqualini.progweb.DAO.MedicoDAO;
import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.DAO.SsnDAO;
import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.Medico;
import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.model.Ssn;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/*
* TODO:
* */

@WebServlet(name = "SettingServet",urlPatterns = "/settings")
public class SettingsJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("id") != null){
            String uid = GeneralMethods.getUserIdCookie(request.getCookies());
            if (uid != null && uid.startsWith("paz_")){
                PazienteDAO pDAO = new PazienteDAO();
                Paziente p = pDAO.getPaziente(uid, true);
                ArrayList<Medico> lista_medici_provincia =
                        new ListMediciDAO().getMediciProvincia(p.getProvinciaResidenza());

                session.setAttribute("paziente", p);
                session.setAttribute("medico", pDAO.getMyMedico(p.getId()));
                session.setAttribute("lista_medici_provincia", lista_medici_provincia);
                RequestDispatcher rd = request.getRequestDispatcher("settings_paziente.jsp");
                rd.forward(request, response);
            } else if (uid != null && uid.startsWith("med_")){
                Medico m = new MedicoDAO().getMedico(uid, true);

                session.setAttribute("medico", m);
                RequestDispatcher rd = request.getRequestDispatcher("settings_medico.jsp");
                rd.forward(request, response);
            } else if (uid != null && uid.startsWith("ssn_")){
                Ssn ssn = new SsnDAO().getSSN(uid, true);

                session.setAttribute("ssn", ssn);
                RequestDispatcher rd = request.getRequestDispatcher("settings_ssn.jsp");
                rd.forward(request, response);
            } else {
                System.out.println("UID is null 222");
                RequestDispatcher rd = request.getRequestDispatcher("404.html");
                rd.forward(request, response);
            }
        } else {
            System.out.println("UID is null");
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
