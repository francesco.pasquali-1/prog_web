package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.DAO.SsnDAO;
import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.*;
import pasqualini.progweb.DAO.TicketDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "FaiEsameServlet",urlPatterns = "/fai_esame")
public class FaiEsameJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cf_paziente = request.getParameter("cf");

        try{
            if (cf_paziente != null && !cf_paziente.equals("")){
                PazienteDAO pDAO = new PazienteDAO();
                Paziente p = pDAO.getPazienteByCF(cf_paziente);
                ArrayList<PrescrizioneEsame> prescrizioni = pDAO.getMyPrescrizioniEsami(p.getId());

                HttpSession session = request.getSession();

                session.setAttribute("paziente", p);
                session.setAttribute("esami_prescritti", prescrizioni);
                RequestDispatcher rd = request.getRequestDispatcher("fai_esame.jsp");
                rd.forward(request, response);
            } else {
                RequestDispatcher rd = request.getRequestDispatcher("404.html");
                rd.forward(request, response);
            }
        } catch (Exception e){
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        String pid = request.getParameter("pid");
        String esame = request.getParameter("esame"); // This is the id of prescription
        String descrizione_visita = request.getParameter("description");
        descrizione_visita = descrizione_visita.replaceAll("'", "&#39;");
        String ticketValue = request.getParameter("ticket_value");
        String ticketPay = request.getParameter("ticket_pay");

        if (uid.startsWith("ssn_")){
            PrescrizioneEsame prescrizioneEsame = new PazienteDAO().getPrescrizioneEsame(esame);
            Ticket ticket = new TicketDAO().getNewTicket(ticketValue, ticketPay.equals("true"));
            Ssn ssn = new SsnDAO().getSSN(uid, true);
            long date = System.currentTimeMillis();
            ErogazioneEsame e = new ErogazioneEsame(prescrizioneEsame, ticket, ssn, date, descrizione_visita);
            PazienteDAO p = new PazienteDAO();
            TicketDAO t = new TicketDAO();
            boolean riuscita_erogazione = p.addErogazioneEsame(e);

            String add_esame;
            if (riuscita_erogazione){
                add_esame = "Esame aggiunto correttamente";
            } else {
                add_esame = "C'e' stato qualche problema. Riprovare piu' tardi";
            }
            response.getWriter().write(add_esame);
        } else {
            String add_esame;
            add_esame = "Utente non abilitato";
            response.getWriter().write(add_esame);
        }
    }
}
