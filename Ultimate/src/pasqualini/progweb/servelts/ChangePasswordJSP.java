package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.PasswordDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
* TODO:
*  Pagina dedicata in caso di password errata o meno (conferma con redirect)
* */

@WebServlet(name = "ChangePasswordServet",urlPatterns = "/change_password")
public class ChangePasswordJSP extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = request.getParameter("id");
        String old_password = request.getParameter("old_password");
        String new_password = request.getParameter("new_password");
        String confirm_password = request.getParameter("confirm_password");

        String change_password = null;
        try {
            if (!new_password.equals(confirm_password)){
                change_password = "Le due password non corrispondono";
            } else if (PasswordDAO.updatePassword(uid, old_password, new_password)){
                change_password = "Password cambiata correttamente";
            } else {
                change_password = "Password precedente errata.";
            }

            response.getWriter().write(change_password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
