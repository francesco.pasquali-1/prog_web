package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.MedicoDAO;
import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.DAO.TicketDAO;
import pasqualini.progweb.DAO.VisitaSpecialisticaDAO;
import pasqualini.progweb.model.VisitaSpecialistica;
import pasqualini.progweb.generalclass.GeneralMethods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
* TODO:
*  Pagina dedicata in caso di password errata o meno (conferma con redirect)
* */

@WebServlet(name = "VisitaSpecialisticaServet",urlPatterns = "/add_specialistica")
public class AddVisitaSpecialisticaJSP extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        String pid = request.getParameter("pid");
        String descrizione = request.getParameter("description");
        String ticket = request.getParameter("ticket");
        boolean t_paid = false;
        if (ticket != null && ticket.equals("true")){
            t_paid = true;
        }
        long date = System.currentTimeMillis();

        if (uid.startsWith("med_")){
            PazienteDAO pDAO = new PazienteDAO();
            MedicoDAO mDAO = new MedicoDAO();
            VisitaSpecialistica v = new VisitaSpecialistica(pDAO.getPaziente(pid, true),
                    mDAO.getMedico(uid, true), new TicketDAO().getNewTicket("50.00", t_paid),
                    descrizione, date);

            VisitaSpecialisticaDAO vDAO = new VisitaSpecialisticaDAO();

            String add_visita;
            if (vDAO.addVisitaSpecialistica(v)) {
                add_visita = "Visita aggiunta correttamente";
            } else {
                add_visita = "E' stato riscontrato qualche problema. Riprovare più tardi";
            }
            response.getWriter().write(add_visita);
        } else {
            response.getWriter().write("Non puoi aggiungere visite con questi privilegi");
        }
    }
}