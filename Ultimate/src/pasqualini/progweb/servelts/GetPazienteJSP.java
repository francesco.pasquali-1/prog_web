package pasqualini.progweb.servelts;

import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.*;
import pasqualini.progweb.DAO.PazienteDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/*
* TODO:
* */

@WebServlet(name = "GetPazienteServlet",urlPatterns = "/get_paziente")
public class GetPazienteJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pid = request.getParameter("pid");

        HttpSession session = request.getSession();

        if (GeneralMethods.getUserIdCookie(request.getCookies()).startsWith("med_")){
            PazienteDAO pDAO= new PazienteDAO();
            Paziente p = pDAO.getPaziente(pid, true);

            ArrayList<PrescrizioneEsame> pr = pDAO.getMyPrescrizioniEsami(pid);
            ArrayList<ErogazioneEsame> er = pDAO.getErogazioniEsame(pid);
            ArrayList<VisitaBase> vi = pDAO.getVisiteBase(pid);
            ArrayList<PrescrizioneFarmaco> fp = pDAO.getMyPrescrizioniFarmaci(pid);
            ArrayList<Ticket> tp = pDAO.getTicket(pid, false);
            ArrayList<Ticket> tpp = pDAO.getTicket(pid, true);

            session.setAttribute("paziente", p);
            session.setAttribute("prescrizioni_esami", pr);
            session.setAttribute("esami_fatti", er);
            session.setAttribute("visite_base", vi);
            session.setAttribute("prescrizioni_farmaci", fp);
            session.setAttribute("ticket_pagare", tp);
            session.setAttribute("ticket_pagati", tpp);
            session.setAttribute("error", null);

            RequestDispatcher rd = request.getRequestDispatcher("paziente_medico.jsp");
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
