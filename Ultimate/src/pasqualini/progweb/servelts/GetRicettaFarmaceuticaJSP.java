package pasqualini.progweb.servelts;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.DAO.PrescrizioneEsameDAO;
import pasqualini.progweb.DAO.PrescrizioneFarmacoDAO;
import pasqualini.progweb.DAO.VisitaBaseDAO;
import pasqualini.progweb.generalclass.GeneralMethods;
import pasqualini.progweb.model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;


/*
* TODO:
* */

@WebServlet(name = "GetRicettaFarmaceuticaServlet",urlPatterns = "/get_ricetta_farmaceutica")
public class GetRicettaFarmaceuticaJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String vid = request.getParameter("vid");
        String fid = request.getParameter("fid");

        HttpSession session = request.getSession();

        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        if (uid.startsWith("paz_")){
            PazienteDAO pDAO= new PazienteDAO();
            Paziente p = pDAO.getPaziente(uid, true);

            VisitaBase visitaBase = new VisitaBaseDAO().getVisita(vid);
            PrescrizioneFarmacoDAO prescrizioneFarmacoDAO = new PrescrizioneFarmacoDAO();
            PrescrizioneFarmaco prescrizioneFarmaco = prescrizioneFarmacoDAO.getPrescrizioneFarmaco(vid, fid);

            try {
                Document document = new Document();

                // intestazione
                String filename = "./prova_pdf.pdf";
                PdfWriter.getInstance(document, new FileOutputStream(filename));
                document.open();
                Paragraph nome = new Paragraph("Paziente: " + p.getNomeCognome());
                Paragraph cf = new Paragraph("Codice Fiscale: " + p.getCf());
                document.add(nome);
                document.add(cf);

                PdfPTable table = new PdfPTable(2);

                table.addCell("Nome Farmaco: ");
                table.addCell(prescrizioneFarmaco.getFarmaco().getNome());

                // prima riga (Medico)
                table.addCell("Medico Visita:");
                table.addCell(pDAO.getMyMedico(p.getId()).getNomeCognome());

                table.addCell("Data prescrizione: ");
                table.addCell(String.valueOf(prescrizioneFarmaco.getVisita().getDate()));

                // seconda riga (Data)
                table.addCell("Codice prescrizione: ");
                table.addCell(prescrizioneFarmaco.getVisita().getId() + prescrizioneFarmaco.getFarmaco().getId());

                document.add(table);

                ByteArrayOutputStream bout =
                        QRCode.from("Hello, World!")
                                .withSize(250, 250)
                                .to(ImageType.PNG)
                                .stream();

                document.add(Image.getInstance(bout.toByteArray()));

                document.close();
            } catch (DocumentException | FileNotFoundException e) {
                e.printStackTrace();
            }

            RequestDispatcher rd = request.getRequestDispatcher("200.html");
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
