package pasqualini.progweb.servelts;

import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.model.ErogazioneEsame;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
* TODO:
*  Redirect to GetPaziente
* */

@WebServlet(name = "ErogazioneEsame",urlPatterns = "/erogazione_esame")
public class ErogazioneEsameJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String prescid = request.getParameter("id");
        ErogazioneEsame er = new PazienteDAO().getErogazioneEsame(prescid);
        if (er != null) {
            HttpSession session = request.getSession();

            session.setAttribute("erogazione_esame", er);
            RequestDispatcher rd = request.getRequestDispatcher("erogazione_esame.jsp");
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
