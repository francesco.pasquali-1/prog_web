package pasqualini.progweb.servelts;

import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.model.PrescrizioneEsame;
import pasqualini.progweb.DAO.PazienteDAO;
import pasqualini.progweb.generalclass.GeneralMethods;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
* TODO:
*  Redirect to GetPaziente
* */

@WebServlet(name = "PrescrizioneEsame",urlPatterns = "/presrizione_esami")
public class PrescizioneEsameJSP extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uid = GeneralMethods.getUserIdCookie(request.getCookies());
        String prescid = request.getParameter("prescrizione_id");
        PazienteDAO pazienteDAO = new PazienteDAO();
        PrescrizioneEsame pr = pazienteDAO.getPrescrizioneEsame(prescid);
        System.out.println(pr);
        if (pr != null) {
            Paziente p = pr.getVisita().getPaziente();

            HttpSession session = request.getSession();

            session.setAttribute("prescrizione_esame", pr);

            RequestDispatcher rd = null;
            if (uid.startsWith("paz_")){
                rd = request.getRequestDispatcher("prescrizione_esame.jsp");
            } else if (uid.startsWith("med_")) {
                rd = request.getRequestDispatcher("prescrizione_esame_medico.jsp");
            } else {
                rd = request.getRequestDispatcher("404.html");
            }
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("404.html");
            rd.forward(request, response);
        }
    }
}
