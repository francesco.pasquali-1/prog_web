package pasqualini.progweb.generalclass;

import pasqualini.progweb.model.Paziente;
import pasqualini.progweb.DAO.ListPazientiDAO;
import pasqualini.progweb.DAO.PazienteDAO;

import java.util.ArrayList;
import java.util.HashMap;

public class Pazienti {
    private static HashMap<String, Paziente> pazienti = new HashMap<>();
    private static HashMap<String, String> nomiPazienti = new HashMap<>();

    public static Paziente getPaziente(String id){
        Paziente p = pazienti.getOrDefault(id, null);
        if (p == null){
            PazienteDAO pd = new PazienteDAO();
            p = pd.getPaziente(id, true);
            if (p != null){
                addPaziente(p);
            }
        }
        return p;
    }
    public static String getNomePaziente(String id){
        String nomePaziente = nomiPazienti.getOrDefault(id, null);
        if (nomePaziente == null){
            PazienteDAO pd = new PazienteDAO();
            nomePaziente = pd.getPaziente(id, true).getNome();
            addNomePaziente(id, nomePaziente);
        }
        return nomePaziente;
    }
    public static ArrayList<String> getAllNames(){
        // This might return whole hashmap
        if (nomiPazienti.isEmpty()){
            ListPazientiDAO p = new ListPazientiDAO();
            HashMap<String, String> lp = p.getAllPazienti();
            for(String pid : lp.keySet()){
                nomiPazienti.put(pid, lp.get(pid));
            }
        }
        return new ArrayList<>(nomiPazienti.values());
    }

    private static boolean addPaziente(Paziente p){
        return pazienti.put(p.getId(), p) != null;
    }
    private static boolean addNomePaziente(String id, String nome){
        return nomiPazienti.put(id, nome) != null;
    }

    public static Paziente updatePaziente(Paziente p){
        return pazienti.put(p.getId(), p);
    }
}
