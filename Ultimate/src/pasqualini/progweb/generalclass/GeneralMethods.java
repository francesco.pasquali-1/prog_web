package pasqualini.progweb.generalclass;

import javax.servlet.http.Cookie;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.Date;

public class GeneralMethods {
    public static String getSHA512(String password){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());

            StringBuffer sb = new StringBuffer();
            for (byte b : md.digest()) {
                sb.append(String.format("%02x", b & 0xff));
            }

            System.out.println(sb);
            return new String(sb);
        } catch (NoSuchAlgorithmException e){
            System.out.println("NoSuchAlgorithmException SHA-512");
        }
        return null;
    }

    public static String getUserIdCookie(Cookie[] cookies){
        String uid = null;
        if (cookies != null){
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user_id")) {
                    uid = cookie.getValue();
                    break;
                }
            }
        }
        return uid;
    }

    public static String getDate(long date){
        Date d = new Date(date);
        return DateFormat.getInstance().format(d);
    }
}
