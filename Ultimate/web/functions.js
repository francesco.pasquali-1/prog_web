var toggle = true;
var prescription = {
    "esami" : [],
    "farmaci" : [],
    "quantity" : []
}
function getLoginForm() {
    if (toggle){
        document.getElementById("login-background").style.top = "0px";
        document.getElementById("login-background").style.height = $(window).height() + "px";
        document.getElementById("login-background").style.width = $(window).width() + "px";
        document.getElementById("login-background").style.opacity = "0.8";
        document.getElementById("login").style.backgroundColor = "black";
        document.getElementById("login").style.width = "40%";
        document.getElementById("login").style.height = "30%";
        document.getElementById("login").style.margin = "10% auto";
        document.getElementById("login").style.top = "100%";
    } else {
        console.log(document.getElementById("login-background").style.height);
        document.getElementById("login-background").style.top = "-" + $(window).height() + "px";
    }
    toggle = !toggle;
}

function addExam() {
    exam = document.getElementById("add_new_exam").value;
    if (exam == null || exam === "" || exam === " "){
        document.getElementById("exam_error").innerText = "Campo esame vuoto";
        return false;
    } else if (prescription["esami"].includes(exam)){
        document.getElementById("exam_error").innerText = "Esame già inserito";
        return false;
    } else {
        document.getElementById("exam_error").innerText = "";
        let pos = prescription["esami"].length;
        prescription["esami"][pos] = exam;
    }
    stringa = "<table>";
    for (let i = 0; i < prescription["esami"].length; i++){
        stringa += "<tr><td>" + esami_fittizi[prescription["esami"][i]] + "</td></tr>";
    }
    stringa += "</table>";
    document.getElementById("prescrizione_esami").innerHTML = stringa;
}

function addFarmaco() {
    farmaco = document.getElementById("add_new_farmaco").value;
    quantiy = document.getElementById("add_new_farmaco_quantity").value;
    if (farmaco == null || farmaco === "" || farmaco === " "){
        document.getElementById("farmaco_error").innerText = "Campo farmaco vuoto";
        return false;
    } else if (prescription["farmaci"].includes(farmaco)){
        document.getElementById("farmaco_error").innerText = "Farmaco già inserito";
        return false;
    } else {
        document.getElementById("farmaco_error").innerText = "";
        let pos = prescription["farmaci"].length;
        prescription["farmaci"][pos] = farmaco;
        prescription["quantity"][pos] = quantiy;
    }
    stringa = "<table>"
    for (let i = 0; i < prescription["farmaci"].length; i++){
        stringa += "<tr><td>" + farmaci_fittizi[prescription["farmaci"][i]] + "</td><td>" + prescription["quantity"][i] + "</td></tr>";
    }
    stringa += "</table>";
    document.getElementById("prescrizione_farmaci").innerHTML = stringa;
    //document.getElementById("prescrizione_esame").innerHTML(stringa);
}

function show_refer(id) {
    var xmlhttp = new XMLHttpRequest();
    var url = "login_.jsp";
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.onreadystatechange = function () { //Call a function when the state changes.
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
        }
    };
    var parameters = {
        "id" : id,
    }
    xmlhttp.send(JSON.stringify(parameters));
}
