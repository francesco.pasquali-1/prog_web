<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Settings - ${nome} ${cognome}</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="login">
      <span class="d-block d-lg-none">${nome} ${cognome}</span>
      <span class="d-none d-lg-block">
        <!-- Photo with a pen -->
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="${foto}" alt="${foto}">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <h3>${nome} ${cognome}</h3>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <a href="#">Settings</a>
      </ul>
    </div>
    <div>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="login" style="color: black;">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="logout" style="color: black;">Logout</a>
      </li>
    </ul>
  </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <h3>SETTINGS</h3>
        <div>
          <p>
            Nome: ${paziente.getNome()}<br>
            Cognome: ${paziente.getCognome()}<br>
            E-mail: ${paziente.getEmail()}<br>
            Residenza: ${paziente.getProvinciaResidenza()}<br>
            Medico di Base: ${medico.getNomeCognome()}<br>
          </p>

          <p style="color: red;">
            ${change_password}
          </p>

          <p onclick="change_password()">
            Cambia Password
          </p>

          <p onclick="change_medico()">
            Cambia Medico
          </p>

          <div id="password_changed" style="color: red; font-style: italic;">

          </div>
          <div id="change_password_div" style="visibility: hidden;">
            <input id="id_pssw" type="hidden" value="${paziente.getId()}" />
            Old password: <input id="old_password" size="35" type="password" name="old_password" value="" /> <br>
            New password: <input id="new_password" size="35" type="password" name="new_password" value="" /> <br>
            Confirm password: <input id="confirm_password" size="35" type="password" name="confirm_password" value="" /> <br>
            <button onclick="changePassword()">Submit</button>
          </div>

          <div id="medico_changed" style="font-style: italic; color: red;">

          </div>
          <div id="change_medico_div" style="visibility: hidden;">
            <input id="id_med" type="hidden" value="${paziente.getId()}" />
            Selezionare il nuovo medico: <br>
            <select id="new_medico">
              <option value=""></option>" +
              <c:forEach items="${lista_medici_provincia}" var="medici">
                <option value="${medici.getId()}">
                  ${medici.getNomeCognome().toUpperCase()}
                </option>
              </c:forEach>
              </select>
            <button onclick="changeMedico()">Submit</button>
          </div>

          <div id="photo_changed" style="font-style: italic; color: red;">

          </div>
          <div id="change_photo_div" style="visibility: hidden;">
            Carica foto: <br>
            scegli una foto da inviare: <br />
            <input type="new_photo" name="new_photo" size="50" /> <br>
            <button>Salva</button>
          </div>


          <script>
            function change_password(){
              var password_changed = $("#password_changed").html("")
                      .text();
              document.getElementById("change_password_div").style.visibility = "visible";
              document.getElementById("change_medico_div").style.visibility = "hidden";
              document.getElementById("change_photo_div").style.visibility = "hidden";
            }
            function change_medico(){
              var password_changed = $("#password_changed").html("")
                      .text();
              document.getElementById("change_password_div").style.visibility = "hidden";
              document.getElementById("change_medico_div").style.visibility = "visible";
              document.getElementById("change_photo_div").style.visibility = "hidden";
            }
            function change_photo(){
              document.getElementById("change_medico_div").style.visibility = "hidden";
              document.getElementById("change_password_div").style.visibility = "hidden";
              document.getElementById("change_photo_div").style.visibility = "visible";
            }
            function changePassword() {
              var password_changed = $("#password_changed").html("")
                      .text();
              if (null == $('#old_password').val() || $('#old_password').val() == '') {
                alert("enter username");
                return false;
              } else {
                var id = $('#id_pssw').val();
                var old_password = $('#old_password').val();
                var new_password = $('#new_password').val();
                var confirm_password = $('#confirm_password').val();
                var dataString =  "id=" + id
                                  + "&old_password=" + old_password
                                  + "&new_password=" + new_password
                                  + "&confirm_password=" + confirm_password;
                $.ajax({
                  type : "POST",
                  url : "change_password",
                  data : dataString,
                  dataType : "text",
                  success : function(data) {
                    var password_changed = $("#password_changed").html(data)
                            .text();
                  },
                  error : function(jqXHR, textStatus, errorThrown) {
                    var password_changed = $("#password_changed").html(data)
                            .text();
                  }
                });
              }
            }
            function changeMedico() {
              if (null == $('#new_medico').val() || $('#new_medico').val() == '') {
                alert("enter username");
                return false;
              } else {
                var id = $('#id_med').val();
                var new_medico = $('#new_medico').val();
                var new_medico_name = $('#new_medico').text();
                var dataString = "id=" + id
                        + "&new_medico=" + new_medico;
                $.ajax({
                  type : "POST",
                  url : "change_medico",
                  data : dataString,
                  dataType : "text",
                  success : function(data) {
                    var medico_changed = $("#medico_changed").html(data)
                            .text();
                    location.reload();
                  },
                  error : function(jqXHR, textStatus, errorThrown) {
                    var medico_changed = $("#medico_changed").html(data)
                            .text();
                  }
                });
              }
            }
            function changePhoto() {
              if (null == $('#new_photo').val() || $('#new_photo').val() == '') {
                return false;
              } else {
                var new_photo = $('#new_photo').value();
                var dataString = "new_photo=" + new_photo;
                $.ajax({
                  type : "POST",
                  url : "change_foto",
                  data : dataString,
                  dataType : "text",
                  success : function(data) {
                    var medico_changed = $("#photo_changed").html(data)
                            .text();
                    location.reload();
                  },
                  error : function(jqXHR, textStatus, errorThrown) {
                    var medico_changed = $("#photo_changed").html(data)
                            .text();
                  }
                });
              }
            }
          </script>
        </div>
      </div>
    </section>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
