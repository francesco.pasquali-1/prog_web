<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Paziente - ${nome} ${cognome}</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

  <style>
    table {
      width: 100%;
    }
    td {
      width: 30%;
    }

    .scroll {
      overflow:auto;
      height: 40%;
    }
    .intable {
      margin-top: 5px;
      margin-bottom: 5px;
    }
  </style>

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="login">
      <span class="d-block d-lg-none">${medico.getNome()} ${medico.getCognome()}</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2"
             src="${medico.getFotoPath()}" alt="${medico.getFotoPath()}">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <h3>${medico.getNome()} ${medico.getCognome()}</h3>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#info_paziente">Info Paziente</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#esami">Esami</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#visite_mediche">Visita Mediche</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#farmaci">Farmaci</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#ticket">Tickets</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login">Home</a>
        </li>
      </ul>
    </div>
    <div>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="settings" style="color: black;">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" onclick="logout" style="color: black;">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">
    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="info_paziente">
      <div class="w-100">
        <h3 class="mb-0">Dati Paziente</h3>
        <div class="subheading">
          <table>
            <tr>
              <td>
                Paziente
              </td>
              <td>
                ${paziente.getNomeCognome()}
              </td>
            </tr>
            <tr>
              <td>
                Codice Fiscale
              </td>
              <td>
                ${paziente.getCf()}
              </td>
            </tr>
            <tr>
              <td>
                Luogo Nascita
              </td>
              <td>
                ${paziente.getLuogoNascita()}
              </td>
            </tr>
            <tr>
              <td>
                Sesso
              </td>
              <td>
                ${paziente.getSesso()}
              </td>
            </tr>
          </table>
        </div>
        <div>
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" style="height: 200px; width: 200px"
               src="${paziente.getFotoPath()}" alt="${paziente.getFotoPath()}">
        </div>
      </div>
    </section>

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="esami">
      <div class="w-100">
        <h3 class="mb-0">Esami da fare</h3>
        <div class="subheading">
          <table>
            <tr>
              <td>
                Data visita
              </td>
              <td>
                Medico visita
              </td>
              <td>
                Esame
              </td>
            </tr>
          </table>
        </div>
        <hr class="intable">
        <div class="subheading mb-5 scroll">
          <table>
            <c:forEach items="${prescrizioni_esami}" var="e">
              <tr>
                <td>
                  ${e.getVisita().getData()}
                </td>
                <td>
                    ${e.getVisita().getMedico().getNome()} ${e.getVisita().getMedico().getCognome()}
                </td>
                <td>
                  ${e.getEsame()}
                </td>
                <td>
                  <a href="presrizione_esami?prescrizione_id=${e.getId()}">Watch</a>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <h3 class="mb-0">Esami Fatti</h3>
        <div class="subheading">
          <table>
            <tr>
              <td>
                Data
              </td>
              <td>
                Locazione
              </td>
              <td>
                Esame
              </td>
            </tr>
          </table>
        </div>
        <hr class="intable">
        <div class="subheading mb-5 scroll">
          <table>
            <c:forEach items="${esami_fatti}" var="esame">
              <tr>
                <td>
                    ${esame.getData()}
                </td>
                <td>
                    ${esame.getSsn().getNome()}
                </td>
                <td>
                    ${esame.getEsame().getEsame().getNome()}
                </td>
                <td>
                  <a href="erogazione_esame?id=${esame.getEsame().getId()}">Watch</a>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="visite_mediche">
      <div class="w-100">
        <h3 class="mb-0">Visite mediche</h3>
        <div class="subheading">
          <table>
            <tr>
              <td>
                Data
              </td>
              <td>
                Medico
              </td>
            </tr>
          </table>
        </div>
        <hr class="intable">
        <div class="subheading mb-5 scroll">
          <table>
            <c:forEach items="${visite_base}" var="visita_base">
              <tr>
                <td>
                  ${visita_base.getData()}
                </td>
                <td>
                  ${visita_base.getMedico().getNome()} ${visita_base.getMedico().getCognome()}
                </td>
                <td>
                  <a href="visita_base?id=${visita_base.getId()}">Watch</a>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>

    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-items-center" id="farmaci">
      <div class="w-100">
        <h3 class="mb-0">Farmaci da prendere</h3>
        <div class="subheading mb-5">
          <table>
            <tr>
              <td>
                Nome Farmaco
              </td>
              <td>
                Quantit&agrave;
              </td>
            </tr>
            <c:forEach items="${prescrizioni_farmaci}" var="farmaco">
              <tr>
                <td>
                  ${farmaco.getFarmaco().getNome()}
                </td>
                <td>
                  ${farmaco.getQuantity()}
                </td>
                <td>
                  <a href="visita_base?id=${farmaco.getVisita().getId()}">Watch</a>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <h3 class="mb-0">Farmaci presi</h3>
        <div class="subheading mb-5">
          <table>
            <tr>
              <td>
                Nome Farmaco
              </td>
              <td>
                Quantit&agrave;
              </td>

            </tr>
<%--            <c:forEach items="${farmaci_presi}" var="farmaco">--%>
<%--              <tr>--%>
<%--                <td>--%>
<%--                    ${farmaco.getFarmaco().getNome()}--%>
<%--                </td>--%>
<%--                <td>--%>
<%--                    ${farmaco.getQuantity()}--%>
<%--                </td>--%>
<%--              </tr>--%>
<%--            </c:forEach>--%>
          </table>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="ticket">
      <div class="w-100">
        <h3 class="mb-0">Ticket da pagare</h3>
        <div class="subheading mb-5">
          <table>
            <tr>
              <td>
                ID Ticket
              </td>
              <td>
                Valore
              </td>
            </tr>
            <c:forEach items="${ticket_pagare}" var="ticket">
              <tr>
                <td>
                    ${ticket.getId()}
                </td>
                <td>
                    ${ticket.getPrice()}
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <h3 class="mb-0">Ticket pagati</h3>
        <div class="subheading mb-5">
          <table>
            <tr>
              <td>
                ID Ticket
              </td>
              <td>
                Valore
              </td>
            </tr>
            <c:forEach items="${ticket_pagati}" var="ticket">
              <tr>
                <td>
                    ${ticket.getId()}
                </td>
                <td>
                    € ${ticket.getPrice()}
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </section>

  </div>

  <div id="show" style="background-color: black; position: center;">
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
