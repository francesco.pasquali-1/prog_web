<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Esame</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <span class="d-block d-lg-none">${ssn.getNome()}</span>
      <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="${ssn.getFotoPath()}" alt="${ssn.getFotoPath()}">
        </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#esame">Fai un esame</a>
        </li>
      </ul>
    </div>
    <div>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="login" style="color: black;">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="logout" style="color: black;">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <h2>Fai un esame</h2>
        <h3>${paziente.getNomeCognome()}</h3> <br>

      <input id="pid" name="id" value="${paziente.getId()}" type="hidden">
      <span>
        Selezionare Esame:
        <select id="esame">
          <option name="esame" value=""></option>
          <c:forEach items="${esami_prescritti}" var="esame">
            <option value="${esame.getId()}">${esame.getEsame().getNome().toUpperCase()}</option>
          </c:forEach>
        </select>
      </span>
      <span>
          Data: <b id="data_visita"></b>
          <script>
            let d_visita = new Date();
            document.getElementById("data_visita").innerHTML = d_visita.getDate() + "/" + d_visita.getMonth()+1 + "/" + d_visita.getFullYear();
          </script>
      </span>
      <div id="form_descrizione_visita" style="margin-top: 20px;">
        Descrizione: </br>
        <textarea name="descrizione_visita" id="descrizione_esame" rows="5" style="width: 100%;"></textarea>
      </div>
      <div id="ticket">
        Prezzo Ticket (Segnare la casella solo in caso abbia pagato): <i> 11.00</i>
        <input id="ticket_pay" type="checkbox" name="ticket_pay" value=""/>
      </div>

      <div>
        <button onclick="addEsame()">Submit</button>
      </div>

      <div id="post_esame" style="color: red;">

      </div>
    </div>

    </section>
  </div>

  <script>
    function addEsame() {
      var pid = $('#pid').val();
      var esame = $('#esame').val();
      var description = $('#descrizione_esame').val();
      var ticket_value = 11.00;
      var ticket_pay = $('#ticket_pay').val();
      var dataString = "pid=" + pid +
                        "&esame=" + esame +
                        "&description=" + description +
                        "&ticket_value=" + ticket_value +
                        "&ticket_pay=" + ticket_pay;
      $.ajax({
        type : "POST",
        url : "fai_esame",
        data : dataString,
        dataType : "text",
        success : function(data) {
          var ajaxResponse = $("#post_esame").html(data)
                  .text();
        },
        error : function(data) {
          var ajaxResponse = $("#post_esame").html(data)
                  .text();
        }
      });
    }
  </script>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
