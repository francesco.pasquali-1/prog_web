<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Medico - ${medico.getNome()} ${medico.getCognome()}</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="login">
      <span class="d-block d-lg-none">${medico.getNome()} ${medico.getCognome()}</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="${medico.getFotoPath()}" alt="${medico.getFotoPath()}">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#visita">Fai una visita</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#esame">Fai una visita specialistica</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#pazienti">I miei pazienti</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#cerca_pazienti">Cerca Paziente</a>
        </li>
      </ul>
    </div>
    <div>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="settings" style="color: black;">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="logout" style="color: black;">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">
    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="visita">
      <div class="w-100">
        <h3 class="mb-5">Fai una visita</h3>
        <div id="form_visita">
          <span>
            Paziente:
             <select id="my_paz_visita">
                <option value=""></option>
                <c:forEach items="${lista_miei_pazienti}" var="p">
                  <option value="${p.getId()}">
                      ${p.getNomeCognome().toUpperCase()}
                  </option>
                </c:forEach>
              </select>
          </span>
          <span>
            Data: <b id="data_visita"></b>
            <script>
              let d_visita = new Date();
              document.getElementById("data_visita").innerHTML = d_visita.getDate() + "/" + d_visita.getMonth()+1 + "/" + d_visita.getFullYear();
            </script>
          </span>
          <div id="just_presc">
          </div>
          <div id="form_descrizione_visita" style="margin-top: 20px;">
            Descrizione: </br>
            <textarea id="descrizione_visita" rows="5" style="width: 100%;"></textarea>
          </div>
          <div id="form_prescrizioni_visita" style="margin-top: 20px;">
            Prescrizioni:
            <div style="background-color: lightgrey; padding: 10px 3px 10px 3px;">
              <b style="color: black;">Esami:</b> </br>
              <div>
                <select id="add_new_exam">
                  <option value=""></option>
                  <c:forEach items="${lista_esami}" var="esame">
                    <option value="${esame.getId()}">${esame.getNome().toUpperCase()}</option>
                  </c:forEach>
                </select>
                <button onclick="addExam()" style="width: 20px; height: 20px">Add</button> </br>
                <p id="exam_error" style="color: red;"></p>
              </div>
              <div id="prescrizione_esami">

              </div>
              <b style="color: black;">Farmaci:</b>
              <div>
                <select id="add_new_farmaco">
                  <option value=""></option>
                  <c:forEach items="${lista_farmaci}" var="farmaco">
                    <option value="${farmaco.getId()}">${farmaco.getNome().toUpperCase()}</option>
                  </c:forEach>
                </select>
                <select id="add_new_farmaco_quantity">
                  <option value="1">1</option>
                  <c:forEach begin="2" end="10" var="i">
                    <option value="${i}">${i}</option>
                  </c:forEach>
                </select>
                <button onclick="addFarmaco()" style="width: 20px; height: 20px">Add</button> </br>
                <p id="farmaco_error" style="color: red;"></p>
              </div>
              <div id="prescrizione_farmaci">

              </div>
            </div>
            <button id="send_visita" onclick="addVisita()">Salva</button>
          </div>
          <div id="response_add_visita" style="color: red; font-style: italic"></div>
        </div>
      </div>

    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="esame">
      <div class="w-100">
        <h3 class="mb-5">Fai una visita specialistica</h3>
        <div id="form_visita_specialisitca">
          <table id="table_esami" style="width: 70%;">
            <tr>
              <td>
                Paziente: </br>
                <select id="pid_specialistica">
                  <option value=""></option>
                  <c:forEach items="${lista_tutti_pazienti.keySet()}" var="paziente">
                    <option value="${paziente}">
                        ${lista_tutti_pazienti.get(paziente).toUpperCase()}
                    </option>
                  </c:forEach>
                </select>
              </td>
              <td>
                Data: </br>
                <b id="data_visita_specialistica"></b>
                <script>
                  let d_esame = new Date();
                  document.getElementById("data_visita_specialistica").innerHTML = d_esame.getDate() + "/" + d_esame.getMonth()+1 + "/"
                          + d_esame.getFullYear();
                </script>
              </td>
            </tr>
          </table>
          <div id="form_descrizione_visita_specialistica" style="margin-top: 20px;">
            Descrizione: </br>
            <textarea id="descrizione_visita_specialistica" rows="5" style="width: 100%;"></textarea>
          </div>
          <input id="mid" value="${id}" type="hidden"/>
          <div>
            <input id="ticket_visita_specialistica" type="checkbox" value="true">
              Ticket visita specialistica (Segnare solo se pagata)
          </div>
          <div>
            <button onclick="addVisitaSpecialistica()">Salva</button>
          </div>
          <div id="response_visita_specialistica" style="color: red;">

          </div>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="pazienti">
      <div class="w-100">
        <h3 class="mb-5">I miei pazienti</h3>
        <div style="margin-top: 100px;">
          <table style="width: 70%;">
            <tr>
              <td>
                <select id="get_paziente">
                  <option value=""></option>
                  <c:forEach items="${lista_miei_pazienti}" var="paziente">
                    <option value="${paziente.getId()}">
                        ${paziente.getNomeCognome().toUpperCase()}
                    </option>
                  </c:forEach>
                </select>
              </td>
              <td>
                <button onclick="getPaziente()">
                  Cerca
                </button>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="cerca_pazienti">
      <div class="w-100">
        <h2 class="mb-5">Carca Paziente</h2>
        <div style="margin-top: 100px;">
          <table style="width: 70%;">
            <tr>
              <td>
                <select id="get_all_paziente">
                  <option value=""></option>
                  <c:forEach items="${lista_tutti_pazienti.keySet()}" var="paziente">
                    <option value="${paziente}">
                        ${lista_tutti_pazienti.get(paziente).toUpperCase()}
                    </option>
                  </c:forEach>
                </select>
                </select>
              </td>
              <td>
                <button onclick="getAllPaziente()">
                  Cerca
                </button>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script>
    function addVisita() {
      var pid = $('#my_paz_visita').val();
      var mid = $('#mid').val();
      var description = $('#descrizione_visita').val();
      prescription['mid'] = mid;
      prescription['pid'] = pid;
      prescription['description'] = description;
      var dataString = "data=" + JSON.stringify(prescription);
      console.log(dataString);
      $.ajax({
        type : "POST",
        url : "add_visita",
        data : dataString,
        dataType : "text",
        //contentType : "application/json; charset=utf-8",
        success : function(data) {
          var ajaxResponse = $("#response_add_visita").html(data)
                  .text();
        },
        error : function(jqXHR, textStatus, errorThrown) {
          var ajaxResponse = $("#response_add_visita").html(data)
                  .text();
        }
      });
    }

    function getPaziente() {
      var pid = $('#get_paziente').val();
      location.replace("get_paziente?pid=" + pid);
    }

    function getAllPaziente() {
      var pid = $('#get_all_paziente').val();
      location.replace("get_paziente?pid=" + pid);
    }

    function addVisitaSpecialistica() {
      var pid = $('#pid_specialistica').val();
      var description = $('#descrizione_visita_specialistica').val();
      var ticket = null;
      if ($('#ticket_visita_specialistica').checkbox){
        ticket = $('#ticket_visita_specialistica').val()
      }
      var dataString = "pid=" + pid
                        + "&description=" + description
                        + "&ticket=" + ticket;
      // console.log(dataString);
      $.ajax({
        type : "POST",
        url : "add_specialistica",
        data : dataString,
        dataType : "text",
        //contentType : "application/json; charset=utf-8",
        success : function(data) {
          var ajaxResponse = $("#response_visita_specialistica").html(data)
                  .text();
        },
        error : function(jqXHR, textStatus, errorThrown) {
          var ajaxResponse = $("#response_visita_specialistica").html(data)
                  .text();
        }
      });
    }

    farmaci_fittizi = {
      <c:forEach items="${lista_farmaci}" var="farmaco">
      "${farmaco.getId()}" : "${farmaco.getNome().toUpperCase()}",
      </c:forEach>
    }
    esami_fittizi = {
      <c:forEach items="${lista_esami}" var="esame">
      "${esame.getId()}" : "${esame.getNome().toUpperCase()}",
      </c:forEach>
    }
  </script>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

  <!-- My script -->
  <script src="functions.js"></script>

</body>

</html>
