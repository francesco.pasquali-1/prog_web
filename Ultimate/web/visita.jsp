<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Visita - ${nome} ${cognome}</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="login">
      <span class="d-block d-lg-none">Clarence Taylor</span>
      <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="${foto}" alt="${foto}">
        </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <h3>${nome} ${cognome}</h3>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#">Visita Base</a>
        </li>
      </ul>
    </div>
    <div>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="login" style="color: black;">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="settings" style="color: black;">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="logout" style="color: black;">Logout</a>
        </li>
      </ul>
    </div>
  </nav>


<div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <table style="width: 70%">
          <tr>
            <td>
              <h4>Data visita:</h4>
            </td>
            <td>
              <h3>${visita.getData()}</h3>
            </td>
          </tr>
          <tr>
            <td>
              <h4>Medico visita:</h4>
            </td>
            <td>
              <h3>${visita.getMedico().getCognome()}
                ${visita.getMedico().getNome()}</h3>
            </td>
          </tr>
          <tr>
            <td>
              <h4>Descrizione:</h4>
            </td>
            <td>
              <h3>${visita.getDescription()}</h3>
            </td>
          </tr>
          <tr>
            <td>Prescrizioni: </td>
          </tr>
          <tr>
            <c:forEach items="${prescrisioni_esami}" var="esame">
              <tr>
                <td>
                  Esame
                </td>
                <td>
                    ${esame.getVisita().getData()}
                </td>
                <td>
                    ${esame.getEsame().getNome()}
                </td>
              </tr>
            </c:forEach>
          </tr>
          <tr>
            <c:forEach items="${prescrisioni_farmaci}" var="farmaco">
              <tr>
                <td>
                  Farmaco
                </td>
                <td>
                    ${farmaco.getFarmaco().getNome()}
                </td>
                <td>
                    ${farmaco.getQuantity()}
                </td>
              </tr>
            </c:forEach>
          </tr>
        </table>
      </div>
    </section>

    <hr class="m-0">
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>

</body>

</html>
