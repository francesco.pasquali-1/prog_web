--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md532e12f215ba27cb750c9e093ce4b5127';






\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: prog_web; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE prog_web WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE prog_web OWNER TO postgres;

\connect prog_web

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: erogazione_esami; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.erogazione_esami (
    prescid character varying(16) NOT NULL,
    tid character varying(12) NOT NULL,
    sid character varying(15) NOT NULL,
    date bigint NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.erogazione_esami OWNER TO postgres;

--
-- Name: erogazione_farmaci; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.erogazione_farmaci (
    vid character varying(15) NOT NULL,
    fid character varying(12) NOT NULL,
    date bigint NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.erogazione_farmaci OWNER TO postgres;

--
-- Name: esami; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.esami (
    eid character varying(10) NOT NULL,
    ename character varying(20) NOT NULL
);


ALTER TABLE public.esami OWNER TO postgres;

--
-- Name: farmaci; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.farmaci (
    fid character varying(12) NOT NULL,
    fnome character varying(50) NOT NULL
);


ALTER TABLE public.farmaci OWNER TO postgres;

--
-- Name: has_base; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.has_base (
    pid character varying(15) NOT NULL,
    mid character varying(15) NOT NULL
);


ALTER TABLE public.has_base OWNER TO postgres;

--
-- Name: medici; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medici (
    mid character varying(15) NOT NULL,
    mnome character varying(20) NOT NULL,
    mcognome character varying(20) NOT NULL,
    msesso character varying(1) NOT NULL,
    mcodice_fiscale character varying(16) NOT NULL,
    mprovincia_operato character varying(50) NOT NULL
);


ALTER TABLE public.medici OWNER TO postgres;

--
-- Name: pazienti; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pazienti (
    pid character varying(15) NOT NULL,
    pnome character varying(20) NOT NULL,
    pcognome character varying(20) NOT NULL,
    psesso character varying(1) NOT NULL,
    pcodice_fiscale character varying(16) NOT NULL,
    pluogo_nascita character varying(25) NOT NULL,
    pdata_nascita bigint NOT NULL,
    pprovincia_residenza character varying(50) NOT NULL
);


ALTER TABLE public.pazienti OWNER TO postgres;

--
-- Name: prescrizione_esami; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.prescrizione_esami (
    prescid character varying(15) NOT NULL,
    vid character varying(15) NOT NULL,
    eid character varying(10) NOT NULL
);


ALTER TABLE public.prescrizione_esami OWNER TO postgres;

--
-- Name: prescrizione_farmaci; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.prescrizione_farmaci (
    vid character varying(15) NOT NULL,
    fid character varying(12) NOT NULL,
    quantity integer
);


ALTER TABLE public.prescrizione_farmaci OWNER TO postgres;

--
-- Name: ssn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ssn (
    sid character varying(15) NOT NULL,
    snome character varying(30) NOT NULL,
    sprovincia character varying(10) NOT NULL
);


ALTER TABLE public.ssn OWNER TO postgres;

--
-- Name: ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket (
    tid character varying(10) NOT NULL,
    tprice double precision NOT NULL,
    tpaid boolean NOT NULL
);


ALTER TABLE public.ticket OWNER TO postgres;

--
-- Name: utenti; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.utenti (
    uid character varying(15) NOT NULL,
    ufoto character varying(30) NOT NULL,
    uemail character varying(50) NOT NULL,
    upassword character varying(129) NOT NULL
);


ALTER TABLE public.utenti OWNER TO postgres;

--
-- Name: visita_base; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visita_base (
    vid character varying(15) NOT NULL,
    pid character varying(15) NOT NULL,
    mid character varying(15) NOT NULL,
    date bigint NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.visita_base OWNER TO postgres;

--
-- Name: visita_specialistica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visita_specialistica (
    pid character varying(15) NOT NULL,
    mid character varying(15) NOT NULL,
    tid character varying(15) NOT NULL,
    date bigint NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.visita_specialistica OWNER TO postgres;

--
-- Data for Name: erogazione_esami; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.erogazione_esami (prescid, tid, sid, date, description) FROM stdin;
33	2	ssn_3	1473518193	Aggiungere qui la descrizione di un esame
86	3	ssn_4	1460259755	Aggiungere qui la descrizione di un esame
26	4	ssn_4	1462444941	Aggiungere qui la descrizione di un esame
109	5	ssn_3	1425531914	Aggiungere qui la descrizione di un esame
44	6	ssn_5	1521407441	Aggiungere qui la descrizione di un esame
23	7	ssn_3	1511332827	Aggiungere qui la descrizione di un esame
98	8	ssn_4	1491664662	Aggiungere qui la descrizione di un esame
100	9	ssn_4	1550307009	Aggiungere qui la descrizione di un esame
92	10	ssn_1	1554056058	Aggiungere qui la descrizione di un esame
122	11	ssn_4	1568056080	Aggiungere qui la descrizione di un esame
74	12	ssn_1	1503278778	Aggiungere qui la descrizione di un esame
64	13	ssn_3	1567837707	Aggiungere qui la descrizione di un esame
114	14	ssn_4	1431855470	Aggiungere qui la descrizione di un esame
13	15	ssn_4	1431432820	Aggiungere qui la descrizione di un esame
54	16	ssn_4	1423231172	Aggiungere qui la descrizione di un esame
115	17	ssn_5	1425551368	Aggiungere qui la descrizione di un esame
75	18	ssn_3	1543018658	Aggiungere qui la descrizione di un esame
120	19	ssn_3	1578953895	Aggiungere qui la descrizione di un esame
35	20	ssn_2	1496751738	Aggiungere qui la descrizione di un esame
101	21	ssn_5	1550068704	Aggiungere qui la descrizione di un esame
16	22	ssn_2	1506500012	Aggiungere qui la descrizione di un esame
11	23	ssn_4	1450643615	Aggiungere qui la descrizione di un esame
130	24	ssn_4	1567611606	Aggiungere qui la descrizione di un esame
48	25	ssn_5	1547602198	Aggiungere qui la descrizione di un esame
118	26	ssn_1	1558902585	Aggiungere qui la descrizione di un esame
146	27	ssn_5	1493604781	Aggiungere qui la descrizione di un esame
28	28	ssn_5	1428801028	Aggiungere qui la descrizione di un esame
102	29	ssn_2	1463982698	Aggiungere qui la descrizione di un esame
58	30	ssn_3	1428367398	Aggiungere qui la descrizione di un esame
47	31	ssn_5	1517097801	Aggiungere qui la descrizione di un esame
96	32	ssn_4	1432780951	Aggiungere qui la descrizione di un esame
93	33	ssn_2	1559708615	Aggiungere qui la descrizione di un esame
71	34	ssn_1	1529055895	Aggiungere qui la descrizione di un esame
42	35	ssn_1	1526263684	Aggiungere qui la descrizione di un esame
32	36	ssn_5	1497578537	Aggiungere qui la descrizione di un esame
41	37	ssn_1	1548033993	Aggiungere qui la descrizione di un esame
134	38	ssn_3	1463613012	Aggiungere qui la descrizione di un esame
131	39	ssn_2	1516709859	Aggiungere qui la descrizione di un esame
53	40	ssn_5	1543526348	Aggiungere qui la descrizione di un esame
112	41	ssn_5	1518816309	Aggiungere qui la descrizione di un esame
6	42	ssn_1	1499378407	Aggiungere qui la descrizione di un esame
59	43	ssn_5	1514349628	Aggiungere qui la descrizione di un esame
1	44	ssn_5	1454661794	Aggiungere qui la descrizione di un esame
3	45	ssn_3	1440660915	Aggiungere qui la descrizione di un esame
132	46	ssn_3	1492906653	Aggiungere qui la descrizione di un esame
37	47	ssn_3	1474841111	Aggiungere qui la descrizione di un esame
145	48	ssn_4	1565437479	Aggiungere qui la descrizione di un esame
15	49	ssn_5	1550176929	Aggiungere qui la descrizione di un esame
79	50	ssn_5	1426171452	Aggiungere qui la descrizione di un esame
\.


--
-- Data for Name: erogazione_farmaci; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.erogazione_farmaci (vid, fid, date, quantity) FROM stdin;
94	629	1579252255	5
37	926	1566875591	3
32	535	1318440343	5
11	540	1468627389	1
34	494	1539023527	2
24	565	1412322635	4
71	674	1158808132	2
68	230	1131537145	1
20	141	1359072516	3
3	656	1514716879	1
64	427	1390393211	4
95	239	1482851404	2
5	791	1311825187	1
77	723	1272199355	1
30	927	1579523302	2
41	198	1408728756	4
25	25	1307175353	4
11	611	1556803587	2
50	346	1579740438	2
29	253	1472087993	3
28	711	1357282553	4
62	295	1427627305	4
26	42	1536593769	1
39	607	1577064499	5
14	399	1508107735	1
75	58	1555188136	3
75	631	1393170563	1
28	690	1292963339	1
25	629	1372388218	2
2	831	1438860507	2
86	469	1482807326	2
89	492	1040606058	1
74	547	1232356400	5
73	52	1575161890	5
58	260	1545411765	3
42	613	1109088197	4
86	522	1492516338	1
3	846	1541261684	4
16	570	1345911523	1
86	432	1450700832	3
95	254	1445105253	4
75	808	1470295270	2
99	697	1483249441	5
77	759	1227906579	1
46	289	1189470137	2
89	796	1358986242	3
36	114	1559384699	1
27	804	1343148041	5
37	934	1578357392	5
8	394	1565966649	1
36	70	1575957779	2
30	13	1547632987	3
99	221	1444033603	4
37	206	1524363549	5
88	667	1200877134	2
74	766	1203839540	1
83	870	1228079408	1
45	233	1113742474	4
42	761	1467834156	2
83	628	1176172756	3
61	570	1468639831	4
13	482	1557946886	3
5	221	1273486412	1
46	735	1056753999	3
79	156	1488279703	3
60	56	1326963880	1
88	371	1337252853	1
26	239	1562811581	3
69	106	1362410545	1
96	801	1511246619	1
52	114	1406890035	2
48	16	1279019409	2
89	524	1242615321	5
98	180	1383632227	3
91	882	1158919843	5
16	893	1522487679	4
71	952	1501766458	2
62	898	1496940000	3
40	126	1434863728	2
98	132	1180281342	5
99	720	1531062779	4
43	126	1481530641	5
98	851	1514288837	2
54	463	1440955549	4
93	645	1465864765	5
84	422	1488421764	3
79	153	1500617153	5
92	393	1224305933	5
52	406	1450171840	2
46	425	1259578563	4
49	351	1451031297	5
78	895	1309997002	1
26	433	1554755885	1
58	760	1552858860	5
2	327	1261042107	3
7	515	1106408741	4
99	771	1465388951	2
85	474	1571242787	2
57	560	1575269752	3
84	277	1528679498	2
92	163	978382482	2
22	75	1440621874	3
79	572	1475876073	3
76	477	1480294673	2
7	897	1367746154	5
32	634	1227081663	5
97	433	1195971864	3
32	409	1452906073	5
6	745	1579697519	2
98	835	1327442623	3
55	743	1498982060	5
7	286	1321335884	3
3	137	1563036007	5
51	68	1290676967	1
32	765	1485612747	4
88	781	1565041090	4
99	916	1533511118	3
29	664	1568476160	1
53	893	1562398958	4
76	931	1454940745	1
4	635	1465212891	3
97	31	1324093849	4
12	77	1021194843	2
81	805	1015529613	3
14	588	1269041423	1
79	175	1528501335	4
23	617	1555531843	5
27	219	1528920913	2
40	355	1369632556	3
84	291	1394393598	3
12	474	1097488048	2
32	771	1242339767	4
42	99	1048487778	5
69	607	1550624060	4
73	688	1380640055	5
7	586	1388385757	4
81	660	1085734801	4
14	258	1309923775	3
17	827	1533233245	1
14	385	1316124170	5
59	328	1004257449	5
28	604	1576921198	5
85	204	1560490701	3
8	154	1389515777	5
16	254	1270899698	1
90	617	1313298846	3
44	803	1457850359	3
38	315	1439977383	1
31	450	1403819832	3
42	699	1400266217	4
52	175	1575458149	1
92	86	1547228634	4
24	424	1307374105	5
90	677	1373746398	3
42	698	1446705019	1
68	781	1407290307	3
76	221	1413622532	3
78	893	1408436248	1
10	715	1437964999	1
78	873	1366440780	1
36	60	1576405591	1
38	64	1381820282	1
10	777	1461550855	1
28	430	1507401854	4
68	172	1473061786	5
46	47	1229218654	3
97	267	1197807891	1
41	310	1542670788	4
90	866	1291448801	5
49	856	1241657862	2
95	529	1462785141	1
42	535	1508617217	3
36	118	1555386457	1
76	673	1520576933	1
12	215	1146526080	3
88	925	1168750217	2
19	235	1301621231	5
68	961	1157295332	5
2	523	1372430725	5
55	680	1575985756	3
23	777	1554689013	4
14	248	1514125752	5
58	746	1528823114	5
35	967	1239461195	3
87	160	1537641781	4
39	141	1566280585	3
62	339	1383388835	4
36	132	1578763407	2
23	907	1537027368	5
94	480	1577384050	5
92	552	1363507150	3
42	323	1074471761	5
22	33	1497688787	4
89	243	996315528	5
49	787	1488883524	4
22	123	1389750720	5
40	146	1495020822	5
79	440	1505775811	1
51	438	1120675503	2
22	929	1484391487	1
\.


--
-- Data for Name: esami; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.esami (eid, ename) FROM stdin;
1	sangue
2	muscolo cardiaco
3	radiografia
\.


--
-- Data for Name: farmaci; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.farmaci (fid, fnome) FROM stdin;
1	Abilify
2	Acetaminophen
3	Acetaminophen and Hydrocodone
4	Actos
5	Acyclovir
6	Adderall
7	Adderall XR
8	Advair Diskus
9	Advil
10	Albuterol
11	Aldactone
12	Alendronate
13	Aleve
14	Allegra
15	Allopurinol
16	Alprazolam
17	Ambien
18	Amiodarone
19	Amitiza
20	Amitriptyline
21	Amlodipine
22	Amlodipine Besylate
23	Amoxicillin
24	Amoxicillin and Clavulanate
25	Anastrozole
26	Apixaban
27	Aricept
28	Aripiprazole
29	Ascorbic acid
30	Aspirin
31	Atarax
32	Atenolol
33	Ativan
34	Atorvastatin
35	Atropine
36	Augmentin
37	Avapro
38	Azathioprine
39	Azelastine
40	Azithromycin
41	Bacitracin
42	Baclofen
43	Bactrim
44	Bactrim DS
45	Bactroban
46	Basaglar
47	Belbuca
48	Belsomra
49	Benadryl
50	Benazepril
51	Benicar
52	Bentyl
53	Benzonatate
54	Benztropine
55	Betamethasone
56	Bethanechol
57	Bevacizumab
58	Biaxin
59	Bicalutamide
60	Biktarvy
61	Biotin
62	Bisacodyl
63	Bismuth subsalicylate
64	Bisoprolol
65	Boniva
66	Botox
67	Breo Ellipta
68	Brilinta
69	Brimonidine
70	Budesonide
71	Budesonide and Formoterol
72	Bumetanide
73	Bumex
74	Buprenorphine
75	Buprenorphine and Naloxone
76	Bupropion
77	Buspar
78	Buspirone
79	Bydureon
80	Bystolic
81	Calcitriol
82	Calcium carbonate
83	Captopril
84	Carafate
85	Carbamazepine
86	Carbidopa and levodopa
87	Cardizem
88	Carvedilol
89	Cefdinir
90	Ceftriaxone
91	Cefuroxime
92	Celebrex
93	Celecoxib
94	Celexa
95	Cephalexin
96	Cetirizine
97	Chlorthalidone
98	Cholecalciferol
99	Cialis
100	Cilostazol
101	Cipro
102	Ciprofloxacin
103	Citalopram
104	Claritin
105	Clindamycin
106	Clonazepam
107	Clonidine
108	Clopidogrel
109	Clotrimazole
110	Codeine
111	Colace
112	Colchicine
113	Concerta
114	Coreg
115	Coumadin
116	Cozaar
117	Crestor
118	Cyanocobalamin
119	Cyclobenzaprine
120	Cymbalta
121	Debrox
122	Decadron
123	Demerol
124	Denosumab
125	Depakote
126	Depo-Provera
127	Desloratadine
128	Desvenlafaxine
129	Dexamethasone
130	Dexilant
131	Dextroamphetamine
132	Dextromethorphan
133	Diazepam
134	Diclofenac
135	Diclofenac Sodium
136	Dicyclomine
137	Diflucan
138	Digoxin
139	Dilantin
140	Dilaudid
141	Diltiazem
142	Diovan
143	Diphenhydramine
144	Ditropan
145	Divalproex
146	Divalproex sodium
147	Docusate
148	Docusate Sodium
149	Domperidone
150	Donepezil
151	Doxazosin
152	Doxepin
153	Doxycycline
154	Dulcolax
155	Dulera
156	Duloxetine
157	DuoNeb
158	Dupixent
159	Dutasteride
160	Dyazide
161	Echinacea
162	Ecotrin
163	Effexor
164	Effexor XR
165	Effient
166	Elavil
167	Elidel
168	Eligard
169	Eliquis
170	Elmiron
171	Emgality
172	Empagliflozin
173	Enalapril
174	Enbrel
175	Enoxaparin
176	Entresto
177	Entyvio
178	Epclusa
179	Ephedrine
180	Epidiolex
181	Epinephrine
182	EpiPen
183	Eplerenone
184	Ergocalciferol
185	Ertapenem
186	Erythromycin
187	Escitalopram
188	Esomeprazole
189	Estrace
190	Estradiol
191	Eszopiclone
192	Etodolac
193	Eucrisa
194	Evista
195	Excedrin
196	Exelon
197	Exemestane
198	Exforge
199	Eylea
200	Ezetimibe
201	Famotidine
202	Farxiga
203	Febuxostat
204	Felodipine
205	Femara
206	Fenofibrate
207	Fentanyl
208	Ferrous gluconate
209	Ferrous Sulfate
210	Fexofenadine
211	Finasteride
212	Fioricet
213	Fish Oil
214	Flagyl
215	Flecainide
216	Flexeril
217	Flomax
218	Flonase
219	Florastor
220	Flovent
221	Flovent HFA
222	Fluconazole
223	Fludrocortisone
224	Fluocinonide
225	Fluorouracil
226	Fluoxetine
227	Fluticasone
228	Fluticasone nasal
229	Fluticasone Propionate
230	Fluticasone and salmeterol
231	Fluvoxamine
232	Fluzone
233	Focalin
234	Focalin XR
235	Folic Acid
236	Forteo
237	Fosamax
238	Fosfomycin
239	Fosinopril
240	Furosemide
241	Gabapentin
242	Galantamine
243	Gardasil
244	Garlic
245	Gaviscon
246	Gemcitabine
247	Gemfibrozil
248	Gemzar
249	Genotropin
250	Gentamicin
251	Gentamicin ophthalmic
252	Genvoya
253	Geodon
254	Gilenya
255	Ginger
256	Ginkgo
257	Ginkgo biloba
258	Ginseng
259	Gleevec
260	Glimepiride
261	Glipizide
262	Glipizide and metformin
263	Glucagon
264	Glucophage
265	Glucosamine
266	Glucosamine & Chondroitin with MSM
267	Glucose
268	Glucotrol
269	Glutathione
270	Glyburide
271	Glycerin
272	GlycoLax
273	Glycopyrrolate
274	Glyxambi
275	GoLYTELY
276	Gralise
277	Granisetron
278	Griseofulvin
279	Guaifenesin
280	Guanfacine
281	Halcion
282	Haldol
283	Halobetasol topical
284	Haloperidol
285	Harvoni
286	Hemabate
287	Heparin
288	Heparin Sodium
289	Herceptin
290	Hiprex
291	Hizentra
292	Horizant
293	Humalog
294	Humira
295	Humulin 70/30
296	Humulin N
297	Humulin R
298	Hyaluronic Acid
299	Hycodan
300	Hydralazine
301	Hydrea
302	Hydrochlorothiazide
303	Hydrochlorothiazide and lisinopril
304	Hydrochlorothiazide and losartan
305	Hydrochlorothiazide and triamterene
306	Hydrochlorothiazide and valsartan
307	Hydrocodone
308	Hydrocodone and acetaminophen
309	Hydrocortisone
310	Hydrocortisone topical
311	Hydromorphone
312	Hydroxychloroquine
313	Hydroxyurea
314	Hydroxyzine
315	Hydroxyzine Hydrochloride
316	Hydroxyzine Pamoate
317	Hyoscyamine
318	Hysingla ER
319	Hytrin
320	Hyzaar
321	Ibandronate
322	Ibrance
323	Ibrutinib
324	Ibu
325	Ibuprofen
326	Imatinib
327	Imbruvica
328	Imdur
329	Imipramine
330	Imitrex
331	Imodium
332	Imodium A-D
333	Imuran
334	Incruse Ellipta
335	Indapamide
336	Inderal
337	Indocin
338	Indomethacin
339	Infliximab
340	Ingrezza
341	Insulin
342	Insulin aspart
343	Insulin glargine
344	Insulin lispro
345	Insulin regular
346	Intuniv
347	Invega
348	Invega Sustenna
349	Invokana
350	Ipratropium
351	Ipratropium Bromide
352	Irbesartan
353	Isoniazid
354	Isosorbide
355	Isosorbide dinitrate
356	Isosorbide mononitrate
357	Isotretinoin
358	Itraconazole
359	Ivabradine
360	Ivermectin
361	Jadelle
362	Jadenu
363	Jakafi
364	Jalyn
365	Jantoven
366	Janumet
367	Janumet XR
368	Januvia
369	Jardiance
370	Jatenzo
371	Jencycla
372	Jentadueto
373	Jentadueto XR
374	Jetrea
375	Jeuveau
376	Jevtana
377	Jiaogulan
378	Jinteli
379	Jolessa
380	Jolivette
381	Jornay PM
382	Jublia
383	Juleber
384	Juluca
385	Junel
386	Junel 1/20
387	Junel Fe 1/20
388	Junel Fe 1.5/30
389	Juvederm
390	Juxtapid
391	Jynarque
392	Kadcyla
393	Kadian
394	Kaletra
395	Kaopectate
396	Kapspargo Sprinkle
397	Kapvay
398	Kariva
399	Kava
400	Kayexalate
401	Kcentra
402	K-Dur
403	Keflex
404	Kenalog
405	Kenalog-40
406	Keppra
407	Keppra XR
408	Ketalar
409	Ketamine
410	Ketoconazole
411	Ketoconazole Cream
412	Ketoconazole Shampoo
413	Ketoconazole topical
414	Ketoprofen
415	Ketorolac
416	Ketorolac ophthalmic
417	Ketorolac Tromethamine
418	Ketotifen
419	Ketotifen ophthalmic
420	Kevzara
421	Keytruda
422	Kisqali
423	Klonopin
424	Klor-Con
425	Kombiglyze XR
426	Kratom
427	Krill Oil
428	Krystexxa
429	K-Tab
430	Kyleena
431	Kyprolis
432	Labetalol
433	Lactulose
434	Lamictal
435	Lamotrigine
436	Lansoprazole
437	Lantus
438	Lasix
439	Latanoprost
440	Latuda
441	Leflunomide
442	Letrozole
443	Levaquin
444	Levemir
445	Levetiracetam
446	Levocetirizine
447	Levofloxacin
448	Levothyroxine
449	Lexapro
450	Lidocaine
451	Linezolid
452	Linzess
453	Lipitor
454	Lisinopril
455	Lithium
456	Livalo
457	Lomotil
458	Loperamide
459	Lopressor
460	Loratadine
461	Lorazepam
462	Lortab
463	Losartan
464	Lotrel
465	Lovastatin
466	Lovaza
467	Lovenox
468	Lumigan
469	Lunesta
470	Lupron
471	Lyrica
472	Macrobid
473	Magnesium
474	Magnesium citrate
475	Magnesium oxide
476	Meclizine
477	Medrol
478	Medrol Dosepak
479	Melatonin
480	Meloxicam
481	Memantine
482	Meropenem
483	Mesalamine
484	Metamucil
485	Metformin
486	Methadone
487	Methimazole
488	Methocarbamol
489	Methotrexate
490	Methylphenidate
491	Methylprednisolone
492	Metoclopramide
493	Metolazone
494	Metoprolol
495	Metoprolol Tartrate
496	Metronidazole
497	Midazolam
498	Midodrine
499	Minocycline
500	MiraLAX
501	Mirapex
502	Mirtazapine
503	Mobic
504	Modafinil
505	Montelukast
506	Morphine
507	Motrin
508	Mucinex
509	Mupirocin
510	Mupirocin topical
511	Myrbetriq
512	Nabumetone
513	Nadolol
514	Naloxone
515	Naltrexone
516	Namenda
517	Namzaric
518	Naprosyn
519	Naproxen
520	Naratriptan
521	Narcan
522	Nasacort
523	Nasonex
524	Nebivolol
525	Neomycin
526	Neosporin
527	Neulasta
528	Neurontin
529	Nexium
530	Nexplanon
531	Niacin
532	Niaspan
533	Nicotine
534	Nifedipine
535	Nitrofurantoin
536	Nitroglycerin
537	Nitrostat 
538	Nivolumab
539	Norco
540	Norepinephrine
541	Norethindrone
542	Norflex
543	Nortriptyline
544	Norvasc
545	NovoLog
546	Nucynta
547	Nuedexta
548	NuvaRing
549	Nuvigil
550	NyQuil
551	Nystatin
552	Ocrelizumab
553	Ocrevus
554	Octreotide
555	Ocuflox
556	Ocuvite
557	Ofev
558	Ofloxacin
559	Ofloxacin ophthalmic
560	Olanzapine
561	Olaparib
562	Olmesartan
563	Olopatadine ophthalmic
564	Omalizumab
565	Omega-3 polyunsaturated fatty acids
566	Omeprazole
567	Omnicef
568	Ondansetron
569	Onfi
570	Onglyza
571	Opana
572	Opdivo
573	Opium
574	Orencia
575	Orlistat
576	Orphenadrine
577	Ortho Tri-Cyclen
578	Oseltamivir
579	Otezla
580	Oxaliplatin
581	Oxazepam
582	Oxcarbazepine
583	Oxybutynin
584	Oxycodone
585	Oxycodone Hydrochloride
586	Oxycontin
587	Oxygen
588	Oxymetazoline nasal
589	Oxymorphone
590	Oxytocin
591	Ozempic
592	Pantoprazole
593	Paracetamol
594	Paroxetine
595	Paxil
596	Penicillin
597	Pepcid
598	Percocet
599	Phenazopyridine
600	Phenergan
601	Phenobarbital
602	Phentermine
603	Phenytoin
604	Pioglitazone
605	Plaquenil
606	Plavix
607	Polyethylene glycol 3350
608	Potassium Chloride
609	Pradaxa
610	Pramipexole
611	Pravastatin
612	Prazosin
613	Prednisolone
614	Prednisone
615	Pregabalin
616	Premarin
617	Prevacid
618	Prilosec
619	Primidone
620	Pristiq
621	ProAir HFA
622	Prochlorperazine
623	Prolia
624	Promethazine
625	Propofol
626	Propranolol
627	Proscar
628	Protonix
629	Prozac
630	Pseudoephedrine
631	Pyridium
632	Qbrelis
633	Qbrexza
634	QNASL
635	Q-Pap
636	Qsymia
637	Qternmet XR
638	Qualaquin
639	Quasense
640	Quazepam
641	Qudexy XR
642	Quercetin
643	Questran
644	Questran Light
645	Quetiapine
646	Quetiapine Fumarate
647	QuilliChew ER
648	Quillivant XR
649	Quinapril
650	Quinapril Hydrochloride
651	Quinidine
652	Quinidine Sulfate
653	Quinine
654	Quinine Sulfate
655	Quixin
656	Qutenza
657	Qvar
658	Qvar Redihaler
659	Rabeprazole
660	Raloxifene
661	Ramipril
662	Ranexa
663	Ranitidine
664	Ranolazine
665	Rapaflo
666	Reclast
667	Reglan
668	Relafen
669	Relpax
670	Remeron
671	Remicade
672	Renvela
673	Repaglinide
674	Repatha
675	Requip
676	Restasis
677	Restoril
678	Revatio
679	Revlimid
680	Rexulti
681	Rifampin
682	Rifaximin
683	Risedronate
684	Risperdal
685	Risperidone
686	Ritalin
687	Rituxan
688	Rituximab
689	Rivaroxaban
690	Rivastigmine
691	Rizatriptan
692	Robaxin
693	Robitussin
694	Rocephin
695	Ropinirole
696	Rosuvastatin
697	Roxicodone
698	Rozerem
699	Salbutamol
700	Santyl
701	Saw Palmetto
702	Saxenda
703	Scopolamine
704	Senna
705	Senokot
706	Sensipar
707	Septra
708	Seroquel
709	Sertraline
710	Sevelamer
711	Shingrix
712	Sildenafil
713	Simethicone
714	Simvastatin
715	Sinemet
716	Singulair
717	Sitagliptin
718	Skelaxin
719	Sodium bicarbonate
720	Sodium chloride
721	Solifenacin
722	Solu-Medrol
723	Soma
724	Sotalol
725	Spiriva
726	Spironolactone
727	Stelara
728	Strattera
729	Suboxone
730	Subutex
731	Sucralfate
732	Sudafed
733	Sulfamethoxazole and trimethoprim
734	Sulfasalazine
735	Sumatriptan
736	Symbicort
737	Synthroid
738	Systane
739	Tacrolimus
740	Tadalafil
741	Tamiflu
742	Tamoxifen
743	Tamsulosin
744	Tegretol
745	Telmisartan
746	Temazepam
747	Terazosin
748	Terbinafine
749	Tessalon
750	Tessalon Perles
751	Testosterone
752	Tetracycline
753	Theophylline
754	Thiamine
755	Timolol
756	Tiotropium
757	Tizanidine
758	Tolterodine
759	Topamax
760	Topiramate
761	Toprol XL
762	Toradol
763	Torsemide
764	Tradjenta
765	Tramadol
766	Trazodone
767	Trelegy Ellipta
768	Tresiba
769	Triamcinolone
770	Triamterene
771	Tricor
772	Trileptal
773	Trimethoprim
774	Trintellix
775	Trulicity
776	Truvada
777	Tylenol
778	Tylenol with Codeine #3
779	Ubiquinone
780	Uceris
781	Udenyca
782	Ulesfia
783	Ulipristal
784	Uloric
785	Ultomiris
786	Ultracet
787	Ultram
788	Ultram ER
789	Ultravate
790	Ultravate X Cream
791	Ultresa
792	Umeclidinium
793	Umeclidinium and vilanterol
794	Unasyn
795	Unisom
796	Unisom SleepMelts
797	Unithroid
798	Univasc
799	Upadacitinib
800	Uptravi
801	Ureacin-10
802	Urea Cream
803	Urecholine
804	Uribel
805	Urispas
806	Uristat
807	Urocit-K
808	Urogesic-Blue
809	Urokinase
810	Uro-MP
811	Uroxatral
812	Urso
813	Ursodiol
814	Ustekinumab
815	Ustell
816	Utibron Neohaler
817	Utopic
818	Uva Ursi
819	Valacyclovir
820	Valium
821	Valproate Sodium
822	Valproic acid
823	Valsartan
824	Valtrex
825	Vancomycin
826	Varenicline
827	Vascepa
828	Vasopressin
829	Vasotec
830	Veltassa
831	Venlafaxine
832	Venofer
833	Ventolin
834	Ventolin HFA
835	Verapamil
836	Versed
837	VESIcare
838	Viagra
839	Vibramycin
840	Vicodin
841	Victoza
842	Vigamox
843	Viibryd
844	Vimpat
845	Vistaril
846	Vitamin B 12
847	Vitamin C
848	Vitamin D
849	Vitamin D2
850	Vitamin D3
851	Vitamin E
852	Vivitrol
853	Voltaren
854	Voltaren Gel
855	Vortioxetine
856	Vraylar
857	Vytorin
858	Vyvanse
859	Wakix
860	Wal-itin
861	Wal-Zyr D
862	Warfarin
863	Warfarin Sodium
864	Wart Remover
865	Welchol
866	Wellbutrin
867	Wellbutrin SR
868	Wellbutrin XL
869	Wellcovorin
870	Westcort
871	Willow Bark
872	Witch Hazel
873	Witch hazel topical
874	Wixela Inhub
875	Wormwood
876	WP Thyroid
877	Xadago
878	Xalatan
879	Xalkori
880	Xanax
881	Xanax XR
882	Xarelto
883	Xeljanz
884	Xeljanz XR
885	Xeloda
886	Xenazine
887	Xenical
888	Xenleta
889	Xeomin
890	Xermelo
891	Xgeva
892	Xhance
893	Xiaflex
894	Xifaxan
895	Xigduo XR
896	Xiidra
897	Xofigo
898	Xofluza
899	Xolair
900	Xopenex
901	Xopenex HFA
902	Xospata
903	Xpovio
904	Xrylix
905	Xtampza ER
906	Xtandi
907	Xulane
908	Xultophy
909	Xylitol
910	Xylocaine
911	Xylocaine Jelly
912	Xylometazoline
913	Xylometazoline nasal
914	Xyosted
915	Xyrem
916	Xyzal
917	Yarrow
918	Yasmin
919	Yaz
920	Yervoy
921	Yescarta
922	Yohimbe
923	Yohimbine
924	Yondelis
925	Yonsa
926	Yosprala
927	Yupelri
928	Yuvafem
929	Zaditor
930	Zaleplon
931	Zanaflex
932	Zantac
933	Zaroxolyn
934	Zarxio
935	Zebeta
936	Zemplar
937	Zenpep
938	Zestoretic
939	Zestril
940	Zetia
941	Ziac
942	Zidovudine
943	Zinc
944	Ziprasidone
945	Zithromax
946	Zocor
947	Zofran
948	Zofran ODT
949	Zoladex
950	Zoledronic acid
951	Zolmitriptan
952	Zoloft
953	Zolpidem
954	Zolpidem Tartrate
955	Zometa
956	Zomig
957	Zonegran
958	Zonisamide
959	Zostavax
960	Zosyn
961	Zovirax
962	Zubsolv
963	Zyban
964	Zyloprim
965	Zyprexa
966	Zyrtec
967	Zytiga
968	Zyvox
\.


--
-- Data for Name: has_base; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.has_base (pid, mid) FROM stdin;
paz_1	med_10
paz_2	med_18
paz_3	med_7
paz_4	med_2
paz_5	med_2
paz_6	med_24
paz_7	med_3
paz_8	med_15
paz_9	med_9
paz_10	med_19
paz_11	med_6
paz_12	med_1
paz_13	med_7
paz_14	med_2
paz_15	med_6
paz_16	med_25
paz_17	med_21
paz_18	med_3
paz_19	med_11
paz_20	med_1
paz_21	med_1
paz_22	med_3
paz_23	med_19
paz_24	med_9
paz_25	med_10
paz_26	med_19
paz_27	med_23
paz_28	med_5
paz_29	med_14
paz_30	med_12
paz_31	med_4
paz_32	med_23
paz_33	med_5
paz_34	med_16
paz_35	med_10
paz_36	med_7
paz_37	med_25
paz_38	med_16
paz_39	med_7
paz_40	med_22
paz_41	med_19
paz_42	med_13
paz_43	med_18
paz_44	med_9
paz_45	med_6
paz_46	med_9
paz_47	med_4
paz_48	med_25
paz_49	med_22
paz_50	med_9
paz_51	med_2
paz_52	med_10
paz_53	med_3
paz_54	med_11
paz_55	med_18
paz_56	med_17
paz_57	med_16
paz_58	med_10
paz_59	med_24
paz_60	med_20
paz_61	med_18
paz_62	med_23
paz_63	med_20
paz_64	med_3
paz_65	med_17
paz_66	med_24
paz_67	med_5
paz_68	med_19
paz_69	med_9
paz_70	med_10
paz_71	med_10
paz_72	med_12
paz_73	med_12
paz_74	med_16
paz_75	med_18
paz_76	med_11
paz_77	med_8
paz_78	med_12
paz_79	med_21
paz_80	med_6
paz_81	med_24
paz_82	med_7
paz_83	med_19
paz_84	med_15
paz_85	med_13
paz_86	med_1
paz_87	med_23
paz_88	med_22
paz_89	med_15
paz_90	med_9
paz_91	med_18
paz_92	med_1
paz_93	med_7
paz_94	med_10
paz_95	med_25
paz_96	med_9
paz_97	med_7
paz_98	med_13
paz_99	med_17
paz_100	med_1
\.


--
-- Data for Name: medici; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.medici (mid, mnome, mcognome, msesso, mcodice_fiscale, mprovincia_operato) FROM stdin;
med_1	Gaudino	Ogliaro	m	GLRGDN73M24L378X	Trento
med_2	Osvaldo	Pozzana	m	PZZSLD79C18L378P	Trento
med_3	Clodomiro	Ercoli	m	RCLCDM94L06L378T	Trento
med_4	Benigno	Annibalini	m	NNBBGN49P05L378K	Trento
med_5	Caino	Gerardi	m	GRRCNA88H08L378N	Trento
med_6	Gustavo	Zollino	m	ZLLGTV98T02L378P	Trento
med_7	Eleuterio	Rizzini	m	RZZLTR93H14L378L	Trento
med_8	Isidoro	Glorioso	m	GLRSDR99C25L378O	Trento
med_9	Amina	Paolillo	f	PLLMNA74R65L378M	Trento
med_10	Salomone	Anella	m	NLLSMN31T13L378C	Trento
med_11	Degna	Gardo	f	GRDDGN00M45L378U	Trento
med_12	Berenice	Anzalone	f	NZLBNC52R46L378C	Trento
med_13	Veneranda	Bugane'	f	BGNVRN72S54L378B	Trento
med_14	Donata	Martimucci	f	MRTDNT67R47L378E	Trento
med_15	Tizio	Grancara	m	GRNTZI93L29L378P	Trento
med_16	Paterniano	Le grottaglie	m	LGRPRN62A09L378H	Trento
med_17	Viviano	Buonavia	m	BNVVVN69H07L378N	Trento
med_18	Floriana	Vernola	f	VRNFRN30M59L378M	Trento
med_19	Priscilla	Occhetto	f	CCHPSC81S62L378K	Trento
med_20	Evangelina	Taiani	f	TNAVGL98P46L378O	Trento
med_21	Menardo	Giuseppina	m	GSPMRD53E06L378N	Trento
med_22	Paciano	Rinalduzzi	m	RNLPCN63C04L378E	Trento
med_23	Fatima	Presenti	f	PRSFTM93R65L378C	Trento
med_24	Eliodoro	Bajetti	m	BJTLDR55C19L378R	Trento
med_25	Eleuterio	Infuli	m	NFLLTR95A08L378F	Trento
\.


--
-- Data for Name: pazienti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pazienti (pid, pnome, pcognome, psesso, pcodice_fiscale, pluogo_nascita, pdata_nascita, pprovincia_residenza) FROM stdin;
paz_1	Vittorio	Romero	m	RMRVTR46L21L378P	trento	-740018197	Trento
paz_2	Alba	Ghidoni	f	GHDLBA87T68L378T	vicenza	567711641	Trento
paz_3	Antigone	Palmioli	f	PLMNGN95A55L378G	torino	790125110	Trento
paz_4	Ondina	Ezza	f	ZZENDN78E43L378X	torino	263040186	Trento
paz_5	Berenice	Anzalone	f	NZLBNC52R46L378C	milano	-543920407	Trento
paz_6	Giacobbe	Ardizzi	m	RDZGBB72P06L378S	verona	84635876	Trento
paz_7	Delizia	Maranza	f	MRNDLZ86C50L378L	milano	510822185	Trento
paz_8	Publio	Lavagi	m	LVGPBL36S21L378H	firenze	-1044893309	Trento
paz_9	Eleuterio	Rizzini	m	RZZLTR93H14L378L	vicenza	740037480	Trento
paz_10	Libero	Alliana	m	LLNLBR88S27L378J	bolzano	596637031	Trento
paz_11	Tarcisio	Alberani	m	LBRTCS68T30L378K	padova	-31696837	Trento
paz_12	Giosuele	Ignelzi	m	GNLGSL54A21L378V	bolzano	-503163770	Trento
paz_13	Vala	Morosinotto	f	MRSVLA37E46L378C	milano	-1030506228	Trento
paz_14	Isabella	Barcello	f	BRCSLL99C57L378A	torino	921692410	Trento
paz_15	Cointa	Gambacci	f	GMBCNT41P44L378I	torino	-893883013	Trento
paz_16	Paciano	Rinalduzzi	m	RNLPCN63C04L378E	padova	-215549929	Trento
paz_17	Mariano	Macorich	m	MCRMRN60T15L378S	firenze	-285457261	Trento
paz_18	Gustavo	Zollino	m	ZLLGTV98T02L378P	vicenza	912572693	Trento
paz_19	Esa\\xc3\\xb9	Brambilla	m	BRMSCX71L11L378S	milano	48117364	Trento
paz_20	Elimena	Buonandi'	f	BNNLMN71L69L378M	vicenza	49595369	Trento
paz_21	Isidoro	Alimondo	m	LMNSDR75A03L378W	torino	158002413	Trento
paz_22	Cassiopea	Ros	f	RSOCSP97C42L378W	brescia	857266474	Trento
paz_23	Veneranda	Bugane'	f	BGNVRN72S54L378B	negrar	90624747	Trento
paz_24	Rainelda	Barbagiovanni	f	BRBRLD30A70L378X	brescia	-1259738210	Trento
paz_25	Barsaba	Morasca	m	MRSBSB68H29L378M	bolzano	-47601082	Trento
paz_26	Benvenuto	Batista	m	BTSBVN73S28L378W	firenze	123312822	Trento
paz_27	Pardo	Vidon	m	VDNPRD85H24L378Z	brescia	488435956	Trento
paz_28	Callisto	Iozzolino	m	ZZLCLS40C30L378I	negrar	-939021575	Trento
paz_29	Gaudino	Ogliaro	m	GLRGDN73M24L378X	torino	115023670	Trento
paz_30	Bardomiano	Licciardo	m	LCCBDM99T19L378L	firenze	945618385	Trento
paz_31	Walter	Zanni	m	ZNNWTR68A02L378A	bolzano	-62991026	Trento
paz_32	Osvaldo	Pozzana	m	PZZSLD79C18L378P	trento	290645200	Trento
paz_33	Ermilo	Ruggi	m	RGGRML36B12L378I	milano	-1069292831	Trento
paz_34	Irene	Guadagno	f	GDGRNI37S43L378D	padova	-1014893064	Trento
paz_35	Germano	Militerni	m	MLTGMN93S15L378S	negrar	753401538	Trento
paz_36	Servidio	Brioni	m	BRNSVD65T24L378B	verona	-126913145	Trento
paz_37	Catullo	Ghelardoni	m	GHLCLL82C25L378Y	vicenza	385915096	Trento
paz_38	Teodora	Anfolsi	f	NFLTDR86A59L378M	milano	506476261	Trento
paz_39	Menardo	Giuseppina	m	GSPMRD53E06L378N	torino	-525640118	Trento
paz_40	Cronida	Battiston	f	BTTCND53A60L378W	trento	-534770901	Trento
paz_41	Lara	Manieli	f	MNLLRA45T45L378N	firenze	-759646841	Trento
paz_42	Tizio	Grancara	m	GRNTZI93L29L378P	milano	743909482	Trento
paz_43	Idea	Liuzzo	f	LZZDIE60A61L378O	negrar	-313860547	Trento
paz_44	Vespasiano	Medda	m	MDDVPS88B15L378Z	bolzano	571906559	Trento
paz_45	Arialdo	Michetti	m	MCHRLD51A18L378K	bolzano	-598107020	Trento
paz_46	Susanna	Redavid	f	RDVSNN93C70L378X	milano	733475984	Trento
paz_47	Donata	Martimucci	f	MRTDNT67R47L378E	milano	-70508262	Trento
paz_48	Asia	Gulli'	f	GLLSAI84E61L378J	padova	453981133	Trento
paz_49	Priscilla	Occhetto	f	CCHPSC81S62L378K	trento	375285857	Trento
paz_50	Abelardo	Bazzigalupo	m	BZZBRD90C09L378V	milano	636965342	Trento
paz_51	Galatea	Paglione	f	PGLGLT44L57L378V	brescia	-803430544	Trento
paz_52	Tiziana	Maniago	f	MNGTZN84T47L378V	firenze	471292674	Trento
paz_53	Marina	Mastacchi	f	MSTMRN33E56L378K	padova	-1155892960	Trento
paz_54	Santina	Mignone	f	MGNSTN89L58L378H	bolzano	616744663	Trento
paz_55	Costante	Parolin	m	PRLCTN32A27L378W	torino	-1196983543	Trento
paz_56	Floriana	Vernola	f	VRNFRN30M59L378M	padova	-1242360307	Trento
paz_57	Milena	Quisini	f	QSNMLN40E61L378L	milano	-934513073	Trento
paz_58	Viviano	Buonavia	m	BNVVVN69H07L378N	verona	-17907056	Trento
paz_59	Paterniano	Le grottaglie	m	LGRPRN62A09L378H	vicenza	-251770166	Trento
paz_60	Sarbello	Botto	m	BTTSBL57E16L378V	torino	-398543923	Trento
paz_61	Salomone	Anella	m	NLLSMN31T13L378C	negrar	-1200841325	Trento
paz_62	Ileana	Pisone	f	PSNLNI82H48L378D	verona	392399874	Trento
paz_63	Almerigo	Ambrosiani	m	MBRLRG76A09L378L	trento	190059559	Trento
paz_64	Caterina	Uliano	f	LNUCRN79R42L378N	padova	307691883	Trento
paz_65	Caino	Gerardi	m	GRRCNA88H08L378N	negrar	581762960	Trento
paz_66	Gonzaga	Panarone	m	PNRGZG88C12L378L	torino	574166042	Trento
paz_67	Lara	Abbazia	f	BBZLRA93A52L378S	milano	726849375	Trento
paz_68	Godeberta	Lieggio	f	LGGGBR70C47L378F	torino	5628191	Trento
paz_69	Mercede	Pozzar	f	PZZMCD85P55L378E	vicenza	495646254	Trento
paz_70	Tesauro	Brunini	m	BRNTSR39R29L378U	verona	-952246807	Trento
paz_71	Gisella	Vadala'	f	VDLGLL33R67L378T	bolzano	-1141764270	Trento
paz_72	Gianluca	Gemelli	m	GMLGLC33P16L378M	vicenza	-1145262280	Trento
paz_73	Nino	Taibi	m	TBANNI67B18L378N	brescia	-90469573	Trento
paz_74	Evangelina	Taiani	f	TNAVGL98P46L378O	firenze	905088153	Trento
paz_75	Eleuterio	Infuli	m	NFLLTR95A08L378F	verona	789526193	Trento
paz_76	Verenzio	Punzo	m	PNZVNZ31L20L378I	milano	-1213434512	Trento
paz_77	Eliodoro	Bajetti	m	BJTLDR55C19L378R	brescia	-466727342	Trento
paz_78	Minervino	Pentangelo	m	PNTMRV85H13L378I	torino	487529560	Trento
paz_79	Decimo	Bodo	m	BDODCM52H11L378H	milano	-554030407	Trento
paz_80	Petronio	Vesta	m	VSTPRN48M06L378Q	bolzano	-675477207	Trento
paz_81	Dino	Paschini	m	PSCDNI37R04L378C	firenze	-1017492250	Trento
paz_82	Fatima	Presenti	f	PRSFTM93R65L378C	milano	751514720	Trento
paz_83	Quiteria	Zeoli	f	ZLEQTR44R70L378H	vicenza	-794289723	Trento
paz_84	Isidoro	Glorioso	m	GLRSDR99C25L378O	padova	922359730	Trento
paz_85	Amina	Paolillo	f	PLLMNA74R65L378M	milano	151947304	Trento
paz_86	Gabriella	Giovannino	f	GVNGRL64P53L378D	torino	-167257435	Trento
paz_87	Endrigo	Magnoni	m	MGNNRG46T07L378F	negrar	-727951981	Trento
paz_88	Tizio	Brocchi	m	BRCTZI86P10L378Q	negrar	526744732	Trento
paz_89	Priscilla	Messa	f	MSSPSC53S64L378I	firenze	-508130279	Trento
paz_90	Benigno	Annibalini	m	NNBBGN49P05L378K	vicenza	-641326301	Trento
paz_91	Danilo	Prampolini	m	PRMDNL33S13L378Q	trento	-1140229321	Trento
paz_92	Degna	Gardo	f	GRDDGN00M45L378U	firenze	965461229	Trento
paz_93	Lazzaro	Ridulfo	m	RDLLZR74R11L378K	vicenza	150746678	Trento
paz_94	Uriele	Piazzi	m	PZZRLU40M03L378O	bolzano	-928141424	Trento
paz_95	Tolomeo	Roggerini	m	RGGTLM76R04L378S	negrar	213256870	Trento
paz_96	Clodomiro	Ercoli	m	RCLCDM94L06L378T	torino	773515849	Trento
paz_97	Stefania	Rumbo	f	RMBSFN65S59L378K	brescia	-129936445	Trento
paz_98	Beronico	Mirano	m	MRNBNC70L12L378T	padova	16626274	Trento
paz_99	Taziana	Vezzi	f	VZZTZN94R47L378D	torino	781566859	Trento
paz_100	Aristofane	Berlingiere	m	BRLRTF31R06L378C	bolzano	-1206751639	Trento
\.


--
-- Data for Name: prescrizione_esami; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.prescrizione_esami (prescid, vid, eid) FROM stdin;
1	83	1
2	54	2
3	3	1
4	15	3
5	43	3
6	68	1
7	31	2
8	92	3
9	14	1
10	39	3
11	28	1
12	39	1
13	99	1
14	95	2
15	62	3
16	29	2
17	38	2
18	68	2
19	4	1
20	17	3
21	74	3
22	96	1
23	35	1
24	7	2
25	34	2
26	83	2
27	91	3
28	12	2
29	53	3
30	89	3
31	12	3
32	57	2
33	93	3
34	50	1
35	70	2
36	24	2
37	61	3
38	86	3
39	11	1
40	71	3
41	2	2
42	23	1
43	13	1
44	27	1
45	47	3
46	66	2
47	56	1
48	2	1
49	6	3
50	37	1
51	33	2
52	46	1
53	94	1
54	6	1
55	2	3
56	56	2
57	69	3
58	51	1
59	87	3
60	18	3
61	14	3
62	71	1
63	22	3
64	9	3
65	10	3
66	79	1
67	91	1
68	5	2
69	22	1
70	4	3
71	64	3
72	41	1
73	71	2
74	57	3
75	1	2
76	5	1
77	31	1
78	25	3
79	46	3
80	94	3
81	73	3
82	42	3
83	15	1
84	33	3
85	83	3
86	44	2
87	7	1
88	13	2
89	35	2
90	13	3
91	97	2
92	75	3
93	3	2
94	80	1
95	65	3
96	12	1
97	35	3
98	52	2
99	96	3
100	42	2
101	38	1
102	29	1
103	52	1
104	84	3
105	56	3
106	46	2
107	25	2
108	77	2
109	10	2
110	45	1
111	66	1
112	44	3
113	66	3
114	64	2
115	76	3
116	44	1
117	10	1
118	63	1
119	75	1
120	61	2
121	86	2
122	26	1
123	28	2
124	48	2
125	99	3
126	89	2
127	48	3
128	6	2
129	81	3
130	81	1
131	43	1
132	90	1
133	82	1
134	85	3
135	65	1
136	97	3
137	47	1
138	36	3
139	23	2
140	80	3
141	49	1
142	78	3
143	19	2
144	26	2
145	88	1
146	16	3
147	54	3
\.


--
-- Data for Name: prescrizione_farmaci; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.prescrizione_farmaci (vid, fid, quantity) FROM stdin;
71	546	4
47	697	5
79	448	5
28	664	4
51	139	1
23	736	1
27	659	3
13	379	1
27	883	3
68	420	4
87	360	5
17	633	1
72	70	1
83	643	1
72	217	5
24	863	5
89	648	3
18	176	5
39	200	1
12	366	2
98	535	3
26	122	4
92	742	2
96	268	4
99	886	2
98	845	2
18	521	2
94	322	3
54	166	2
97	372	5
31	928	3
22	438	1
49	14	3
47	947	5
27	773	1
93	468	1
63	285	2
78	793	5
1	278	4
85	181	4
18	469	3
86	261	2
33	470	4
7	556	1
78	574	1
68	517	1
24	505	4
15	735	3
64	522	2
64	93	1
66	232	2
79	429	4
98	819	5
94	847	1
61	343	3
41	210	2
88	747	1
70	708	2
37	878	5
95	17	2
7	223	1
80	884	1
70	562	1
28	576	5
97	665	4
33	954	1
4	742	4
54	482	5
39	110	1
1	660	2
59	441	5
83	77	1
92	258	1
52	807	4
41	1	4
72	132	1
15	316	3
78	65	4
98	686	4
59	84	1
90	275	5
64	628	1
98	448	4
77	625	3
36	788	1
89	236	3
88	855	5
75	643	4
63	126	1
99	371	3
22	402	5
57	119	3
50	490	4
77	53	3
49	410	2
81	146	3
80	96	4
96	402	2
77	218	5
54	95	5
38	129	3
28	822	3
17	80	4
2	273	3
97	847	1
7	144	2
54	262	1
79	938	2
20	432	2
1	169	5
95	862	1
51	133	4
39	878	1
66	466	1
67	728	2
15	944	5
19	619	3
73	741	2
53	493	1
35	191	4
36	198	3
9	705	1
33	690	5
99	647	1
86	435	4
71	598	4
97	666	3
20	759	5
79	900	1
2	174	1
95	199	3
13	97	4
76	421	5
40	588	3
40	838	5
25	508	1
95	450	1
99	740	5
86	543	1
29	53	4
23	225	4
3	455	2
4	946	1
62	900	2
68	671	2
45	544	1
87	203	5
1	500	5
18	847	1
21	771	1
8	53	4
91	109	5
97	175	2
44	572	3
95	92	3
64	446	1
29	187	2
9	775	5
18	556	1
1	331	4
26	275	3
78	758	1
14	694	4
9	119	2
73	76	2
12	39	3
85	699	3
55	521	4
88	385	5
12	787	1
40	840	5
13	512	3
76	395	5
95	836	4
40	699	5
14	448	5
57	624	4
18	715	2
15	784	1
25	542	5
67	717	2
36	150	1
84	505	1
42	23	2
14	372	4
98	233	5
24	678	4
6	364	3
92	336	2
39	928	2
98	653	3
99	535	2
99	951	4
94	257	5
50	50	1
74	47	4
23	598	3
55	550	4
98	927	4
11	890	1
\.


--
-- Data for Name: ssn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ssn (sid, snome, sprovincia) FROM stdin;
ssn_1	ssn_Trento_1	Trento
ssn_2	ssn_Trento_2	Trento
ssn_3	ssn_Bolzano_3	Bolzano
ssn_4	ssn_Bolzano_4	Bolzano
ssn_5	ssn_Trento_5	Trento
\.


--
-- Data for Name: ticket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket (tid, tprice, tpaid) FROM stdin;
1	11	t
2	11	f
3	11	f
4	11	t
5	11	t
6	11	t
7	11	t
8	11	t
9	11	f
10	11	f
11	11	f
12	11	t
13	11	t
14	11	t
15	11	f
16	11	t
17	11	f
18	11	t
19	11	t
20	11	t
21	11	t
22	11	f
23	11	f
24	11	t
25	11	t
26	11	t
27	11	t
28	11	f
29	11	t
30	11	t
31	11	t
32	11	f
33	11	f
34	11	t
35	11	t
36	11	f
37	11	f
38	11	f
39	11	t
40	11	t
41	11	f
42	11	f
43	11	f
44	11	t
45	11	t
46	11	f
47	11	t
48	11	f
49	11	f
50	50	f
51	50	f
52	50	t
53	50	t
54	50	t
55	50	f
56	50	f
57	50	f
58	50	t
59	50	t
60	50	t
61	50	f
62	50	t
63	50	f
64	50	t
65	50	t
66	50	f
67	50	t
68	50	t
69	50	f
70	50	f
71	50	t
72	50	t
73	50	t
74	50	f
75	50	t
76	50	t
77	50	t
78	50	f
79	50	t
80	50	t
81	50	t
82	50	f
83	50	f
84	50	f
85	50	f
86	50	f
87	50	f
88	50	f
89	50	f
90	50	t
91	50	f
92	50	f
93	50	t
94	50	t
95	50	t
96	50	f
97	50	t
98	50	t
99	50	t
100	50	f
\.


--
-- Data for Name: utenti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.utenti (uid, ufoto, uemail, upassword) FROM stdin;
paz_1	./img/picture.1337	vittorio.romero386121@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_2	./img/picture.790	alba.ghidoni462069@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_3	./img/picture.626	antigone.palmioli873318@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_4	./img/picture.362	ondina.ezza418818@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_5	./img/picture.1340	berenice.anzalone843086@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_6	./img/picture.437	giacobbe.ardizzi290420@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_7	./img/picture.1633	delizia.maranza4705@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_8	./img/picture.1539	publio.lavagi689304@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_9	./img/picture.1115	eleuterio.rizzini853675@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_10	./img/picture.1198	libero.alliana859997@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_11	./img/picture.1323	tarcisio.alberani869411@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_12	./img/picture.1581	giosuele.ignelzi919673@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_13	./img/picture.658	vala.morosinotto375351@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_14	./img/picture.1251	isabella.barcello457273@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_15	./img/picture.1001	cointa.gambacci194295@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_16	./img/picture.1227	paciano.rinalduzzi969515@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_17	./img/picture.1261	mariano.macorich574758@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_18	./img/picture.170	gustavo.zollino819554@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_19	./img/picture.1137	esa\\xc3\\xb9.brambilla951473@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_20	./img/picture.1111	elimena.buonandi'233625@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_21	./img/picture.1009	isidoro.alimondo481619@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_22	./img/picture.627	cassiopea.ros824305@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_23	./img/picture.689	veneranda.bugane'192376@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_24	./img/picture.1129	rainelda.barbagiovanni616047@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_25	./img/picture.252	barsaba.morasca579686@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_26	./img/picture.327	benvenuto.batista187205@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_27	./img/picture.1507	pardo.vidon321111@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_28	./img/picture.1086	callisto.iozzolino136499@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_29	./img/picture.1031	gaudino.ogliaro894448@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_30	./img/picture.275	bardomiano.licciardo212730@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_31	./img/picture.999	walter.zanni790437@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_32	./img/picture.257	osvaldo.pozzana258748@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_33	./img/picture.1384	ermilo.ruggi318594@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_34	./img/picture.8	irene.guadagno125991@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_35	./img/picture.567	germano.militerni15781@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_36	./img/picture.1025	servidio.brioni806715@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_37	./img/picture.1528	catullo.ghelardoni285511@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_38	./img/picture.562	teodora.anfolsi453912@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_39	./img/picture.1335	menardo.giuseppina91445@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_40	./img/picture.259	cronida.battiston336511@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_41	./img/picture.1082	lara.manieli664602@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_42	./img/picture.655	tizio.grancara56327@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_43	./img/picture.550	idea.liuzzo318564@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_44	./img/picture.1480	vespasiano.medda280443@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_45	./img/picture.444	arialdo.michetti770534@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_46	./img/picture.1109	susanna.redavid706656@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_47	./img/picture.976	donata.martimucci15303@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_48	./img/picture.1410	asia.gulli'527263@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_49	./img/picture.1199	priscilla.occhetto246071@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_50	./img/picture.897	abelardo.bazzigalupo641226@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_51	./img/picture.140	galatea.paglione853479@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_52	./img/picture.1599	tiziana.maniago158640@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_53	./img/picture.1128	marina.mastacchi430548@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_54	./img/picture.1404	santina.mignone439281@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_55	./img/picture.549	costante.parolin600697@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_56	./img/picture.1482	floriana.vernola801163@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_57	./img/picture.1325	milena.quisini336199@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_58	./img/picture.910	viviano.buonavia543226@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_59	./img/picture.152	paterniano.le grottaglie717380@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_60	./img/picture.513	sarbello.botto769144@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_61	./img/picture.979	salomone.anella221902@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_62	./img/picture.861	ileana.pisone113567@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_63	./img/picture.1195	almerigo.ambrosiani246929@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_64	./img/picture.973	caterina.uliano216990@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_65	./img/picture.323	caino.gerardi590331@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_66	./img/picture.842	gonzaga.panarone437595@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_67	./img/picture.975	lara.abbazia394276@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_68	./img/picture.1186	godeberta.lieggio751216@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_69	./img/picture.449	mercede.pozzar624493@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_70	./img/picture.367	tesauro.brunini422341@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_71	./img/picture.597	gisella.vadala'14519@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_72	./img/picture.844	gianluca.gemelli831066@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_73	./img/picture.1288	nino.taibi308679@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_74	./img/picture.1018	evangelina.taiani133058@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_75	./img/picture.55	eleuterio.infuli531400@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_76	./img/picture.72	verenzio.punzo678054@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_77	./img/picture.938	eliodoro.bajetti32678@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_78	./img/picture.1126	minervino.pentangelo324537@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_79	./img/picture.925	decimo.bodo442494@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_80	./img/picture.43	petronio.vesta334814@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_81	./img/picture.204	dino.paschini733100@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_82	./img/picture.772	fatima.presenti103187@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_83	./img/picture.715	quiteria.zeoli747361@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_84	./img/picture.1123	isidoro.glorioso849105@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_85	./img/picture.641	amina.paolillo335230@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_86	./img/picture.972	gabriella.giovannino960101@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_87	./img/picture.727	endrigo.magnoni663606@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_88	./img/picture.1078	tizio.brocchi973898@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_89	./img/picture.579	priscilla.messa623906@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_90	./img/picture.1417	benigno.annibalini259453@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_91	./img/picture.1596	danilo.prampolini336002@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_92	./img/picture.750	degna.gardo366084@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_93	./img/picture.474	lazzaro.ridulfo697761@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_94	./img/picture.527	uriele.piazzi676957@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_95	./img/picture.722	tolomeo.roggerini569580@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_96	./img/picture.104	clodomiro.ercoli93007@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_97	./img/picture.1634	stefania.rumbo95187@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_98	./img/picture.808	beronico.mirano751427@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_99	./img/picture.578	taziana.vezzi282504@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
paz_100	./img/picture.954	aristofane.berlingiere223881@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_1	./img/picture.83	gaudino.ogliaro894448@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_2	./img/picture.792	osvaldo.pozzana258748@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_3	./img/picture.1391	clodomiro.ercoli93007@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_4	./img/picture.228	benigno.annibalini259453@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_5	./img/picture.406	caino.gerardi590331@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_6	./img/picture.794	gustavo.zollino819554@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_7	./img/picture.537	eleuterio.rizzini853675@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_8	./img/picture.886	isidoro.glorioso849105@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_9	./img/picture.1048	amina.paolillo335230@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_10	./img/picture.13	salomone.anella221902@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_11	./img/picture.65	degna.gardo366084@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_12	./img/picture.749	berenice.anzalone843086@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_13	./img/picture.696	veneranda.bugane'192376@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_14	./img/picture.1193	donata.martimucci15303@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_15	./img/picture.1503	tizio.grancara56327@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_16	./img/picture.1187	paterniano.le grottaglie717380@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_17	./img/picture.560	viviano.buonavia543226@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_18	./img/picture.909	floriana.vernola801163@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_19	./img/picture.1565	priscilla.occhetto246071@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_20	./img/picture.1444	evangelina.taiani133058@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_21	./img/picture.1604	menardo.giuseppina91445@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_22	./img/picture.832	paciano.rinalduzzi969515@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_23	./img/picture.1618	fatima.presenti103187@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_24	./img/picture.759	eliodoro.bajetti32678@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
med_25	./img/picture.862	eleuterio.infuli531400@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
ssn_1	./img/picture.392	ssn1@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
ssn_2	./img/picture.181	ssn2@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
ssn_3	./img/picture.1182	ssn3@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
ssn_4	./img/picture.1636	ssn4@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
ssn_5	./img/picture.870	ssn5@hospital.com	ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413
\.


--
-- Data for Name: visita_base; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.visita_base (vid, pid, mid, date, description) FROM stdin;
1	paz_2	med_18	1542670927	This is a normal description. In this filed the doctor writes the examination information
2	paz_7	med_3	1220306539	This is a normal description. In this filed the doctor writes the examination information
3	paz_45	med_6	1496858100	This is a normal description. In this filed the doctor writes the examination information
4	paz_9	med_9	1069641314	This is a normal description. In this filed the doctor writes the examination information
5	paz_13	med_7	1271773641	This is a normal description. In this filed the doctor writes the examination information
6	paz_58	med_10	1558950166	This is a normal description. In this filed the doctor writes the examination information
7	paz_55	med_18	1018938784	This is a normal description. In this filed the doctor writes the examination information
8	paz_18	med_3	1216481065	This is a normal description. In this filed the doctor writes the examination information
9	paz_57	med_16	1286390553	This is a normal description. In this filed the doctor writes the examination information
10	paz_36	med_7	1436733659	This is a normal description. In this filed the doctor writes the examination information
11	paz_54	med_11	1426969084	This is a normal description. In this filed the doctor writes the examination information
12	paz_56	med_17	969986610	This is a normal description. In this filed the doctor writes the examination information
13	paz_8	med_15	1158994974	This is a normal description. In this filed the doctor writes the examination information
14	paz_3	med_7	1247892890	This is a normal description. In this filed the doctor writes the examination information
15	paz_58	med_10	1487455526	This is a normal description. In this filed the doctor writes the examination information
16	paz_80	med_6	1114128752	This is a normal description. In this filed the doctor writes the examination information
17	paz_54	med_11	1438869719	This is a normal description. In this filed the doctor writes the examination information
18	paz_54	med_11	1057653825	This is a normal description. In this filed the doctor writes the examination information
19	paz_5	med_2	1221444829	This is a normal description. In this filed the doctor writes the examination information
20	paz_50	med_9	1079953239	This is a normal description. In this filed the doctor writes the examination information
21	paz_3	med_7	965819333	This is a normal description. In this filed the doctor writes the examination information
22	paz_76	med_11	1369690141	This is a normal description. In this filed the doctor writes the examination information
23	paz_13	med_7	1489035392	This is a normal description. In this filed the doctor writes the examination information
24	paz_76	med_11	1269846215	This is a normal description. In this filed the doctor writes the examination information
25	paz_81	med_24	1292965749	This is a normal description. In this filed the doctor writes the examination information
26	paz_75	med_18	1471832163	This is a normal description. In this filed the doctor writes the examination information
27	paz_85	med_13	1315645527	This is a normal description. In this filed the doctor writes the examination information
28	paz_98	med_13	1241471740	This is a normal description. In this filed the doctor writes the examination information
29	paz_79	med_21	1433210579	This is a normal description. In this filed the doctor writes the examination information
30	paz_18	med_3	1497659206	This is a normal description. In this filed the doctor writes the examination information
31	paz_52	med_10	1259521109	This is a normal description. In this filed the doctor writes the examination information
32	paz_91	med_18	1188239958	This is a normal description. In this filed the doctor writes the examination information
33	paz_60	med_20	1117411913	This is a normal description. In this filed the doctor writes the examination information
34	paz_16	med_25	1154940124	This is a normal description. In this filed the doctor writes the examination information
35	paz_53	med_3	1010335502	This is a normal description. In this filed the doctor writes the examination information
36	paz_71	med_10	1553891007	This is a normal description. In this filed the doctor writes the examination information
37	paz_66	med_24	1460596172	This is a normal description. In this filed the doctor writes the examination information
38	paz_61	med_18	1308806992	This is a normal description. In this filed the doctor writes the examination information
39	paz_2	med_18	1547177054	This is a normal description. In this filed the doctor writes the examination information
40	paz_40	med_22	1226057417	This is a normal description. In this filed the doctor writes the examination information
41	paz_35	med_10	1031055835	This is a normal description. In this filed the doctor writes the examination information
42	paz_83	med_19	1033479931	This is a normal description. In this filed the doctor writes the examination information
43	paz_63	med_20	1459254306	This is a normal description. In this filed the doctor writes the examination information
44	paz_45	med_6	1413266301	This is a normal description. In this filed the doctor writes the examination information
45	paz_100	med_1	1101764311	This is a normal description. In this filed the doctor writes the examination information
46	paz_82	med_7	985992991	This is a normal description. In this filed the doctor writes the examination information
47	paz_70	med_10	1238451892	This is a normal description. In this filed the doctor writes the examination information
48	paz_32	med_23	1081849073	This is a normal description. In this filed the doctor writes the examination information
49	paz_58	med_10	1232743168	This is a normal description. In this filed the doctor writes the examination information
50	paz_26	med_19	1534792489	This is a normal description. In this filed the doctor writes the examination information
51	paz_88	med_22	959211890	This is a normal description. In this filed the doctor writes the examination information
52	paz_51	med_2	1402820475	This is a normal description. In this filed the doctor writes the examination information
53	paz_30	med_12	1224694526	This is a normal description. In this filed the doctor writes the examination information
54	paz_38	med_16	1094161917	This is a normal description. In this filed the doctor writes the examination information
55	paz_14	med_2	1497526128	This is a normal description. In this filed the doctor writes the examination information
56	paz_69	med_9	1008896208	This is a normal description. In this filed the doctor writes the examination information
57	paz_97	med_7	1393663157	This is a normal description. In this filed the doctor writes the examination information
58	paz_62	med_23	1522748300	This is a normal description. In this filed the doctor writes the examination information
59	paz_34	med_16	965534753	This is a normal description. In this filed the doctor writes the examination information
60	paz_89	med_15	974708285	This is a normal description. In this filed the doctor writes the examination information
61	paz_95	med_25	1297367160	This is a normal description. In this filed the doctor writes the examination information
62	paz_27	med_23	1064955428	This is a normal description. In this filed the doctor writes the examination information
63	paz_42	med_13	1187319104	This is a normal description. In this filed the doctor writes the examination information
64	paz_79	med_21	1334449490	This is a normal description. In this filed the doctor writes the examination information
65	paz_27	med_23	1031279351	This is a normal description. In this filed the doctor writes the examination information
66	paz_75	med_18	988652209	This is a normal description. In this filed the doctor writes the examination information
67	paz_84	med_15	1027697792	This is a normal description. In this filed the doctor writes the examination information
68	paz_62	med_23	1097906615	This is a normal description. In this filed the doctor writes the examination information
69	paz_16	med_25	1307184391	This is a normal description. In this filed the doctor writes the examination information
70	paz_99	med_17	1345070978	This is a normal description. In this filed the doctor writes the examination information
71	paz_6	med_24	1002261319	This is a normal description. In this filed the doctor writes the examination information
72	paz_93	med_7	1051558611	This is a normal description. In this filed the doctor writes the examination information
73	paz_97	med_7	1050993555	This is a normal description. In this filed the doctor writes the examination information
74	paz_58	med_10	981087734	This is a normal description. In this filed the doctor writes the examination information
75	paz_74	med_16	1252798689	This is a normal description. In this filed the doctor writes the examination information
76	paz_29	med_14	1318638280	This is a normal description. In this filed the doctor writes the examination information
77	paz_15	med_6	1033996169	This is a normal description. In this filed the doctor writes the examination information
78	paz_34	med_16	1304758922	This is a normal description. In this filed the doctor writes the examination information
79	paz_50	med_9	1456072549	This is a normal description. In this filed the doctor writes the examination information
80	paz_49	med_22	1466767792	This is a normal description. In this filed the doctor writes the examination information
81	paz_54	med_11	960867629	This is a normal description. In this filed the doctor writes the examination information
82	paz_20	med_1	1132602961	This is a normal description. In this filed the doctor writes the examination information
83	paz_56	med_17	1006172831	This is a normal description. In this filed the doctor writes the examination information
84	paz_21	med_1	964927239	This is a normal description. In this filed the doctor writes the examination information
85	paz_70	med_10	1556456947	This is a normal description. In this filed the doctor writes the examination information
86	paz_97	med_7	1449690975	This is a normal description. In this filed the doctor writes the examination information
87	paz_61	med_18	1111079447	This is a normal description. In this filed the doctor writes the examination information
88	paz_37	med_25	1133894782	This is a normal description. In this filed the doctor writes the examination information
89	paz_74	med_16	951689602	This is a normal description. In this filed the doctor writes the examination information
90	paz_100	med_1	1287856370	This is a normal description. In this filed the doctor writes the examination information
91	paz_17	med_21	1016746362	This is a normal description. In this filed the doctor writes the examination information
92	paz_82	med_7	948073805	This is a normal description. In this filed the doctor writes the examination information
93	paz_72	med_12	1098792892	This is a normal description. In this filed the doctor writes the examination information
94	paz_24	med_9	1572775446	This is a normal description. In this filed the doctor writes the examination information
95	paz_26	med_19	1137387517	This is a normal description. In this filed the doctor writes the examination information
96	paz_61	med_18	1488835474	This is a normal description. In this filed the doctor writes the examination information
97	paz_78	med_12	1184414498	This is a normal description. In this filed the doctor writes the examination information
98	paz_65	med_17	1176145287	This is a normal description. In this filed the doctor writes the examination information
99	paz_37	med_25	1234315879	This is a normal description. In this filed the doctor writes the examination information
\.


--
-- Data for Name: visita_specialistica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.visita_specialistica (pid, mid, tid, date, description) FROM stdin;
paz_31	med_19	51	1109720542	Aggiungere qui la descrizione della visita specialistica
paz_53	med_19	52	1229431112	Aggiungere qui la descrizione della visita specialistica
paz_35	med_21	53	1194595946	Aggiungere qui la descrizione della visita specialistica
paz_97	med_4	54	1471338279	Aggiungere qui la descrizione della visita specialistica
paz_44	med_5	55	1058730143	Aggiungere qui la descrizione della visita specialistica
paz_91	med_10	56	1545189092	Aggiungere qui la descrizione della visita specialistica
paz_77	med_6	57	957919544	Aggiungere qui la descrizione della visita specialistica
paz_87	med_2	58	1487066888	Aggiungere qui la descrizione della visita specialistica
paz_9	med_8	59	1246320779	Aggiungere qui la descrizione della visita specialistica
paz_17	med_7	60	1275941210	Aggiungere qui la descrizione della visita specialistica
paz_31	med_16	61	1495330285	Aggiungere qui la descrizione della visita specialistica
paz_76	med_10	62	1282703320	Aggiungere qui la descrizione della visita specialistica
paz_13	med_14	63	1424915995	Aggiungere qui la descrizione della visita specialistica
paz_29	med_18	64	984419774	Aggiungere qui la descrizione della visita specialistica
paz_88	med_8	65	1373577191	Aggiungere qui la descrizione della visita specialistica
paz_55	med_16	66	1479244646	Aggiungere qui la descrizione della visita specialistica
paz_19	med_20	67	1035535610	Aggiungere qui la descrizione della visita specialistica
paz_50	med_2	68	1170042188	Aggiungere qui la descrizione della visita specialistica
paz_63	med_5	69	1165925551	Aggiungere qui la descrizione della visita specialistica
paz_59	med_18	70	1396000847	Aggiungere qui la descrizione della visita specialistica
paz_14	med_24	71	1085975234	Aggiungere qui la descrizione della visita specialistica
paz_24	med_14	72	1067470697	Aggiungere qui la descrizione della visita specialistica
paz_51	med_23	73	1408293594	Aggiungere qui la descrizione della visita specialistica
paz_77	med_14	74	1394806375	Aggiungere qui la descrizione della visita specialistica
paz_96	med_3	75	983404382	Aggiungere qui la descrizione della visita specialistica
paz_59	med_14	76	1098258362	Aggiungere qui la descrizione della visita specialistica
paz_45	med_25	77	1466330169	Aggiungere qui la descrizione della visita specialistica
paz_41	med_4	78	1284309360	Aggiungere qui la descrizione della visita specialistica
paz_68	med_22	79	1495046859	Aggiungere qui la descrizione della visita specialistica
paz_74	med_17	80	1346515309	Aggiungere qui la descrizione della visita specialistica
paz_72	med_24	81	1162218621	Aggiungere qui la descrizione della visita specialistica
paz_71	med_3	82	1060541740	Aggiungere qui la descrizione della visita specialistica
paz_74	med_3	83	1283546954	Aggiungere qui la descrizione della visita specialistica
paz_3	med_3	84	1336074043	Aggiungere qui la descrizione della visita specialistica
paz_62	med_8	85	1047925863	Aggiungere qui la descrizione della visita specialistica
paz_97	med_16	86	1206171615	Aggiungere qui la descrizione della visita specialistica
paz_70	med_1	87	1322867995	Aggiungere qui la descrizione della visita specialistica
paz_5	med_22	88	1561698459	Aggiungere qui la descrizione della visita specialistica
paz_7	med_21	89	1537727113	Aggiungere qui la descrizione della visita specialistica
paz_9	med_19	90	1476384254	Aggiungere qui la descrizione della visita specialistica
paz_46	med_13	91	1300516087	Aggiungere qui la descrizione della visita specialistica
paz_91	med_4	92	1077783834	Aggiungere qui la descrizione della visita specialistica
paz_65	med_2	93	1332799313	Aggiungere qui la descrizione della visita specialistica
paz_88	med_25	94	1514403500	Aggiungere qui la descrizione della visita specialistica
paz_85	med_25	95	1007858275	Aggiungere qui la descrizione della visita specialistica
paz_100	med_13	96	1483400660	Aggiungere qui la descrizione della visita specialistica
paz_59	med_22	97	1093550259	Aggiungere qui la descrizione della visita specialistica
paz_68	med_18	98	1017408257	Aggiungere qui la descrizione della visita specialistica
paz_65	med_6	99	1343052455	Aggiungere qui la descrizione della visita specialistica
paz_27	med_9	100	1403362269	Aggiungere qui la descrizione della visita specialistica
\.


--
-- Name: erogazione_esami erogazione_esami_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_esami
    ADD CONSTRAINT erogazione_esami_pkey PRIMARY KEY (prescid);


--
-- Name: erogazione_farmaci erogazione_farmaci_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_farmaci
    ADD CONSTRAINT erogazione_farmaci_pkey PRIMARY KEY (vid, fid);


--
-- Name: esami esami_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.esami
    ADD CONSTRAINT esami_pkey PRIMARY KEY (eid);


--
-- Name: farmaci farmaci_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.farmaci
    ADD CONSTRAINT farmaci_pkey PRIMARY KEY (fid);


--
-- Name: has_base has_base_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.has_base
    ADD CONSTRAINT has_base_pkey PRIMARY KEY (pid, mid);


--
-- Name: medici medici_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medici
    ADD CONSTRAINT medici_pkey PRIMARY KEY (mid);


--
-- Name: pazienti pazienti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pazienti
    ADD CONSTRAINT pazienti_pkey PRIMARY KEY (pid);


--
-- Name: prescrizione_esami prescrizione_esami_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_pkey PRIMARY KEY (prescid);


--
-- Name: prescrizione_farmaci prescrizione_farmaci_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_pkey PRIMARY KEY (vid, fid);


--
-- Name: ssn ssn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ssn
    ADD CONSTRAINT ssn_pkey PRIMARY KEY (sid);


--
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (tid);


--
-- Name: utenti utenti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti
    ADD CONSTRAINT utenti_pkey PRIMARY KEY (uid);


--
-- Name: visita_base visita_base_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_base
    ADD CONSTRAINT visita_base_pkey PRIMARY KEY (vid);


--
-- Name: visita_specialistica visita_specialistica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_specialistica
    ADD CONSTRAINT visita_specialistica_pkey PRIMARY KEY (pid, mid, tid);


--
-- Name: erogazione_esami erogazione_esami_prescid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_esami
    ADD CONSTRAINT erogazione_esami_prescid_fkey FOREIGN KEY (prescid) REFERENCES public.prescrizione_esami(prescid);


--
-- Name: erogazione_esami erogazione_esami_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_esami
    ADD CONSTRAINT erogazione_esami_sid_fkey FOREIGN KEY (sid) REFERENCES public.ssn(sid);


--
-- Name: erogazione_esami erogazione_esami_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_esami
    ADD CONSTRAINT erogazione_esami_tid_fkey FOREIGN KEY (tid) REFERENCES public.ticket(tid);


--
-- Name: erogazione_farmaci erogazione_farmaci_fid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_farmaci
    ADD CONSTRAINT erogazione_farmaci_fid_fkey FOREIGN KEY (fid) REFERENCES public.farmaci(fid);


--
-- Name: erogazione_farmaci erogazione_farmaci_vid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.erogazione_farmaci
    ADD CONSTRAINT erogazione_farmaci_vid_fkey FOREIGN KEY (vid) REFERENCES public.visita_base(vid);


--
-- Name: has_base has_base_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.has_base
    ADD CONSTRAINT has_base_mid_fkey FOREIGN KEY (mid) REFERENCES public.medici(mid);


--
-- Name: has_base has_base_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.has_base
    ADD CONSTRAINT has_base_pid_fkey FOREIGN KEY (pid) REFERENCES public.pazienti(pid);


--
-- Name: medici medici_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medici
    ADD CONSTRAINT medici_mid_fkey FOREIGN KEY (mid) REFERENCES public.utenti(uid);


--
-- Name: pazienti pazienti_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pazienti
    ADD CONSTRAINT pazienti_pid_fkey FOREIGN KEY (pid) REFERENCES public.utenti(uid);


--
-- Name: prescrizione_esami prescrizione_esami_eid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_eid_fkey FOREIGN KEY (eid) REFERENCES public.esami(eid);


--
-- Name: prescrizione_esami prescrizione_esami_vid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_vid_fkey FOREIGN KEY (vid) REFERENCES public.visita_base(vid);


--
-- Name: prescrizione_farmaci prescrizione_farmaci_fid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_fid_fkey FOREIGN KEY (fid) REFERENCES public.farmaci(fid);


--
-- Name: prescrizione_farmaci prescrizione_farmaci_vid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_vid_fkey FOREIGN KEY (vid) REFERENCES public.visita_base(vid);


--
-- Name: ssn ssn_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ssn
    ADD CONSTRAINT ssn_sid_fkey FOREIGN KEY (sid) REFERENCES public.utenti(uid);


--
-- Name: visita_base visita_base_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_base
    ADD CONSTRAINT visita_base_mid_fkey FOREIGN KEY (mid) REFERENCES public.medici(mid);


--
-- Name: visita_base visita_base_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_base
    ADD CONSTRAINT visita_base_pid_fkey FOREIGN KEY (pid) REFERENCES public.pazienti(pid);


--
-- Name: visita_specialistica visita_specialistica_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_specialistica
    ADD CONSTRAINT visita_specialistica_mid_fkey FOREIGN KEY (mid) REFERENCES public.medici(mid);


--
-- Name: visita_specialistica visita_specialistica_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_specialistica
    ADD CONSTRAINT visita_specialistica_pid_fkey FOREIGN KEY (pid) REFERENCES public.pazienti(pid);


--
-- Name: visita_specialistica visita_specialistica_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visita_specialistica
    ADD CONSTRAINT visita_specialistica_tid_fkey FOREIGN KEY (tid) REFERENCES public.ticket(tid);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

