# import sql for postgres
import psycopg2, sqlite3, random, time, hashlib
import codicefiscale
from datetime import datetime
from random import randint

# USEFUL INFO
'''
The date format is in american format (mm/dd/yyyy)

MANCA DESCRIZIONE NELLA VISITA PAZIENTE!
'''

_names = []
_surnames = []
_farmaci = []
_locations = {
    # "verona" : "L781R",
    # "negrar" : "F861L",
    # "trento" : "L378Z",
    # "brescia" : "B157E",
    # "bolzano" : "A952M",
    # "firenze" : "D612F",
    # "milano" : "F205O",
    # "torino" : "L219T",
    # "padova" : "G224D",
    # "vicenza" : "L840Q"
    "verona" : "L781",
    "negrar" : "F861",
    "trento" : "L378",
    "brescia" : "B157",
    "bolzano" : "A952",
    "firenze" : "D612",
    "milano" : "F205",
    "torino" : "L219",
    "padova" : "G224",
    "vicenza" : "L840"
}
_esami = [('1','sangue'),('2','muscolo cardiaco'),('3','radiografia')]
_provincie = ["Trento", "Bolzano", "Verona"]

_pazienti = []
_medici = []
_ssn = []
_ticket = []
_has_base = []
_visita_base = []
_prescrizioni = []
_erogazione_farmaci = set()
_pics = set()

# useful functions
def get_names():
    global _names, _surnames

    db = sqlite3.connect('./db_names/names_surnames.sql3')
    cursor = db.cursor()

    query = "SELECT name, gender FROM names;"
    cursor.execute(query)
    _names = cursor.fetchall()

    query = "SELECT surname FROM surnames;"
    cursor.execute(query)
    _surnames = cursor.fetchall()

def get_farmaci():
    global _farmaci

    db = sqlite3.connect('./db_names/farmaci.sql3')
    cursor = db.cursor()

    query = 'SELECT name, code FROM farmaci;'
    cursor.execute(query)
    _farmaci = cursor.fetchall()

def str_time_prop(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    format = '%d/%m/%Y'

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return str(int(ptime))

def random_date(start, end, prop):
    return str_time_prop(start, end, '%d/%m/%Y', prop)

def get_password(password):
    return hashlib.sha512(password.encode()).hexdigest()

def create_arr_pazienti():
    for paz in range(1,101):
        pid = "paz_{}".format(paz)
        name, gender = random.choice(_names)
        pnome = name
        pcognome = random.choice(_surnames)[0].capitalize()
        psesso = gender
        pluogo_nascita = random.choice(list(_locations.keys()))
        pdata_nascita = random_date("1/1/1930", "31/12/2000", random.random())
        pemail = "%s.%s%s@hospital.com" % (pnome.lower(), pcognome.lower(), random.randint(0,1000000))
        pprovincia_residenza = "Trento"
        pcodice_fiscale = codicefiscale.build(surname=pcognome, name=pnome,
                            birthday= datetime.strptime(time.strftime('%d/%m/%Y', time.localtime(int(pdata_nascita))), '%d/%m/%Y'),
                            sex=psesso.upper(),
                            municipality= _locations[pprovincia_residenza.lower()])
        #pdata_nascita = datetime.strptime(time.strftime('%d/%m/%Y', time.localtime(int(pdata_nascita))), "%d/%m/%Y").strftime("%m/%d/%Y")
        _pazienti.append((pid, pnome, pcognome, psesso, pcodice_fiscale,
                            pluogo_nascita, pdata_nascita, pemail, 
                            pprovincia_residenza))

def create_arr_medici():
    pmedici = set()
    i = 1
    while i < 26:
        paz = random.choice(_pazienti)
        if paz not in pmedici:
            pmedici.add(paz)
            mid = "med_{}".format(i)
            mnome = paz[1]
            mcognome = paz[2]
            msesso = paz[3]
            mcodice_fiscale = paz[4]
            memail = paz[7]
            mprovincia_operato = "Trento"
            _medici.append((mid, mnome, mcognome, msesso, mcodice_fiscale,
                            memail, mprovincia_operato))
            i += 1

def load_pics():
    with open("db_images/pics.txt", 'r') as f:
        string = f.readline()
        while string != "":
            _pics.add("./img/" + string.replace("\n", ""))
            string = f.readline()


# connection to database

def create_tables(conn, cursor):
    try:    
        create_utenti(conn, cursor)
        create_pazienti(conn, cursor)
        create_medici(conn, cursor)
        cerate_ssn(conn, cursor)
        create_farmaci(conn, cursor)
        create_esami(conn, cursor)
        create_ticket(conn, cursor)

        create_has_base(conn, cursor)
        create_visita_base(conn, cursor)
        create_visita_specialistica(conn, cursor)
        create_prescrizione_farmaci(conn, cursor)
        create_erogazione_farmaci(conn, cursor)
        create_prescrizione_esami(conn, cursor)
        create_erogazione_esami(conn, cursor)
    except Exception as e:
        conn.rollback()
        print(e)

def pop_tables(conn, cursor):
    try:
        pop_utenti(conn,cursor)
        pop_pazienti(conn,cursor)
        pop_medici(conn,cursor)
        pop_ssn(conn,cursor)
        pop_farmaci(conn,cursor)
        pop_esami(conn,cursor)
        pop_ticket(conn,cursor)

        pop_has_base(conn,cursor)
        pop_visita_base(conn,cursor)
        pop_visita_specialistica(conn,cursor)
        pop_prescrizione_farmaci(conn,cursor)
        pop_erogazione_farmaci(conn,cursor)
        pop_prescrizione_esami(conn,cursor)
        pop_erogazione_esami(conn,cursor)
    except Exception as e:
        conn.rollback()
        print(e)

def create_utenti(conn, cursor):
    print("Creating utenti...")
    query = "CREATE TABLE utenti (\
                uid VARCHAR(15),\
                ufoto VARCHAR(30) NOT NULL,\
                uemail VARCHAR(50) NOT NULL,\
                upassword VARCHAR(129) NOT NULL,\
                PRIMARY KEY (uid)\
                )"
    cursor.execute(query)
    conn.commit()

def pop_utenti(conn, cursor):
    print("Populating utenti")
    password = '123456'
    hash_p = get_password(password)
    query = "INSERT INTO utenti (uid, ufoto, uemail, upassword) VALUES(%s,%s,%s,%s)"

    paz = []
    for i in range(1,101):
        email = _pazienti[i-1][7]
        _pazienti[i-1] = (_pazienti[i-1][0],_pazienti[i-1][1],_pazienti[i-1][2],\
                            _pazienti[i-1][3],_pazienti[i-1][4],_pazienti[i-1][5],\
                            _pazienti[i-1][6],_pazienti[i-1][8])
        paz.append(("paz_{}".format(i), _pics.pop(), email, hash_p))
    cursor.executemany(query, paz)
    conn.commit()
    
    med = []
    for i in range(1,26):
        email = _medici[i-1][5]
        _medici[i-1] = _medici[i-1][0],_medici[i-1][1],_medici[i-1][2],\
                        _medici[i-1][3],_medici[i-1][4],_medici[i-1][6]
        med.append(("med_{}".format(i), _pics.pop(), email, hash_p))
    cursor.executemany(query, med)
    conn.commit()

    ssn = []
    for i in range(1,6):
        email = "ssn%s@hospital.com" % i
        ssn.append(("ssn_{}".format(i), _pics.pop(), email, hash_p))
    cursor.executemany(query, ssn)
    conn.commit()

def create_pazienti(conn, cursor):
    print("Creating pazienti...")
    query = "CREATE TABLE pazienti (\
                pid VARCHAR(15),\
                pnome VARCHAR(20) NOT NULL,\
                pcognome VARCHAR(20) NOT NULL,\
                psesso VARCHAR(1) NOT NULL,\
                pcodice_fiscale VARCHAR(16) NOT NULL,\
                pluogo_nascita VARCHAR(25) NOT NULL,\
                pdata_nascita BIGINT NOT NULL,\
                pprovincia_residenza VARCHAR(50) NOT NULL,\
                PRIMARY KEY (pid),\
                FOREIGN KEY (pid) REFERENCES utenti (uid)\
                )"
    cursor.execute(query)
    conn.commit()

def pop_pazienti(conn, cursor):
    global _names, _surnames, _locations, _pazienti

    print("Populating pazienti...")
    query = "INSERT INTO pazienti (pid, pnome, pcognome, psesso, pcodice_fiscale,\
                pluogo_nascita, pdata_nascita, pprovincia_residenza)\
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
    cursor.executemany(query, _pazienti)
    conn.commit() 

def create_medici(conn, cursor):
    print("Creating medici...")
    query = "CREATE TABLE medici (\
                mid VARCHAR(15),\
                mnome VARCHAR(20) NOT NULL,\
                mcognome VARCHAR(20) NOT NULL,\
                msesso VARCHAR(1) NOT NULL,\
                mcodice_fiscale VARCHAR(16) NOT NULL,\
                mprovincia_operato VARCHAR(50) NOT NULL,\
                PRIMARY KEY (mid),\
                FOREIGN KEY (mid) REFERENCES utenti (uid)\
                )"
    cursor.execute(query)
    conn.commit()

def pop_medici(conn, cursor):
    global _medici

    print("Populating medici...")
    query = "INSERT INTO medici (mid, mnome, mcognome, msesso, mcodice_fiscale,\
                mprovincia_operato) VALUES (%s,%s,%s,%s,%s,%s)"

    cursor.executemany(query, _medici)
    conn.commit() 

def cerate_ssn(conn, cursor):
    print("Creating ssn...")
    query = "CREATE TABLE ssn(\
                sid VARCHAR(15),\
                snome VARCHAR(30) NOT NULL,\
                sprovincia VARCHAR(10) NOT NULL,\
                PRIMARY KEY (sid),\
                FOREIGN KEY (sid) REFERENCES utenti (uid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_ssn(conn, cursor):
    global _ssn

    print("Populating ssn...")
    query = "INSERT INTO ssn (sid, snome, sprovincia) VALUES (%s,%s,%s);"
    for i in range(1,6):
        sid = "ssn_{}".format(i)
        sprovincia = random.choice(_provincie)
        snome = "ssn_{}_{}".format(sprovincia, i)
        _ssn.append((sid,snome,sprovincia))
    cursor.executemany(query, _ssn)
    conn.commit() 

def create_farmaci(conn, cursor):
    query = "CREATE TABLE farmaci(\
                fid VARCHAR(12),\
                fnome VARCHAR(50) NOT NULL,\
                PRIMARY KEY (fid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_farmaci(conn, cursor):
    global _farmaci

    print("Populating farmaci...")
    query = "INSERT INTO farmaci (fid, fnome) VALUES (%s,%s);"
    f = []
    for nome, code in _farmaci:
        f.append((code,nome))
    cursor.executemany(query, f)
    conn.commit()

def create_esami(conn, cursor):
    print("Creating esami...")
    query = "CREATE TABLE esami(\
                eid VARCHAR(10),\
                ename VARCHAR(20) NOT NULL,\
                PRIMARY KEY (eid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_esami(conn, cursor):
    global _esami
    print("Populating esami...")
    query = "INSERT INTO esami(eid, ename) VALUES (%s,%s);"
    cursor.executemany(query, _esami)
    conn.commit()

def create_ticket(conn, cursor):
    print("Creating ticket...")
    query = "CREATE TABLE ticket(\
                tid VARCHAR(10),\
                tprice FLOAT NOT NULL,\
                tpaid BOOLEAN NOT NULL,\
                PRIMARY KEY (tid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_ticket(conn, cursor):
    print("Popuating ticket...")
    query = "INSERT INTO ticket(tid, tprice, tpaid) VALUES (%s,%s,%s);"

    # price = set(['50.00','25.00'])
    for i in range(1,50):
        tprice = 11.00
        tpaid = True if random.random() > 0.5 else False
        _ticket.append((str(i), tprice, tpaid))
    for i in range(50,101):
        tprice = 50.00
        tpaid = True if random.random() > 0.5 else False
        _ticket.append((str(i), tprice, tpaid))
    cursor.executemany(query, _ticket)
    conn.commit()

# RELATIONS
def create_has_base(conn, cursor):
    print("Creating has_base...")
    query = "CREATE TABLE has_base (\
                pid VARCHAR(15),\
                mid VARCHAR(15),\
                PRIMARY KEY (pid, mid),\
                FOREIGN KEY (pid) REFERENCES pazienti (pid),\
                FOREIGN KEY (mid) REFERENCES medici (mid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_has_base(conn, cursor):
    global _medici, _pazienti, _has_base

    print("Populating has_base...")
    query = "INSERT INTO has_base (pid, mid) VALUES (%s, %s);"
    medici = {}
    for medico in _medici:
        if medico[5] not in medici:
            medici[medico[5]] = [medico[0]]
        else:
            medici[medico[5]].append(medico[0])

    for paziente in _pazienti:
        med = 0
        while med == 0 or med == paziente[0]:
            med = random.choice(medici[paziente[7]])
        _has_base.append((paziente[0], med))
    cursor.executemany(query, _has_base)
    conn.commit()

def create_visita_base(conn, cursor):
    print("Creating visita_base")
    query = "CREATE TABLE visita_base (\
                vid VARCHAR(15),\
                pid VARCHAR(15) NOT NULL,\
                mid VARCHAR(15) NOT NULL,\
                date BIGINT NOT NULL,\
                description TEXT NOT NULL,\
                PRIMARY KEY (vid),\
                FOREIGN KEY (pid) REFERENCES pazienti (pid),\
                FOREIGN KEY (mid) REFERENCES medici (mid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_visita_base(conn, cursor):
    global _has_base

    print("Populating visita_base")
    query = "INSERT INTO visita_base (vid, pid, mid, date, description)\
                VALUES (%s,%s,%s,%s,%s);"

    for vid in range(1,100):
        has_base = random.choice(_has_base)
        date = random_date("1/1/2000", "08/11/2019", random.random())
        # date = datetime.strptime(date, "%d/%m/%Y").strftime("%m/%d/%Y")
        description = "This is a normal description. In this filed the doctor writes the examination information"
        _visita_base.append((str(vid), has_base[0], has_base[1], date, description))
    cursor.executemany(query, _visita_base)
    conn.commit()

def create_visita_specialistica(conn, cursor):
    print("Creating visita_specialistica")
    query = "CREATE TABLE visita_specialistica (\
                pid VARCHAR(15) NOT NULL,\
                mid VARCHAR(15) NOT NULL,\
                tid VARCHAR(15) NOT NULL,\
                date BIGINT NOT NULL,\
                description TEXT NOT NULL,\
                PRIMARY KEY (pid, mid, tid),\
                FOREIGN KEY (pid) REFERENCES pazienti (pid),\
                FOREIGN KEY (mid) REFERENCES medici (mid),\
                FOREIGN KEY (tid) REFERENCES ticket (tid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_visita_specialistica(conn, cursor):
    global _pazienti, _medici, _ticket
    print("Populating visita_specialitica...")
    query = "INSERT INTO visita_specialistica (pid,mid,tid,date,description) VALUES (%s,%s,%s,%s,%s);"
    visita_specialistica = []
    for i in range(50,len(_ticket)):
        med = 0
        paziente = random.choice(_pazienti)
        while med == 0 or med == paziente[0]:
            med = random.choice(_medici)[0]
        ticket = _ticket[i][0]
        date = random_date("1/1/2000", "08/11/2019", random.random())
        description = 'Aggiungere qui la descrizione della visita specialistica'
        visita_specialistica.append((paziente[0], med, ticket, date, description))
    cursor.executemany(query, visita_specialistica)
    conn.commit()

def create_prescrizione_farmaci(conn, cursor):
    print("Creating prescrizione_farmaci")
    query = "CREATE TABLE prescrizione_farmaci (\
                vid VARCHAR(15),\
                fid VARCHAR(12),\
                quantity INTEGER,\
                PRIMARY KEY (vid, fid),\
                FOREIGN KEY (vid) REFERENCES visita_base (vid),\
                FOREIGN KEY (fid) REFERENCES farmaci (fid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_prescrizione_farmaci(conn, cursor):
    global _visita_base, _farmaci, _erogazione_farmaci

    print("Populating prescrizione_farmaci...")
    query = "INSERT INTO prescrizione_farmaci(vid, fid, quantity)\
                VALUES (%s,%s,%s);"
    
    prescrizioni = set()
    for i in range(400):
        if (i%2 == 0):
            visita = farmaco = 0
            while visita == 0 or farmaco == 0 or (visita,farmaco) in prescrizioni:
                visita = random.choice(_visita_base)[0]
                farmaco = random.choice(_farmaci)[1]
            prescrizioni.add((visita,farmaco))
        else:
            visita = farmaco = 0
            while visita == 0 or farmaco == 0 or (visita,farmaco) in prescrizioni:
                v = random.choice(_visita_base)
                visita = v[0]
                date = randint(int(v[3]),1579858381)
                farmaco = random.choice(_farmaci)[1]
                quantity = random.randint(1,5)
            _erogazione_farmaci.add((visita,farmaco,date,quantity))
    l_prescrizioni = []
    for presc in prescrizioni:
        quantity = random.randint(1,5)
        l_prescrizioni.append((presc[0],presc[1],quantity))

    cursor.executemany(query, l_prescrizioni)
    conn.commit()

def create_erogazione_farmaci(conn, cursor):
    print("Creating erogazione_farmaci")
    query = "CREATE TABLE erogazione_farmaci (\
                vid VARCHAR(15),\
                fid VARCHAR(12),\
                date BIGINT NOT NULL,\
                quantity INT NOT NULL,\
                PRIMARY KEY (vid, fid),\
                FOREIGN KEY (vid) REFERENCES visita_base (vid),\
                FOREIGN KEY (fid) REFERENCES farmaci (fid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_erogazione_farmaci(conn, cursor):
    global _visita_base, _farmaci, _erogazione_farmaci

    print("Populating erogazione_farmaci...")
    query = "INSERT INTO erogazione_farmaci(vid, fid, date, quantity)\
                VALUES (%s,%s,%s, %s);"
    
    cursor.executemany(query, list(_erogazione_farmaci))
    conn.commit()

def create_prescrizione_esami(conn, cursor):
    print("Creating prescrizione_esami")
    query = "CREATE TABLE prescrizione_esami (\
                prescid VARCHAR(15),\
                vid VARCHAR(15) NOT NULL,\
                eid VARCHAR(10) NOT NULL,\
                PRIMARY KEY (prescid),\
                FOREIGN KEY (vid) REFERENCES visita_base (vid),\
                FOREIGN KEY (eid) REFERENCES esami (eid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_prescrizione_esami(conn, cursor):
    global _prescrizioni
    print("Populating prescrizione_esami...")
    query = "INSERT INTO prescrizione_esami (prescid,vid,eid) VALUES (%s,%s,%s);"

    prescrizioni = set()
    for i in range(200):
        visita = random.choice(_visita_base)
        esame = random.choice(_esami)
        prescrizioni.add((visita[0],esame[0]))
    
    prescrizioni = list(prescrizioni)
    for i in range(1,len(prescrizioni)):
        _prescrizioni.append((str(i), prescrizioni[i][0], prescrizioni[i][1]))
    cursor.executemany(query, _prescrizioni)
    conn.commit()

def create_erogazione_esami(conn, cursor):
    print("Creating erogazione_esami")
    query = "CREATE TABLE erogazione_esami (\
                prescid VARCHAR(16),\
                tid VARCHAR(12) NOT NULL,\
                sid VARCHAR(15) NOT NULL,\
                date BIGINT NOT NULL,\
                description TEXT NOT NULL,\
                PRIMARY KEY (prescid),\
                FOREIGN KEY (prescid) REFERENCES prescrizione_esami (prescid),\
                FOREIGN KEY (tid) REFERENCES ticket (tid),\
                FOREIGN KEY (sid) REFERENCES ssn (sid)\
                );"
    cursor.execute(query)
    conn.commit()

def pop_erogazione_esami(conn, cursor):
    global _prescrizioni
    print("Popolating erogazione_esami...")
    query = "INSERT INTO erogazione_esami (prescid,tid,sid,date,description) VALUES (%s,%s,%s,%s,%s);"
    ero = set()
    prescid = 0
    erogazioni = []
    for i in range(1,50):
        while prescid == 0 or prescid in ero:
            prescid = random.choice(_prescrizioni)[0]
        ero.add((prescid))
        ticket = _ticket[i][0]
        sid = random.choice(_ssn)[0]
        date = random_date("1/1/2015", "15/1/2020", random.random())
        # date = datetime.strptime(date, "%d/%m/%Y").strftime("%m/%d/%Y")
        description = 'Aggiungere qui la descrizione di un esame'
        erogazioni.append((prescid, ticket, sid, date, description))
    cursor.executemany(query, erogazioni)
    conn.commit()

def drop_all(conn, cursor):
    tables = ["erogazione_esami", "prescrizione_farmaci", "erogazione_farmaci", "visita_specialistica",
                "prescrizione_esami", "visita_base", "has_base", "farmaci",
                "medici", "pazienti", "ssn", "utenti", "ticket", "esami"]
    for table in tables:
        try:
            query = "DROP TABLE %s;" % table
            cursor.execute(query)
            conn.commit()
        except Exception as e:
            conn.rollback()
            print(e)


if __name__=="__main__":
    get_names()
    get_farmaci()
    create_arr_pazienti()
    create_arr_medici()
    load_pics()
    conn = psycopg2.connect(host="localhost",database="prog_web",
                            user="postgres", password="password")
    cursor = conn.cursor()

    # pop_utenti(conn, cursor)
    # pop_pazienti(conn, cursor)
    # pop_medici(conn, cursor)

    drop_all(conn,cursor)

    create_tables(conn, cursor)
    pop_tables(conn,cursor)